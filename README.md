# LibView

[![codecov](https://codecov.io/gl/VollkornSandWicht/libview/branch/master/graph/badge.svg)](https://codecov.io/gl/VollkornSandWicht/libview)

## Setup on Windows

1. Download and install Qt5: https://www.qt.io/download-open-source
2. Set up an "Qt5_DIR" environment variable to point to the "Qt5Config.cmake" path in your Qt installation, e.g., "C:\Qt\5.15.0\msvc2019\lib\cmake\Qt5"
3. Download and install CMake: https://cmake.org/download/
4. Clone this repository.
5. Start CMake (GUI) to create the Visual Studio files. If you have the CMake executable in your PATH environment variable, you can also run the "_GenerateProjects.bat" script to generate the project files.
6. Open the Visual Studio solution file in your build directory.

## Setup on Linux (Ubuntu)

Install Qt and build tools.

    # apt install git cmake make g++ qtbase5-dev

Clone and build the project.

    $ git clone https://gitlab.com/VollkornSandWicht/libview.git
    $ cd libview
    $ mkdir build
    $ cd build
    $ cmake ..
    $ cmake --build .
