cmake_minimum_required(VERSION 3.8)

project(LibView)

# To find Qt5 on Windows, set the environment variable
# "Qt5_DIR" to "C:\Qt\5.15.0\msvc2019\lib\cmake\Qt5"
# (or to whatever your installation path and IDE is)

# --- Qt5 configuration ---
set(CMAKE_AUTOMOC ON)
#set(CMAKE_AUTORCC ON)
set(CMAKE_AUTOUIC ON)

find_package(Qt5 COMPONENTS Widgets REQUIRED)

# --- Add targets ---
add_subdirectory(LibView/src/LibView)
add_subdirectory(LibView/src/test)