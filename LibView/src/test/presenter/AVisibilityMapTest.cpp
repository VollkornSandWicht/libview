#ifdef LIBVIEW_TEST

#include "catch2/catch.hpp"
#include "LibView/presenter/AVisibilityMap.h"
#include "LibView/exception/MapIndexNotPresentException.h"
#include "LibView/exception/MapIndexAlreadyPresentException.h"

SCENARIO("Visibility Map")
{
    GIVEN("A Map with entry")
    {
        LibView::index_t index1 = LibView::index_t::requestUnique();
        LibView::index_t index2 = LibView::index_t::requestUnique();
        LibView::presenter::AVisibilityMap aVisibilityMap;
        std::vector<LibView::index_t> keyList;
        keyList.push_back(index1);
        keyList.push_back(index2);

        WHEN("The map has no entries")
        {
            REQUIRE_THROWS_AS(aVisibilityMap.getVisibility(index1), LibView::exception::MapIndexNotPresentException);

            /*REQUIRE_THROWS_AS(aVisibilityMap.setVisibility(index1, true), LibView::exception::MapIndexNotPresentException);*/

            REQUIRE_THROWS_AS(aVisibilityMap.removeKey(index1), LibView::exception::MapIndexNotPresentException);
          
        }


        WHEN("The Map has only one entry")
        {
            aVisibilityMap.addKey(index1);
            
            REQUIRE_THROWS_AS(aVisibilityMap.setVisibilty(index2,true), LibView::exception::MapIndexNotPresentException);
            
            REQUIRE_THROWS_AS(aVisibilityMap.addKey(index1), LibView::exception::MapIndexAlreadyPresentException);
        }

        WHEN("Indices are added with list")
        {
            aVisibilityMap.addKeys(keyList);
            THEN("The Keys will appear in the map as true")
            {
                REQUIRE(aVisibilityMap.getVisibility(index1));
                REQUIRE(aVisibilityMap.getVisibility(index2));
            }

            THEN("The keys can be removed with the list")
            {
                aVisibilityMap.removeKeys(keyList);
                REQUIRE_THROWS_AS(aVisibilityMap.getVisibility(index1), LibView::exception::MapIndexNotPresentException);

                REQUIRE_THROWS_AS(aVisibilityMap.removeKey(index1), LibView::exception::MapIndexNotPresentException);

                REQUIRE_THROWS_AS(aVisibilityMap.getVisibility(index2), LibView::exception::MapIndexNotPresentException);

                REQUIRE_THROWS_AS(aVisibilityMap.removeKey(index2), LibView::exception::MapIndexNotPresentException);
            }
        }


        WHEN("Map is received with getter")
        {
            using namespace LibView;
            LIBVIEW_AVIS_MAP map = aVisibilityMap.getMap();
            REQUIRE(map.size() == 0);

        }



        WHEN("An index is added")
        {
            aVisibilityMap.addKey(index1);
            aVisibilityMap.addKey(index2);


            THEN("It will appear in the map as true")
            {
                REQUIRE(aVisibilityMap.getVisibility(index1));
            }


            THEN("The visibility can be set and returned")
            {
                aVisibilityMap.setVisibilty(index1, true);
                aVisibilityMap.setVisibilty(index2, false);

                REQUIRE(aVisibilityMap.getVisibility(index1));


                REQUIRE_FALSE(aVisibilityMap.getVisibility(index2));

            }

            THEN("The index can be removed and is gone") 
            {
                aVisibilityMap.removeKey(index1);
                
                
                REQUIRE_THROWS_AS(aVisibilityMap.getVisibility(index1), LibView::exception::MapIndexNotPresentException);

                REQUIRE_THROWS_AS(aVisibilityMap.removeKey(index1), LibView::exception::MapIndexNotPresentException);
                
            }
        }


    }

}








#endif
