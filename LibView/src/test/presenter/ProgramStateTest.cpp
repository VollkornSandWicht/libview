#ifdef LIBVIEW_TEST

#include "catch2/catch.hpp"



#include "LibView/presenter/ProgramState.h"
#include "LibView/presenter/AttributeVisibilityMap.h"
#include "LibView/presenter/LibFileVisibilityMap.h"
#include "LibView/model/ModelState.h"
#include "LibView/exception/model/AttributePathNotFoundException.h"


#include <iostream>

SCENARIO("The ProgramState can hold and manage the current state of the application.", "[LibView::presenter::ProgramState]")
{
    GIVEN("A ProgramState instance with reference to non-trivial AttributeVisibilityMap, LibFileVisibilityMap and ModelState")
    {
        LibView::model::ModelState modelState;
        LibView::presenter::LibFileVisibilityMap lvVmap;
        LibView::presenter::AttributeVisibilityMap atVmap;

        LibView::presenter::ProgramState state(atVmap, lvVmap, modelState);

        WHEN("The lastAdded and lastRemoved lists are changed in the modelState")
        {
            LibView::index_t index1 = LibView::index_t::requestUnique();
            LibView::index_t index2 = LibView::index_t::requestUnique();

            std::vector<LibView::index_t> lastAdded;
            lastAdded.push_back(index1);
            std::vector<LibView::index_t> lastRemoved;
            lastRemoved.push_back(index2);

            modelState.updateLastChangedLibFiles(lastAdded, lastRemoved);

            THEN("It is reflected in the programState")
            {
                REQUIRE(state.getLastAddedLibFiles().at(0) == index1);
                REQUIRE(state.getLastRemovedLibFiles().at(0) == index2);
            }
        }

        WHEN("The visibility of LibFile is changed")
        {
            LibView::index_t index3 = LibView::index_t::requestUnique();
            lvVmap.addKey(index3);
            lvVmap.setVisibilty(index3, false);

            THEN("It is reflected in the programState")
            {
                REQUIRE_FALSE(state.getLibFileVisibility(index3));
            }
        }

        WHEN("The visibility of an attribute is changed")
        {
            LibView::index_t index4 = LibView::index_t::requestUnique();
            atVmap.addKey(index4);
            atVmap.setVisibilty(index4, false);

            THEN("It is reflected in the programState")
            {
                REQUIRE_FALSE(state.getAttributeVisibility(index4));
            }
        }

    }
}
#endif