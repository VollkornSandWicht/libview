#ifdef LIBVIEW_TEST
#include "catch2/catch.hpp"

#include "LibView/presenter/AttributeVisibilityMap.h"

SCENARIO("All Indices in the Attribute visibility map can be set to invisible")
{
    GIVEN("A Attribute Visibility map with entries")
    {
        LibView::index_t index1 = LibView::index_t::requestUnique();
        LibView::index_t index2 = LibView::index_t::requestUnique();
        LibView::presenter::AttributeVisibilityMap attributeVisibilityMap;
        attributeVisibilityMap.addKey(index1);
        attributeVisibilityMap.addKey(index2);

        WHEN("All visibilitys should be set to false")
        {
            attributeVisibilityMap.setAllAttributesInvisible();
            THEN("All visibilitys are set to false")
            {
                REQUIRE_FALSE(attributeVisibilityMap.getVisibility(index1));
                REQUIRE_FALSE(attributeVisibilityMap.getVisibility(index2));
            }
        }
    }
}





#endif