#ifdef LIBVIEW_TEST

#include "catch2/catch.hpp"



#include "LibView/model/AttributeMap.h"
#include "LibView/exception/model/AttributeIndexAlreadyFoundException.h"
#include "LibView/exception/model/AttributeIndexNotFoundException.h"
#include "LibView/exception/model/AttributePathAlreadyFoundException.h"
#include "LibView/exception/model/AttributePathNotFoundException.h"


#include <iostream>

SCENARIO("Values can be added to an AttributeMap instance.", "[LibView::model::AttributeMap]")
{
    GIVEN("An empty AttributeMap instance.")
    {
        LibView::model::AttributeMap map;

        LibView::index_t index1 = LibView::index_t::requestUnique();
        LibView::index_t index2 = LibView::index_t::requestUnique();

        WHEN("A new set of values is added.")
        {
            REQUIRE_NOTHROW(map.add("path", index1, 2));

            THEN("The values are present in the underlying map data structure.")
            {
                REQUIRE(map.m_AttributeMap["path"].getFirst() == index1);
                REQUIRE(map.m_AttributeMap["path"].getSecond() == 2);
                REQUIRE(map.m_ReverseMap[index1] == "path");
            }

            THEN("The values can be returned by the get methods")
            {
                REQUIRE(map.getIndex("path") == index1);
                REQUIRE(map.getPath(index1) == "path");
            }
        }
    }

    GIVEN("A non empty AttributeMap instance.")
    {
        LibView::index_t index1 = LibView::index_t::requestUnique();
        LibView::index_t index2 = LibView::index_t::requestUnique();
        
        LibView::model::AttributeMap map;
        map.add("path", index1, 2);

        WHEN("A new set of values is added.")
        {
            REQUIRE_NOTHROW(map.add("path2", index2, 2));

            THEN("The values are present in the underlying map data structure.")
            {
                REQUIRE(map.m_AttributeMap["path2"].getFirst() == index2);
                REQUIRE(map.m_AttributeMap["path2"].getSecond() == 2);
                REQUIRE(map.m_ReverseMap[index2] == "path2");

                REQUIRE(map.m_AttributeMap["path"].getFirst() == index1);
                REQUIRE(map.m_AttributeMap["path"].getSecond() == 2);
                REQUIRE(map.m_ReverseMap[index1] == "path");
            }

            THEN("The values can be returned by the get methods")
            {
                REQUIRE(map.getIndex("path2") == index2);
                REQUIRE(map.getPath(index2) == "path2");
                REQUIRE(map.getIndex("path") == index1);
                REQUIRE(map.getPath(index1) == "path");
            }
        }

        WHEN("An existing set of values is added because of existing path.")
        {


            THEN("An exception is thrown.")
            {
                REQUIRE_THROWS_AS(map.add("path", index2, 2), LibView::exception::AttributePathAlreadyFoundException);
            }
        }

        WHEN("An existing set of values is added because of existing index.")
        {


            THEN("An exception is thrown.")
            {
                REQUIRE_THROWS_AS(map.add("path2", index1, 2), LibView::exception::AttributeIndexAlreadyFoundException);
            }
        }
    }
}

#endif