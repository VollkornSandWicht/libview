#ifdef LIBVIEW_TEST

#include "catch2/catch.hpp"
#include"LibView/model/LibViewPair.h"
#include "LibView/model/AttributeMap.h"
#include "LibView/exception/model/AttributePathAlreadyFoundException.h"
#include "LibView/exception/model/AttributePathNotFoundException.h"
#include "LibView/exception/model/AttributeIndexAlreadyFoundException.h"
#include "LibView/exception/model/AttributeIndexNotFoundException.h"


SCENARIO("AttributeMap")
{
    GIVEN("An map instance")
    {
        LibView::model::AttributeMap map;
        LibView::index_t index1 = LibView::index_t::requestUnique();
        LibView::index_t index2 = LibView::index_t::requestUnique();
        std::string path = "test";
        int64_t multiplicity = 1;
        map.add(path, index1, multiplicity);
        WHEN("values are set")
        {
            map.set(path, index1, multiplicity);
            THEN("the values are correct")
            {
                REQUIRE(map.getIndex(path) == index1);
                REQUIRE(map.getMultiplicity(path) == 1);
                REQUIRE(map.getPath(index1) == "test");
                REQUIRE_THROWS_AS(map.set("test2", index1, multiplicity), LibView::exception::AttributePathNotFoundException);
                REQUIRE_THROWS_AS(map.set(path, index2, multiplicity), LibView::exception::AttributeIndexNotFoundException);
            }
        }

        WHEN("entries are removed")
        {
            map.remove(index1);
            THEN("the entry no longer exists")
            {
                REQUIRE_THROWS_AS(map.has(index1),LibView::exception::AttributeIndexNotFoundException);
            }
        }
        WHEN("multiplicity is asked")
        {
            THEN("the multiplicity is returned")
            {
                REQUIRE(map.getMultiplicity(path) == 1);
                REQUIRE_THROWS_AS(map.getMultiplicity("test2"), LibView::exception::AttributePathNotFoundException);
            }
        }

        WHEN("existance is asked")
        {
            THEN("Existance is returned")
            {//Why no cover
                bool b= map.has(path);
                REQUIRE(b);
            }
        }

        WHEN("PairList is asked")
        {
            std::vector<LibView::model::LibViewPair<LibView::index_t, int64_t>> pairList = map.getPairList();
            THEN("PairList is returned")
            {
                REQUIRE(pairList.size() == 1);
            }
        }

        WHEN("IndexList is asked")
        {
            std::vector<LibView::index_t> indexList = map.getIndexList();
            THEN("IndexList is returned")
            {
                REQUIRE(indexList.size() == 1);
            }
        }
    }
}
#endif