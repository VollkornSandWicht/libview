#include "catch2/catch.hpp"
#include "LibView/model/APreset.h"

class TestPreset : public LibView::model::APreset
{
public:

    inline TestPreset() :
        APreset("TestPreset")
    {
        addPath("(.*)(test)(.*)");
    }
};

SCENARIO("APreset can be used","[model::APreset]")
{
    GIVEN("An instance of TestPreset")
    {
        TestPreset p;
        WHEN("a matching pattern is provided")
        {
            bool result = p.containsPath("1test1");
            THEN("true is returned")
            {
                REQUIRE(result);
            }
        }
        
        
        WHEN("a wrong pattern is provided")
        {
            bool result = p.containsPath("2BEST2");
            THEN("false is returned")
            {
                REQUIRE_FALSE(result);
            }
        }

        WHEN("a index is requested")
        {
            LibView::index_t i = p.index();
            THEN("a non null index is returned")
            {
                REQUIRE_FALSE(i.isNull());
            }
        }
    }
}
