#ifdef LIBVIEW_TEST

#include "catch2/catch.hpp"
#include "LibView/model/AttributeHolder.h"
#include <string>
#include<vector>
#include <iostream>
SCENARIO("AttributeHolder")
{
    GIVEN("An holder instance with some paths and a regex")
    {
        LibView::model::AttributeHolder holder;

        std::string path1 = "trOll";
        std::string path2 = "JoJo";
        std::string path3 = "DIOHHHH";
        
        std::string path4 = "JoJo/Jotaro";
        std::string path5 = "JoJo/Joseph";
        std::string path6 = "E";

        std::string exp = "(Jo){2}";
        std::string exp2 = "(.*)(O)(.*)";
        
        holder.addAttribute(path1);
        holder.addAttribute(path1);
        holder.addAttribute(path2);
        holder.addAttribute(path3);
        holder.addAttribute(path4);
        holder.addAttribute(path5);
        holder.addAttribute(path6);

       
        WHEN("children list is requested")
        {
            std::vector<LibView::index_t> result;
            result = holder.getChildren(holder.getAttribute(path2));

            THEN("The returned indices match")
            {
                REQUIRE(std::find(result.begin(), result.end(), holder.getAttribute(path4)) != result.end());
                REQUIRE(std::find(result.begin(), result.end(), holder.getAttribute(path5)) != result.end());
            }
        }
        
        
        WHEN("an attribute path is requested by an index")
        {
           
            THEN("The path matches")
            {
                REQUIRE(holder.getAttribute(holder.getAttribute(holder.getAttribute(holder.getAttribute(path2))))==path2);
            }
        }
        
        
        
        
        
        
        WHEN("matching attribute list is requested")
        {
            std::vector<LibView::index_t> result;
            result = holder.getMatchingAttributeList(exp);

            THEN("The returned indices match")
            {
                REQUIRE(std::find(result.begin(), result.end(), holder.getAttribute(path2)) != result.end());

            }
        }
        
        
        WHEN("matching attribute list is requested with multiple results")
        {
            std::vector<LibView::index_t> result;
            result = holder.getMatchingAttributeList(exp2);

            THEN("The returned indices match")
            {
                REQUIRE(std::find(result.begin(), result.end(), holder.getAttribute(path1)) != result.end());
                REQUIRE(std::find(result.begin(), result.end(), holder.getAttribute(path2)) == result.end());
                REQUIRE(std::find(result.begin(), result.end(), holder.getAttribute(path3)) != result.end());

            }
        }
    }
}
#endif