#ifdef LIBVIEW_TEST

#include "catch2/catch.hpp"

#include <iostream>

#include"LibView/model/data_structure/VoltageExpression.h"

SCENARIO("VoltageExpression Values can be set through setters and get through getters.", "[LibView::model::data_structure::VoltageExpression]")
{

    GIVEN("An empty VoltageExpression instance.")
    {

        LibView::model::data_structure::VoltageExpression* voltageExpression = new LibView::model::data_structure::VoltageExpression;

        WHEN("voltageExpression is set.")
        {
            REQUIRE_NOTHROW(voltageExpression->setVoltageExpression("VDD + 0.8", 1));

            THEN("The set value can be returned through the getter method.")
            {
                REQUIRE(voltageExpression->getAsDouble() == 5.8);
                REQUIRE(voltageExpression->getAsString() == "VDD + 0.8");

            }
        }

        WHEN("diffrent exprissions are set.")
        {
            REQUIRE_NOTHROW(voltageExpression->setVoltageExpression("VSS + 0.8", 1));

            THEN("The set value can be returned through the getter method.")
            {
                REQUIRE(voltageExpression->getAsDouble() == 0.8);
                REQUIRE(voltageExpression->getAsString() == "VSS + 0.8");
            }

            REQUIRE_NOTHROW(voltageExpression->setVoltageExpression("0.8 + VCC", 1));

            THEN("The set value can be returned through the getter method.")
            {
                REQUIRE(voltageExpression->getAsDouble() == 5.8);
                REQUIRE(voltageExpression->getAsString() == "0.8 + VCC");
            }

            REQUIRE_NOTHROW(voltageExpression->setVoltageExpression("0.8 + VSS", 1));

            THEN("The set value can be returned through the getter method.")
            {
                REQUIRE(voltageExpression->getAsDouble() == 0.8);
                REQUIRE(voltageExpression->getAsString() == "0.8 + VSS");
            }

            REQUIRE_NOTHROW(voltageExpression->setVoltageExpression("VSS", 1));

            THEN("The set value can be returned through the getter method.")
            {
                REQUIRE(voltageExpression->getAsDouble() == 0.0);
                REQUIRE(voltageExpression->getAsString() == "VSS");
            }

            REQUIRE_NOTHROW(voltageExpression->setVoltageExpression("VDD", 1));

            THEN("The set value can be returned through the getter method.")
            {
                REQUIRE(voltageExpression->getAsDouble() == 5.0);
                REQUIRE(voltageExpression->getAsString() == "VDD");
            }

            REQUIRE_NOTHROW(voltageExpression->setVoltageExpression("VCC", 1));

            THEN("The set value can be returned through the getter method.")
            {
                REQUIRE(voltageExpression->getAsDouble() == 5.0);
                REQUIRE(voltageExpression->getAsString() == "VCC");
            }

            REQUIRE_NOTHROW(voltageExpression->setVoltageExpression("0.8", 1));

            THEN("The set value can be returned through the getter method.")
            {
                REQUIRE(voltageExpression->getAsDouble() == 0.8);
                REQUIRE(voltageExpression->getAsString() == "0.8");
            }
        }
        delete voltageExpression;
    }
}
#endif
