
#ifdef LIBVIEW_TEST

#include "catch2/catch.hpp"

#include <iostream>

#include"LibView/model/data_structure/OcvSigmaTransition.h"
#include "LibView/model/ModelFacade.h"
#include "LibView/presenter/PresenterFacade.h"

SCENARIO("OcvSigmaTransition Values can be set through setters and get through getters.", "[LibView::model::data_structure::OcvSigmaTransition]")
{
    LibView::model::ModelFacade* modelFacade = new LibView::model::ModelFacade();
    LibView::presenter::PresenterFacade* presenter = new LibView::presenter::PresenterFacade(modelFacade);


    GIVEN("An empty OcvSigmaTransition instance.")
    {

        LibView::model::data_structure::OcvSigmaTransition* ost = new LibView::model::data_structure::OcvSigmaTransition(modelFacade);
        modelFacade->addAttribute("");

        WHEN("sigma_type is set.")
        {
            REQUIRE_NOTHROW(ost->setSigma_type("abc"));

            THEN("The set value can be returned through the getter method.")
            {
                REQUIRE(ost->getSigma_type() == "abc");
            }
        }

        delete ost;
    }
    delete presenter;
}
#endif