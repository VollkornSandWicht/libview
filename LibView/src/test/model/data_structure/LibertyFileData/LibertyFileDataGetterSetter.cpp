
#ifdef LIBVIEW_TEST

#include "catch2/catch.hpp"

#include <iostream>

#include"LibView/model/data_structure/LibertyFileData.h"
#include"LibView/model/data_structure/Library.h"
#include "LibView/model/ModelFacade.h"
#include "LibView/presenter/PresenterFacade.h"


SCENARIO("LibertyFileData Values can be set through setters and get through getters.", "[LibView::model::data_structure::LibertyFileData]")
{
    LibView::model::ModelFacade* modelFacade = new LibView::model::ModelFacade();
    LibView::presenter::PresenterFacade* presenter = new LibView::presenter::PresenterFacade(modelFacade);

    GIVEN("An LibertyFileData instance with a filepath, and a Library instance.")
    {
        LibView::model::data_structure::LibertyFileData* libertyFileData = new LibView::model::data_structure::LibertyFileData("path/x.exe");
        LibView::model::data_structure::Library* library = new LibView::model::data_structure::Library(modelFacade);
        modelFacade->addAttribute("");

        REQUIRE_NOTHROW(libertyFileData->getLibFileIndex());

        THEN("the filepath and name can be accesed through a getter")
        {
            REQUIRE(libertyFileData->getFilePath() == "path/x.exe");
            REQUIRE(libertyFileData->getDisplayName() == "x");
        }


        WHEN("libraryGroup is set again.")
        {
            REQUIRE_NOTHROW(libertyFileData->setLibraryGroup(library));

            THEN("the filepath and name can be accesed through a getter")
            {
                REQUIRE(libertyFileData->getLibraryGroup() == library);
            }
        }

        WHEN("m_LibFileContent is set again.")
        {
            REQUIRE_NOTHROW(libertyFileData->setFileContents("abc"));

            THEN("the filepath and name can be accesed through a getter")
            {
                REQUIRE(libertyFileData->getFileContents() == "abc");
            }
        }
        delete libertyFileData;
    }
    delete presenter;
}
#endif