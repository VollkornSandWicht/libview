
#ifdef LIBVIEW_TEST

#include "catch2/catch.hpp"


#include <iostream>


#include"LibView/model/data_structure/Library.h"
#include "LibView/model/ModelFacade.h"
#include "LibView/presenter/PresenterFacade.h"

SCENARIO("Library Values can be set through setters and get through getters.", "[LibView::model::data_structure::Library]")
{
	LibView::model::ModelFacade* modelFacade = new LibView::model::ModelFacade();
	LibView::presenter::PresenterFacade* presenter = new LibView::presenter::PresenterFacade(modelFacade);



	GIVEN("An empty Library instance.")
	{
		LibView::model::data_structure::Library library = LibView::model::data_structure::Library(modelFacade);
		modelFacade->addAttribute("");

		WHEN("delay_module is set.")
		{
			REQUIRE_NOTHROW(library.setDelay_model("table_lookup"));

			THEN("The set value can be returned through the getter method.")
			{
				REQUIRE(library.getDelay_model() == "table_lookup");
			}
		}

		WHEN("library_features is set.")
		{
			std::vector<std::string> temp;
			temp.push_back("a");
			temp.push_back("b");
			REQUIRE_NOTHROW(library.setLibrary_features(temp));

			THEN("The set value can be returned through the getter method.")
			{
				REQUIRE(library.getLibrary_features() == temp);
			}
		}

		WHEN("time_unit is set.")
		{
			REQUIRE_NOTHROW(library.setTime_unit("10ps"));

			THEN("The set value can be returned through the getter method.")
			{
				REQUIRE(library.getTime_unit() == "10ps");
			}
		}

		WHEN("current_unit is set.")
		{
			REQUIRE_NOTHROW(library.setCurrent_unit("1mA"));

			THEN("The set value can be returned through the getter method.")
			{
				REQUIRE(library.getCurrent_unit() == "1mA");
			}
		}

		WHEN("voltage_unit is set.")
		{
			REQUIRE_NOTHROW(library.setVoltage_unit("10mV"));

			THEN("The set value can be returned through the getter method.")
			{
				REQUIRE(library.getVoltage_unit() == "10mV");
			}
		}

		WHEN("pulling_resistance_unit is set.")
		{
			REQUIRE_NOTHROW(library.setPulling_resistance_unit("100ohm"));

			THEN("The set value can be returned through the getter method.")
			{
				REQUIRE(library.getPulling_resistance_unit() == "100ohm");
			}
		}

		WHEN("Capacitive_load_unit is set.")
		{
			REQUIRE_NOTHROW(library.setCapacitive_load_unit(5.9, "pf"));

			THEN("The set value can be returned through the getter method.")
			{
				LibView::model::data_structure::CapacitiveLoadUnitAttribute* temp;
				REQUIRE_NOTHROW(temp = library.getCapacitive_load_unit());
				REQUIRE(temp->getDoublePart() == 5.9);
				REQUIRE(temp->getStringPart() == "pf");
			}
		}

		WHEN("leakage_power_unit is set.")
		{
			REQUIRE_NOTHROW(library.setLeakage_power_unit("1nW"));

			THEN("The set value can be returned through the getter method.")
			{
				REQUIRE(library.getLeakage_power_unit() == "1nW");
			}
		}

		WHEN("input_threshold_pct_fall is set.")
		{
			REQUIRE_NOTHROW(library.setInput_threshold_pct_fall(6.9));

			THEN("The set value can be returned through the getter method.")
			{
				REQUIRE(library.getInput_threshold_pct_fall() == 6.9);
			}
		}

		WHEN("input_threshold_pct_rise is set.")
		{
			REQUIRE_NOTHROW(library.setInput_threshold_pct_rise(4.2));

			THEN("The set value can be returned through the getter method.")
			{
				REQUIRE(library.getInput_threshold_pct_rise() == 4.2);
			}
		}

		WHEN("output_threshold_pct_fall is set.")
		{
			REQUIRE_NOTHROW(library.setOutput_threshold_pct_fall(4.2));

			THEN("The set value can be returned through the getter method.")
			{
				REQUIRE(library.getOutput_threshold_pct_fall() == 4.2);
			}
		}

		WHEN("output_threshold_pct_rise is set.")
		{
			REQUIRE_NOTHROW(library.setOutput_threshold_pct_rise(4.2));

			THEN("The set value can be returned through the getter method.")
			{
				REQUIRE(library.getOutput_threshold_pct_rise() == 4.2);
			}
		}

		WHEN("slew_lower_threshold_pct_fall is set.")
		{
			REQUIRE_NOTHROW(library.setSlew_lower_threshold_pct_fall(4.2));

			THEN("The set value can be returned through the getter method.")
			{
				REQUIRE(library.getSlew_lower_threshold_pct_fall() == 4.2);
			}
		}

		WHEN("slew_lower_threshold_pct_rise is set.")
		{
			REQUIRE_NOTHROW(library.setSlew_lower_threshold_pct_rise(4.2));

			THEN("The set value can be returned through the getter method.")
			{
				REQUIRE(library.getSlew_lower_threshold_pct_rise() == 4.2);
			}
		}

		WHEN("slew_upper_threshold_pct_fall is set.")
		{
			REQUIRE_NOTHROW(library.setSlew_upper_threshold_pct_fall(4.2));

			THEN("The set value can be returned through the getter method.")
			{
				REQUIRE(library.getSlew_upper_threshold_pct_fall() == 4.2);
			}
		}

		WHEN("slew_upper_threshold_pct_rise is set.")
		{
			REQUIRE_NOTHROW(library.setSlew_upper_threshold_pct_rise(4.2));

			THEN("The set value can be returned through the getter method.")
			{
				REQUIRE(library.getSlew_upper_threshold_pct_rise() == 4.2);
			}
		}

		WHEN("slew_derate_from_library is set.")
		{
			REQUIRE_NOTHROW(library.setSlew_derate_from_library(4.2));

			THEN("The set value can be returned through the getter method.")
			{
				REQUIRE(library.getSlew_derate_from_library() == 4.2);
			}
		}

		WHEN("nom_process is set.")
		{
			REQUIRE_NOTHROW(library.setNom_process(4.2));

			THEN("The set value can be returned through the getter method.")
			{
				REQUIRE(library.getNom_process() == 4.2);
			}
		}

		WHEN("nom_temperature is set.")
		{
			REQUIRE_NOTHROW(library.setNom_temperature(4.2));

			THEN("The set value can be returned through the getter method.")
			{
				REQUIRE(library.getNom_temperature() == 4.2);
			}
		}

		WHEN("nom_voltage is set.")
		{
			REQUIRE_NOTHROW(library.setNom_voltage(4.2));

			THEN("The set value can be returned through the getter method.")
			{
				REQUIRE(library.getNom_voltage() == 4.2);
			}
		}

		WHEN("default_cell_leakage_power is set.")
		{
			REQUIRE_NOTHROW(library.setDefault_cell_leakage_power(4.2));

			THEN("The set value can be returned through the getter method.")
			{
				REQUIRE(library.getDefault_cell_leakage_power() == 4.2);
			}
		}

		WHEN("default_fanout_load is set.")
		{
			REQUIRE_NOTHROW(library.setDefault_fanout_load(4.2));

			THEN("The set value can be returned through the getter method.")
			{
				REQUIRE(library.getDefault_fanout_load() == 4.2);
			}
		}

		WHEN("default_inout_pin_cap is set.")
		{
			REQUIRE_NOTHROW(library.setDefault_inout_pin_cap(4.2));

			THEN("The set value can be returned through the getter method.")
			{
				REQUIRE(library.getDefault_inout_pin_cap() == 4.2);
			}
		}

		WHEN("default_input_pin_cap is set.")
		{
			REQUIRE_NOTHROW(library.setDefault_input_pin_cap(4.2));

			THEN("The set value can be returned through the getter method.")
			{
				REQUIRE(library.getDefault_input_pin_cap() == 4.2);
			}
		}

		WHEN("default_output_pin_cap is set.")
		{
			REQUIRE_NOTHROW(library.setDefault_output_pin_cap(4.2));

			THEN("The set value can be returned through the getter method.")
			{
				REQUIRE(library.getDefault_output_pin_cap() == 4.2);
			}
		}

		WHEN("default_leakage_power_density is set.")
		{
			REQUIRE_NOTHROW(library.setDefault_leakage_power_density(4.2));

			THEN("The set value can be returned through the getter method.")
			{
				REQUIRE(library.getDefault_leakage_power_density() == 4.2);
			}
		}
	}
	delete presenter;
}

#endif

