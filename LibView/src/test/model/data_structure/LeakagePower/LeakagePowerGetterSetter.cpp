
#ifdef LIBVIEW_TEST

#include "catch2/catch.hpp"

#include <iostream>

#include"LibView/model/data_structure/LeakagePower.h"
#include "LibView/model/ModelFacade.h"
#include "LibView/presenter/PresenterFacade.h"


SCENARIO("LeakagePower Values can be set through setters and get through getters.", "[LibView::model::data_structure::LeakagePower]")
{
    LibView::model::ModelFacade* modelFacade = new LibView::model::ModelFacade();
    LibView::presenter::PresenterFacade* presenter = new LibView::presenter::PresenterFacade(modelFacade);
    
    GIVEN("An empty LeakagePower instance.")
    {
        LibView::model::data_structure::LeakagePower leakagePower(modelFacade);
        
        REQUIRE_NOTHROW(leakagePower.setPath("path()"));

        WHEN("when is set.")
        {
            modelFacade->addAttribute("path(|A1&!A2|)");
            REQUIRE_NOTHROW(leakagePower.setWhen("A1&!A2"));

            THEN("The set value can be returned through the getter method.")
            {
                REQUIRE(leakagePower.getWhen() == "A1&!A2");
            }
        }

        WHEN("value is set.")
        {
            modelFacade->addAttribute("path()");
            REQUIRE_NOTHROW(leakagePower.setValue(4.2));

            THEN("The set value can be returned through the getter method.")
            {
                REQUIRE(leakagePower.getValue() == 4.2);
            }
        }
    }
    delete presenter;
}
#endif