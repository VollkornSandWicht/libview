
#ifdef LIBVIEW_TEST

#include "catch2/catch.hpp"

#include <iostream>

#include"LibView/model/data_structure/OperatingConditions.h"
#include "LibView/model/ModelFacade.h"
#include "LibView/presenter/PresenterFacade.h"

SCENARIO("OperatingConditions Values can be set through setters and get through getters.", "[LibView::model::data_structure::OperatingConditions]")
{
    LibView::model::ModelFacade* modelFacade = new LibView::model::ModelFacade();
    LibView::presenter::PresenterFacade* presenter = new LibView::presenter::PresenterFacade(modelFacade);


    GIVEN("An empty OperatingConditions instance.")
    {

        LibView::model::data_structure::OperatingConditions* operatingConditions = new LibView::model::data_structure::OperatingConditions(modelFacade);
        modelFacade->addAttribute("");

        WHEN("process is set.")
        {
            REQUIRE_NOTHROW(operatingConditions->setProcess(6.9));

            THEN("The set value can be returned through the getter method.")
            {
                REQUIRE(operatingConditions->getProcess() == 6.9);
            }
        }

        WHEN("temprature is set.")
        {
            REQUIRE_NOTHROW(operatingConditions->setTemperature(4.2));

            THEN("The set value can be returned through the getter method.")
            {
                REQUIRE(operatingConditions->getTemperature() == 4.2);
            }
        }

        WHEN("voltage is set.")
        {
            REQUIRE_NOTHROW(operatingConditions->setVoltage(42.0));

            THEN("The set value can be returned through the getter method.")
            {
                REQUIRE(operatingConditions->getVoltage() == 42.0);
            }
        }
        delete operatingConditions;
    }
    delete presenter;
}
#endif