
#ifdef LIBVIEW_TEST

#include "catch2/catch.hpp"

#include <iostream>

#include"LibView/model/data_structure/Timing.h"
#include "LibView/model/ModelFacade.h"
#include "LibView/presenter/PresenterFacade.h"

SCENARIO("Timing Values can be set through setters and get through getters.", "[LibView::model::data_structure::Timing]")
{
    LibView::model::ModelFacade* modelFacade = new LibView::model::ModelFacade();
    LibView::presenter::PresenterFacade* presenter = new LibView::presenter::PresenterFacade(modelFacade);


    GIVEN("An empty Timing instance.")
    {

        LibView::model::data_structure::Timing* timing = new LibView::model::data_structure::Timing(modelFacade);
        

        WHEN("timing_sense is set.")
        {
            modelFacade->addAttribute("|sense|");

            REQUIRE_NOTHROW(timing->setTiming_sense("sense"));

            THEN("The set value can be returned through the getter method.")
            {
                REQUIRE(timing->getTiming_sense() == "sense");
            }
        }

        WHEN("ziming_type is set.")
        {

            modelFacade->addAttribute("|type|");
            REQUIRE_NOTHROW(timing->setTiming_type("type"));

            THEN("The set value can be returned through the getter method.")
            {
                REQUIRE(timing->getTiming_type() == "type");
            }
        }

        WHEN("related_pin is set.")
        {
            modelFacade->addAttribute("|related|");

            REQUIRE_NOTHROW(timing->setRelated_pin("related"));

            THEN("The set value can be returned through the getter method.")
            {
                REQUIRE(timing->getRelated_pin() == "related");
            }
        }
        delete timing;
    }
    delete presenter;
}
#endif
