
#ifdef LIBVIEW_TEST

#include "catch2/catch.hpp"

#include <iostream>

#include"LibView/model/data_structure/TransitionTable.h"
#include "LibView/model/ModelFacade.h"
#include "LibView/presenter/PresenterFacade.h"

SCENARIO("TransitionTable Values can be set through setters and get through getters.", "[LibView::model::data_structure::TransitionTable]")
{
    LibView::model::ModelFacade* modelFacade = new LibView::model::ModelFacade();
    LibView::presenter::PresenterFacade* presenter = new LibView::presenter::PresenterFacade(modelFacade);

    GIVEN("An empty TransitionTable instance.")
    {
        LibView::model::data_structure::TransitionTable* transitionTable = new LibView::model::data_structure::TransitionTable(modelFacade);
        modelFacade->addAttribute("");

        LibView::model::DynamicDimensionArray* dda = new LibView::model::DynamicDimensionArray(2);
        dda->reserve(2);

        WHEN("intermediate_values is set.")
        {

            (*dda)[0] << 5;
            (*dda)[0] << 8;
            (*dda)[0] << 42;
            (*dda)[0] << 17;
            (*dda)[1] << 6;
            (*dda)[1] << 69;

            REQUIRE_NOTHROW(transitionTable->setIntermediate_values(dda));

            THEN("The set value can be returned through the getter method.")
            {
                REQUIRE(transitionTable->getIntermediate_values() == dda);
            }
        }
        delete transitionTable;
    }
    delete presenter;

}

#endif