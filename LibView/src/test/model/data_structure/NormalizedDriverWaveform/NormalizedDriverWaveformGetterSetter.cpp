
#ifdef LIBVIEW_TEST

#include "catch2/catch.hpp"

#include <iostream>

#include"LibView/model/data_structure/NormalizedDriverWaveform.h"
#include "LibView/model/ModelFacade.h"
#include "LibView/presenter/PresenterFacade.h"
#include "LibView/model/data_structure/DynamicDimensionArray.h"

SCENARIO("NormalizedDriverWaveform Values can be set through setters and get through getters.", "[LibView::model::data_structure::NormalizedDriverWaveform]")
{
    LibView::model::ModelFacade* modelFacade = new LibView::model::ModelFacade();
    LibView::presenter::PresenterFacade* presenter = new LibView::presenter::PresenterFacade(modelFacade);


    GIVEN("An empty NormalizedDriverWaveform instance.")
    {

        LibView::model::data_structure::NormalizedDriverWaveform* ndw = new LibView::model::data_structure::NormalizedDriverWaveform(modelFacade);
        

        //LibView::model::data_structure::Cell cell = LibView::model::data_structure::Cell(nullptr);
        LibView::model::DynamicDimensionArray* dda2 = new  LibView::model::DynamicDimensionArray(2);
        dda2->reserve(2);
        LibView::model::DynamicDimensionArray* dda = new LibView::model::DynamicDimensionArray(1);
        WHEN("driver_waveform_name is set.")
        {
            modelFacade->addAttribute("|driver_waveform|");
            REQUIRE_NOTHROW(ndw->setDriver_waveform_name("driver_waveform"));

            THEN("The set value can be returned through the getter method.")
            {
                REQUIRE(ndw->getDriver_waveform_name() == "driver_waveform");
            }
        }

        WHEN("index_1 is set.")
        {
            modelFacade->addAttribute("");
            *dda << 5 << 9 << 10;
            REQUIRE_NOTHROW(ndw->setIndex_1(dda));

            THEN("The set value can be returned through the getter method.")
            {
                REQUIRE(ndw->getIndex_1() == dda);
            }
        }

        WHEN("index_2 is set.")
        {
            modelFacade->addAttribute("");
            *dda << 1 << 7 << 17;
            REQUIRE_NOTHROW(ndw->setIndex_2(dda));

            THEN("The set value can be returned through the getter method.")
            {
                REQUIRE(ndw->getIndex_2() == dda);
            }
        }

        WHEN("values is set.")
        {
            modelFacade->addAttribute("");
            (*dda2)[0] << 1 << 7 << 17;
            (*dda2)[1] << 8 << 0 << 3;
            REQUIRE_NOTHROW(ndw->setValues(dda2));

            THEN("The set value can be returned through the getter method.")
            {
                REQUIRE(ndw->getValues() == dda2);
            }
        }
        delete ndw;
    }
    delete presenter;
}
#endif
