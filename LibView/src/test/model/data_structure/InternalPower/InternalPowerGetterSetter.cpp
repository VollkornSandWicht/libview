
#ifdef LIBVIEW_TEST

#include "catch2/catch.hpp"

#include <iostream>

#include"LibView/model/data_structure/InternalPower.h"
#include "LibView/model/ModelFacade.h"
#include "LibView/presenter/PresenterFacade.h"


SCENARIO("InternalPower Values can be set through setters and get through getters.", "[LibView::model::data_structure::InternalPower]")
{
    LibView::model::ModelFacade* modelFacade = new LibView::model::ModelFacade();
    LibView::presenter::PresenterFacade* presenter = new LibView::presenter::PresenterFacade(modelFacade);
    
    GIVEN("An Ff instance, with set path")
    {
        LibView::model::data_structure::InternalPower internalPower(modelFacade);
        modelFacade->addAttribute("path(|a|)");

        REQUIRE_NOTHROW(internalPower.setPath("path()"));

        WHEN("clocked_on is set.")
        {
            REQUIRE_NOTHROW(internalPower.setRelated_pin("a"));

            THEN("The set value can be returned through the getter method.")
            {
                REQUIRE(internalPower.getRelated_pin() == "a");
            }
        }
    }
    delete presenter;
}
#endif
