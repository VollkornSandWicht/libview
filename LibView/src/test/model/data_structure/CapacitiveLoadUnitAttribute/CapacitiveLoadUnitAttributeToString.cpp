#ifdef LIBVIEW_TEST

#include "catch2/catch.hpp"

#include <iostream>

#include"LibView/model/data_structure/CapacitiveLoadUnitAttribute.h"


SCENARIO("The CapacitiveLoadUnitAttribute class can be returned as a string in the form of the represented attribute.", "[LibView::model::data_structure::CapacitiveLoadUnitAttribute]")
{
    GIVEN("An empty CapacitiveLoadUnitAttribute instance.")
    {
        LibView::model::data_structure::CapacitiveLoadUnitAttribute clua;

        REQUIRE_NOTHROW(clua.setDoublePart(4.2));

        REQUIRE_NOTHROW(clua.setStringPart("kill_it_with_fire"));

        THEN("class can be returned as string.")
        {
            REQUIRE(clua.toString() == "capacitive_load_unit(4.2, kill_it_with_fire);");
        }
    }
}
#endif