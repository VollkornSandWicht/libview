#ifdef LIBVIEW_TEST

#include "catch2/catch.hpp"

#include <iostream>

#include"LibView/model/data_structure/CapacitiveLoadUnitAttribute.h"


SCENARIO("CapacitiveLoadUnitAttribute Values can be set through setters and get through getters.", "[LibView::model::data_structure::CapacitiveLoadUnitAttribute]")
{
    GIVEN("An empty CapacitiveLoadUnitAttribute instance.")
    {
        LibView::model::data_structure::CapacitiveLoadUnitAttribute clua;

        WHEN("doublePart is set.")
        {
            REQUIRE_NOTHROW(clua.setDoublePart(4.2));

            THEN("The set value can be returned through the getter method.")
            {
                REQUIRE(clua.getDoublePart() == 4.2);
            }
        }

        WHEN("stringPart is set.")
        {
            REQUIRE_NOTHROW(clua.setStringPart("kill_it_with_fire"));

            THEN("The set value can be returned through the getter method.")
            {
                REQUIRE(clua.getStringPart() == "kill_it_with_fire");
            }
        }
    }
}
#endif