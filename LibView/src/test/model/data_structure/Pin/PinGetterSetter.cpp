
#ifdef LIBVIEW_TEST

#include "catch2/catch.hpp"

#include <iostream>

#include"LibView/model/data_structure/Pin.h"
#include "LibView/model/ModelFacade.h"
#include "LibView/presenter/PresenterFacade.h"

SCENARIO("Pin Values can be set through setters and get through getters.", "[LibView::model::data_structure::Pin]")
{
    LibView::model::ModelFacade* modelFacade = new LibView::model::ModelFacade();
    LibView::presenter::PresenterFacade* presenter = new LibView::presenter::PresenterFacade(modelFacade);


    GIVEN("An empty Pin instance.")
    {

        LibView::model::data_structure::Pin* pin = new LibView::model::data_structure::Pin(modelFacade);
        modelFacade->addAttribute("");

        //LibView::model::data_structure::Cell cell = LibView::model::data_structure::Cell(nullptr);

        WHEN("capacitance is set.")
        {
            REQUIRE_NOTHROW(pin->setCapacitance(0.8));

            THEN("The set value can be returned through the getter method.")
            {
                REQUIRE(pin->getCapacitance() == 0.8);
            }
        }

        WHEN("direction is set.")
        {
            REQUIRE_NOTHROW(pin->setDirection("in"));

            THEN("The set value can be returned through the getter method.")
            {
                REQUIRE(pin->getDirection() == "in");
            }
        }

        WHEN("driver_waveform_rise is set.")
        {
            REQUIRE_NOTHROW(pin->setDriver_waveform_rise("fall"));

            THEN("The set value can be returned through the getter method.")
            {
                REQUIRE(pin->getDriver_waveform_rise() == "fall");
            }
        }

        WHEN("driver_waveform_fall is set.")
        {
            REQUIRE_NOTHROW(pin->setDriver_waveform_fall("rise"));

            THEN("The set value can be returned through the getter method.")
            {
                REQUIRE(pin->getDriver_waveform_fall() == "rise");
            }
        }

        WHEN("getInput_voltage is set.")
        {
            REQUIRE_NOTHROW(pin->setInput_voltage("1V"));

            THEN("The set value can be returned through the getter method.")
            {
                REQUIRE(pin->getInput_voltage() == "1V");
            }
        }

        WHEN("max_capacitance is set.")
        {
            REQUIRE_NOTHROW(pin->setMax_capacitance(1.7));

            THEN("The set value can be returned through the getter method.")
            {
                REQUIRE(pin->getMax_capacitance() == 1.7);
            }
        }

        WHEN("min_capacitance is set.")
        {
            REQUIRE_NOTHROW(pin->setMin_capacitance(0.5));

            THEN("The set value can be returned through the getter method.")
            {
                REQUIRE(pin->getMin_capacitance() == 0.5);
            }
        }

        WHEN("output_voltage is set.")
        {
            REQUIRE_NOTHROW(pin->setOutput_voltage("input"));

            THEN("The set value can be returned through the getter method.")
            {
                REQUIRE(pin->getOutput_voltage() == "input");
            }
        }

        WHEN("function is set.")
        {
            REQUIRE_NOTHROW(pin->setFunction("a + b = c"));

            THEN("The set value can be returned through the getter method.")
            {
                REQUIRE(pin->getFunction() == "a + b = c");
            }
        }

        WHEN("clock is set.")
        {
            REQUIRE_NOTHROW(pin->setClock("e = m * c^2"));

            THEN("The set value can be returned through the getter method.")
            {
                REQUIRE(pin->getClock() == "e = m * c^2");
            }
        }

        WHEN("three_state is set.")
        {
            REQUIRE_NOTHROW(pin->setThree_state("infinity"));

            THEN("The set value can be returned through the getter method.")
            {
                REQUIRE(pin->getThree_state() == "infinity");
            }
        }

        WHEN("nextstate_type is set.")
        {
            REQUIRE_NOTHROW(pin->setNextstate_type("-infinity"));

            THEN("The set value can be returned through the getter method.")
            {
                REQUIRE(pin->getNextstate_type() == "-infinity");
            }
        }

        WHEN("min_pulse_width_low is set.")
        {
            REQUIRE_NOTHROW(pin->setMin_pulse_width_low(3));

            THEN("The set value can be returned through the getter method.")
            {
                REQUIRE(pin->getMin_pulse_width_low() == 3);
            }
        }

        WHEN("min_pulse_width_high is set.")
        {
            REQUIRE_NOTHROW(pin->setMin_pulse_width_high(1000));

            THEN("The set value can be returned through the getter method.")
            {
                REQUIRE(pin->getMin_pulse_width_high() == 1000);
            }
        }
        delete pin;
    }
    delete presenter;
}
#endif
