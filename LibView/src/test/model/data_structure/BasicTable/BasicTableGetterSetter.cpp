
#ifdef LIBVIEW_TEST

#include "catch2/catch.hpp"

#include <iostream>

#include"LibView/model/data_structure/BasicTable.h"
#include "LibView/model/ModelFacade.h"
#include "LibView/presenter/PresenterFacade.h"

SCENARIO("BasicTable Values can be set through setters and get through getters.", "[LibView::model::data_structure::BasicTable]")
{
    LibView::model::ModelFacade* modelFacade = new LibView::model::ModelFacade();
    LibView::presenter::PresenterFacade* presenter = new LibView::presenter::PresenterFacade(modelFacade);

    GIVEN("An empty BasicTable instance.")
    {
        LibView::model::data_structure::BasicTable* basicTable = new LibView::model::data_structure::BasicTable(modelFacade);
        modelFacade->addAttribute("");

        LibView::model::DynamicDimensionArray* dda = new LibView::model::DynamicDimensionArray(2);
        dda->reserve(2);


        WHEN("values is set.")
        {

            (*dda)[0] << 5;
            (*dda)[0] << 8;
            (*dda)[0] << 42;
            (*dda)[0] << 17;
            (*dda)[1] << 6;
            (*dda)[1] << 69;

            REQUIRE_NOTHROW(basicTable->setValues(dda));

            THEN("The set value can be returned through the getter method.")
            {
                REQUIRE(basicTable->getValues() == dda);
            }
        }
        delete basicTable;
    }
    delete presenter;

}

#endif