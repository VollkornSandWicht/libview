
#ifdef LIBVIEW_TEST

#include "catch2/catch.hpp"

#include <iostream>

#include"LibView/model/data_structure/Latch.h"
#include "LibView/model/ModelFacade.h"
#include "LibView/presenter/PresenterFacade.h"

SCENARIO("Latch Values can be set through setters and get through getters.", "[LibView::model::data_structure::Latch]")
{
    LibView::model::ModelFacade* modelFacade = new LibView::model::ModelFacade();
    LibView::presenter::PresenterFacade* presenter = new LibView::presenter::PresenterFacade(modelFacade);


    GIVEN("An empty Latch instance.")
    {

        LibView::model::data_structure::Latch* latch = new LibView::model::data_structure::Latch(modelFacade);
        modelFacade->addAttribute("");

        //LibView::model::data_structure::Cell cell = LibView::model::data_structure::Cell(nullptr);

        WHEN("enable is set.")
        {
            REQUIRE_NOTHROW(latch->setEnable("abc"));

            THEN("The set value can be returned through the getter method.")
            {
                REQUIRE(latch->getEnable() == "abc");
            }
        }

        WHEN("data_in is set.")
        {
            REQUIRE_NOTHROW(latch->setData_in("abc"));

            THEN("The set value can be returned through the getter method.")
            {
                REQUIRE(latch->getData_in() == "abc");
            }
        }

        delete latch;
    }
    delete presenter;
}
#endif
