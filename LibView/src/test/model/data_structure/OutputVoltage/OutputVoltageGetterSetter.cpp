
#ifdef LIBVIEW_TEST


#include "catch2/catch.hpp"

#include <iostream>

#include"LibView/model/data_structure/OutputVoltage.h"
#include"LibView/model/data_structure/LibertyFileGroup.h"
#include"LibView/model/data_structure/Library.h"
#include "LibView/model/ModelFacade.h"
#include "LibView/presenter/PresenterFacade.h"


SCENARIO("OutputVoltage Values can be set through setters and get through getters.", "[LibView::model::data_structure::OutputVoltage]")
{
    LibView::model::ModelFacade* modelFacade = new LibView::model::ModelFacade();
    LibView::presenter::PresenterFacade* presenter = new LibView::presenter::PresenterFacade(modelFacade);


    GIVEN("An empty OutputVoltage instance, and a Library Parent with voltage_unit set to 100mV")
    {
        LibView::model::data_structure::OutputVoltage* outputVoltage = new LibView::model::data_structure::OutputVoltage(modelFacade);

        LibView::model::data_structure::Library* library = new LibView::model::data_structure::Library(modelFacade);
        modelFacade->addAttribute("");

        REQUIRE_NOTHROW(library->setVoltage_unit("100mV"));

        REQUIRE_NOTHROW(outputVoltage->setParent(library));




        WHEN("vol is set.")
        {
            REQUIRE_NOTHROW(outputVoltage->setVol("VDD + 0.8"));

            THEN("The set value can be returned through the getter method.")
            {
                REQUIRE(outputVoltage->getVol()->getAsDouble() == 50.8);

                REQUIRE(outputVoltage->getVol()->getAsString() == "VDD + 0.8");
            }
        }
        delete library;
    }

    GIVEN("An empty outputVoltage instance, and a Library Parent with voltage_unit set to 10mV")
    {
        LibView::model::data_structure::OutputVoltage* outputVoltage = new LibView::model::data_structure::OutputVoltage(modelFacade);

        LibView::model::data_structure::Library* library = new LibView::model::data_structure::Library(modelFacade);
        modelFacade->addAttribute("");

        REQUIRE_NOTHROW(library->setVoltage_unit("10mV"));

        REQUIRE_NOTHROW(outputVoltage->setParent(library));




        WHEN("vomax is set.")
        {
            REQUIRE_NOTHROW(outputVoltage->setVomax("VCC"));

            THEN("The set value can be returned through the getter method.")
            {
                REQUIRE(outputVoltage->getVomax()->getAsDouble() == 500);

                REQUIRE(outputVoltage->getVomax()->getAsString() == "VCC");
            }
        }
        delete library;
    }

    GIVEN("An empty outputVoltage instance, and a Library Parent with voltage_unit set to 1mV")
    {
        LibView::model::data_structure::OutputVoltage* outputVoltage = new LibView::model::data_structure::OutputVoltage(modelFacade);
        modelFacade->addAttribute("");


        LibView::model::data_structure::Library* library = new LibView::model::data_structure::Library(modelFacade);
        modelFacade->addAttribute("");

        REQUIRE_NOTHROW(library->setVoltage_unit("1mV"));

        REQUIRE_NOTHROW(outputVoltage->setParent(library));

        library->setChild(outputVoltage);

        WHEN("vomin is set.")
        {
            REQUIRE_NOTHROW(outputVoltage->setVomin("4.2 + VDD"));

            THEN("The set value can be returned through the getter method.")
            {
                REQUIRE(outputVoltage->getVomin()->getAsDouble() == 5004.2);

                REQUIRE(outputVoltage->getVomin()->getAsString() == "4.2 + VDD");
            }
        }
        delete library;
    }

    GIVEN("An empty outputVoltage instance, and a Library Parent with voltage_unit set to 1V")
    {
        LibView::model::data_structure::OutputVoltage* outputVoltage = new LibView::model::data_structure::OutputVoltage(modelFacade);

        LibView::model::data_structure::Library* library = new LibView::model::data_structure::Library(modelFacade);
        modelFacade->addAttribute("");

        REQUIRE_NOTHROW(library->setVoltage_unit("1V"));

        REQUIRE_NOTHROW(outputVoltage->setParent(library));

        WHEN("voh is set.")
        {
            REQUIRE_NOTHROW(outputVoltage->setVoh("VDD + 0.8"));

            THEN("The set value can be returned through the getter method.")
            {
                REQUIRE(outputVoltage->getVoh()->getAsDouble() == 5.8);

                REQUIRE(outputVoltage->getVoh()->getAsString() == "VDD + 0.8");
            }
        }
        delete outputVoltage;
    }
    delete presenter;
}
#endif
