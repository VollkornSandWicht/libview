
#ifdef LIBVIEW_TEST

#include "catch2/catch.hpp"

#include <iostream>

#include"LibView/model/data_structure/PowerLutTemplate.h"
#include "LibView/model/ModelFacade.h"
#include "LibView/presenter/PresenterFacade.h"

SCENARIO("PowerLutTemplate Values can be set through setters and get through getters.", "[LibView::model::data_structure::PowerLutTemplate]")
{
    LibView::model::ModelFacade* modelFacade = new LibView::model::ModelFacade();
    LibView::presenter::PresenterFacade* presenter = new LibView::presenter::PresenterFacade(modelFacade);


    GIVEN("An empty PowerLutTemplate instance.")
    {

        LibView::model::data_structure::PowerLutTemplate* plt = new LibView::model::data_structure::PowerLutTemplate(modelFacade);
        modelFacade->addAttribute("");

        WHEN("variable_1 is set.")
        {
            REQUIRE_NOTHROW(plt->setVariable_1("one"));

            THEN("The set value can be returned through the getter method.")
            {
                REQUIRE(plt->getVariable_1() == "one");
            }
        }

        WHEN("variable_2 is set.")
        {
            REQUIRE_NOTHROW(plt->setVariable_2("two"));

            THEN("The set value can be returned through the getter method.")
            {
                REQUIRE(plt->getVariable_2() == "two");
            }
        }

        WHEN("variable_3 is set.")
        {
            REQUIRE_NOTHROW(plt->setVariable_3("three"));

            THEN("The set value can be returned through the getter method.")
            {
                REQUIRE(plt->getVariable_3() == "three");
            }
        }
        delete plt;
    }
    delete presenter;
}
#endif
