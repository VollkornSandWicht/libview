
#ifdef LIBVIEW_TEST

#include "catch2/catch.hpp"

#include <iostream>

#include"LibView/model/data_structure/LuTableTemplate.h"
#include "LibView/model/ModelFacade.h"
#include "LibView/presenter/PresenterFacade.h"

SCENARIO("LuTableTemplate Values can be set through setters and get through getters.", "[LibView::model::data_structure::LuTableTemplate]")
{
    LibView::model::ModelFacade* modelFacade = new LibView::model::ModelFacade();
    LibView::presenter::PresenterFacade* presenter = new LibView::presenter::PresenterFacade(modelFacade);


    GIVEN("An empty LuTableTemplate instance.")
    {

        LibView::model::data_structure::LuTableTemplate* lut = new LibView::model::data_structure::LuTableTemplate(modelFacade);
        modelFacade->addAttribute("");

        //LibView::model::data_structure::Cell cell = LibView::model::data_structure::Cell(nullptr);

        WHEN("variable_1 is set.")
        {
            REQUIRE_NOTHROW(lut->setVariable_1("55"));

            THEN("The set value can be returned through the getter method.")
            {
                REQUIRE(lut->getVariable_1() == "55");
            }
        }

        WHEN("variable_2 is set.")
        {
            REQUIRE_NOTHROW(lut->setVariable_2("145"));

            THEN("The set value can be returned through the getter method.")
            {
                REQUIRE(lut->getVariable_2() == "145");
            }
        }

        WHEN("variable_3 is set.")
        {
            REQUIRE_NOTHROW(lut->setVariable_3("31"));

            THEN("The set value can be returned through the getter method.")
            {
                REQUIRE(lut->getVariable_3() == "31");
            }
        }
        delete lut;
    }
    delete presenter;
}
#endif
