#if 1
#ifdef LIBVIEW_TEST

#include "catch2/catch.hpp"

#include <iostream>

#include "LibView/model/data_structure/DynamicDimensionArray.h"


SCENARIO("DynamicDimensionArray Values can be set through setters and get through getters.", "[LibView::model::DynamicDimensionArray]")
{

    WHEN("values are pushed into the array.")//TODO
    {
        LibView::model::DynamicDimensionArray* dda = new LibView::model::DynamicDimensionArray(2);
        dda->reserve(2);
        (*dda)[0] << 5 << 9 << 3;
        (*dda)[1] << 420 << 69 << 35;

        THEN("they can be accessed through get.")
        {
            REQUIRE(dda->toString() == "[ 5.000000, 9.000000, 3.000000 ]\n[ 420.000000, 69.000000, 35.000000 ]\n");
            REQUIRE_THROWS(dda->get());

            const LibView::model::DynamicDimensionArray cdda;
            LibView::model::DynamicDimensionArray* copy = new LibView::model::DynamicDimensionArray(cdda);

            const LibView::model::DynamicDimensionArray cdda1(1);
            LibView::model::DynamicDimensionArray* copy1 = new LibView::model::DynamicDimensionArray(cdda1);
            REQUIRE(cdda.toString() == copy->toString());
            REQUIRE(cdda1.toString() == copy1->toString());

            delete copy;
        }
        delete dda;
    }

    WHEN("default constructor is called.")
    {
        LibView::model::DynamicDimensionArray* dda = new LibView::model::DynamicDimensionArray;

        const LibView::model::DynamicDimensionArray cdda;


        THEN("m_dimension == 0")
        {
            REQUIRE(dda->getDimension() == 0);
            REQUIRE(dda->get() == 0);

        }

        delete dda;
    }

    WHEN("there is a 3d array")
    {
        LibView::model::DynamicDimensionArray* dda = new LibView::model::DynamicDimensionArray(3);
        dda->reserve(5);

        THEN("m_Dimension at (*dda)[2] == 2")
        {
            LibView::model::DynamicDimensionArray& temp = dda->getPosition(2);
            REQUIRE(temp.getDimension() == 2);
            REQUIRE_THROWS(dda->setScalar(4));
        }

        delete dda;
    }

    WHEN("DynamicDimensionArray is a scalar")
    {
        LibView::model::DynamicDimensionArray* dda = new LibView::model::DynamicDimensionArray;

        THEN("m_Dimension at (*dda)[2] == 2")
        {
            REQUIRE_THROWS(dda->getPosition(5));
            REQUIRE_THROWS((*dda)[0]);
            REQUIRE_THROWS(*dda << 5);
            REQUIRE_THROWS(dda->reserve(2));

        }

        delete dda;
    }

    WHEN("")
    {
        LibView::model::DynamicDimensionArray* ddas = new LibView::model::DynamicDimensionArray;

        const LibView::model::DynamicDimensionArray cddas;

        LibView::model::DynamicDimensionArray* ddaa = new LibView::model::DynamicDimensionArray(2);

        const LibView::model::DynamicDimensionArray cddaa(2);

        THEN("")
        {
            REQUIRE_NOTHROW(ddas->operator=(cddas));
            REQUIRE_NOTHROW(ddas->operator=(cddaa));

        }


    }

    WHEN("array with entries is copied")
    {
        LibView::model::DynamicDimensionArray* dda = new LibView::model::DynamicDimensionArray(2);

        dda->reserve(2);
        (*dda)[0] << 5 << 42;

        LibView::model::DynamicDimensionArray* copy = new LibView::model::DynamicDimensionArray(*dda);

        THEN("copy")
        {

            REQUIRE(copy->toString() == "[ 5.000000, 42.000000 ]\n[  ]\n");
            REQUIRE_NOTHROW(dda->operator=(*copy));
        }
    }
    //std::cout << "test" << std::endl;

}

#endif
#endif