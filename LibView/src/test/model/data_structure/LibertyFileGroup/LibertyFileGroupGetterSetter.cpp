
#ifdef LIBVIEW_TEST

#include "catch2/catch.hpp"


#include <iostream>


#include "LibView/model/data_structure/LibertyFileGroup.h"
#include "LibView/model/data_structure/Library.h"
#include "LibView/model/ModelFacade.h"
#include "LibView/presenter/PresenterFacade.h"

SCENARIO("LibertyFileGroup Values can be set through setters and get through getters.", "[LibView::model::data_structure::LibertyFileGroup]")
{
    LibView::model::ModelFacade* modelFacade = new LibView::model::ModelFacade();
    LibView::presenter::PresenterFacade* presenter = new LibView::presenter::PresenterFacade(modelFacade);

    GIVEN("LibertyFileGroup class with path.")
    {
        LibView::model::data_structure::LibertyFileGroup* lfg = new LibView::model::data_structure::LibertyFileGroup(modelFacade);
        

        REQUIRE_NOTHROW(lfg->setPath("lfg()"));

        WHEN("name is set.")
        {
            modelFacade->addAttribute("lfg(typical)");

            REQUIRE_NOTHROW(lfg->setName("typical"));

            THEN("The set value can be returned through the getter method.")
            {
                REQUIRE(lfg->getName() == "typical");
            }
        }

        WHEN("path is set.")
        {
            modelFacade->addAttribute("library()/random()/idk()");
            REQUIRE_NOTHROW(lfg->setPath("library()/random()/idk()"));

            THEN("The set value can be returned through the getter method.")
            {
                REQUIRE(lfg->getPath() == "library()/random()/idk()");
            }
        }

        WHEN("parent is set.")
        {
            modelFacade->addAttribute("lfg()");
            LibView::model::data_structure::Library* library = new LibView::model::data_structure::Library(modelFacade);
            modelFacade->addAttribute("");


            REQUIRE_NOTHROW(lfg->setParent(library));

            THEN("The set value can be returned through the getter method.")
            {
                REQUIRE(lfg->getParent() == library);
            }

            delete library;
        }

        WHEN("child is set.")
        {
            modelFacade->addAttribute("lfg()");

            LibView::model::data_structure::Library* library = new LibView::model::data_structure::Library(modelFacade);
            LibView::model::data_structure::Library* library2 = new LibView::model::data_structure::Library(modelFacade);
            LibView::model::data_structure::Library* library3 = new LibView::model::data_structure::Library(modelFacade);

            modelFacade->addAttribute("lfg()/one(a)");
            modelFacade->addAttribute("lfg()/two(b)");
            modelFacade->addAttribute("lfg()/three(c)");

            library->setPath("lfg()/one(a)");
            library2->setPath("lfg()/two(b)");
            library3->setPath("lfg()/three(c)");

            REQUIRE_NOTHROW(lfg->setChild(library));
            REQUIRE_NOTHROW(lfg->setChild(library2));
            REQUIRE_NOTHROW(lfg->setChild(library3));

            std::vector< LibView::model::data_structure::LibertyFileGroup*> libFileVector;

            libFileVector.push_back(library);
            libFileVector.push_back(library2);
            libFileVector.push_back(library3);

            THEN("The set value can be returned through the getter method.")
            {
                REQUIRE(lfg->getChild("a") == library);
                REQUIRE(lfg->getChild("b") == library2);
                REQUIRE(lfg->getChild("c") == library3);
                REQUIRE(lfg->getChild("d") == nullptr);

                REQUIRE(lfg->getChildren() == libFileVector);
            }
        }

        WHEN("path is set.")
        {
            modelFacade->addAttribute("lfg()");

            std::string temp1 = "ua";
            std::string temp2 = "ua2";
            std::string value1 = "42) ";
            std::string value2 = " : 420; ";

            REQUIRE_NOTHROW(lfg->setUndifinedAttribute(temp1, value1));
            REQUIRE_NOTHROW(lfg->setUndifinedAttribute(temp2, value2));


            std::vector<std::string> namesVector;
            std::vector<std::string> valuesVector;

            namesVector.push_back(temp1);
            namesVector.push_back(temp2);

            valuesVector.push_back("42");
            valuesVector.push_back("420");

            THEN("The set value can be returned through the getter method.")
            {
                REQUIRE(lfg->getUndifinedAttribute(temp1) == value1);
                REQUIRE(lfg->getUndifinedAttribute(temp2) == value2);
                REQUIRE(lfg->getOrderedAttributes() == namesVector);
                REQUIRE(lfg->getOrderedAttributeValues() == valuesVector);
            }
        }

        WHEN("parent is set.")
        {
            modelFacade->addAttribute("lfg()");
            LibView::model::data_structure::LibertyFileGroup* parent = new LibView::model::data_structure::LibertyFileGroup(modelFacade);
            modelFacade->addAttribute("");

            REQUIRE_NOTHROW(lfg->setParent(parent));

            THEN("parent can be accesed over the getter.")
            {
                //lfg->setPath("");
                REQUIRE(lfg->getParent() == parent);    
            }
        }

        WHEN("path exists.")
        {
            modelFacade->addAttribute("a()/b()/c(|d|e)");
            lfg->setPath("a()/b()/c(|d|e)");

            THEN("a cleaned up name without added parts to make it unique can be accessed.")
            {
                REQUIRE(lfg->getDisplayName() == "c(e)");
            }
        }
    }
    delete presenter;
}
#endif
