
#ifdef LIBVIEW_TEST

#include "catch2/catch.hpp"

#include <iostream>

#include "LibView/model/data_structure/IndexTemplate.h"
#include "LibView/model/data_structure/DynamicDimensionArray.h"
#include "LibView/model/ModelFacade.h"
#include "LibView/presenter/PresenterFacade.h"

SCENARIO("IndexTemplate Values can be set through setters and get through getters.", "[LibView::model::data_structure::IndexTemplate]")
{
    LibView::model::ModelFacade* modelFacade = new LibView::model::ModelFacade();
    LibView::presenter::PresenterFacade* presenter = new LibView::presenter::PresenterFacade(modelFacade);


    GIVEN("An empty Cell instance.")
    {

        LibView::model::data_structure::IndexTemplate* indexTemplate = new LibView::model::data_structure::IndexTemplate(modelFacade);
        modelFacade->addAttribute("");

        LibView::model::DynamicDimensionArray* dda1 = new LibView::model::DynamicDimensionArray(1);
        LibView::model::DynamicDimensionArray* dda2 = new LibView::model::DynamicDimensionArray(1);
        LibView::model::DynamicDimensionArray* dda3 = new LibView::model::DynamicDimensionArray(1);

        *dda1 << 42;
        *dda2 << 55 << 420;
        *dda3 << 69;

        WHEN("index_1 to index_3 are set.")
        {
            REQUIRE_NOTHROW(indexTemplate->setIndex_1(dda1));
            REQUIRE_NOTHROW(indexTemplate->setIndex_2(dda2));
            REQUIRE_NOTHROW(indexTemplate->setIndex_3(dda3));

            THEN("The set value can be returned through the getter method.")
            {
                REQUIRE(indexTemplate->getIndex_1()->toString() == "[ 42.000000 ]");
                REQUIRE(indexTemplate->getIndex_2()->toString() == "[ 55.000000, 420.000000 ]");
                REQUIRE(indexTemplate->getIndex_3()->toString() == "[ 69.000000 ]");
            }

            THEN("The Diminesion of a indext can be accessed through getDimensionOfIndex_X.")
            {
                REQUIRE(indexTemplate->getDimensionOfIndex_1() == 1);
                REQUIRE(indexTemplate->getDimensionOfIndex_2() == 2);
                REQUIRE(indexTemplate->getDimensionOfIndex_3() == 1);
            }
        }
        delete indexTemplate;
    }
    delete presenter;
}
#endif

