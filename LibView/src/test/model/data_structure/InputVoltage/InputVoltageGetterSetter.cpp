
#ifdef LIBVIEW_TEST


#include "catch2/catch.hpp"

#include <iostream>

#include"LibView/model/data_structure/InputVoltage.h"
#include"LibView/model/data_structure/LibertyFileGroup.h"
#include"LibView/model/data_structure/Library.h"
#include "LibView/model/ModelFacade.h"
#include "LibView/presenter/PresenterFacade.h"


SCENARIO("InputVoltage Values can be set through setters and get through getters.", "[LibView::model::data_structure::InputVoltage]")
{
    LibView::model::ModelFacade* modelFacade = new LibView::model::ModelFacade();
    LibView::presenter::PresenterFacade* presenter = new LibView::presenter::PresenterFacade(modelFacade);


    GIVEN("An empty InputVoltage instance, and a Library Parent with voltage_unit set to 100mV")
    {
        LibView::model::data_structure::InputVoltage* inputVoltage = new LibView::model::data_structure::InputVoltage(modelFacade);

        LibView::model::data_structure::Library* library = new LibView::model::data_structure::Library(modelFacade);
        modelFacade->addAttribute("");

        REQUIRE_NOTHROW(library->setVoltage_unit("100mV"));

        REQUIRE_NOTHROW(inputVoltage->setParent(library));


        

        WHEN("vil is set.")
        {
            REQUIRE_NOTHROW(inputVoltage->setVil("VDD + 0.8"));

            THEN("The set value can be returned through the getter method.")
            {
                REQUIRE(inputVoltage->getVil()->getAsDouble() == 50.8);

                REQUIRE(inputVoltage->getVil()->getAsString() == "VDD + 0.8");
            }
        }
        delete library;
    }

    GIVEN("An empty InputVoltage instance, and a Library Parent with voltage_unit set to 10mV")
    {
        LibView::model::data_structure::InputVoltage* inputVoltage = new LibView::model::data_structure::InputVoltage(modelFacade);

        LibView::model::data_structure::Library* library = new LibView::model::data_structure::Library(modelFacade);
        modelFacade->addAttribute("");

        REQUIRE_NOTHROW(library->setVoltage_unit("10mV"));

        REQUIRE_NOTHROW(inputVoltage->setParent(library));


        

        WHEN("vimax is set.")
        {
            REQUIRE_NOTHROW(inputVoltage->setVimax("VCC"));

            THEN("The set value can be returned through the getter method.")
            {
                REQUIRE(inputVoltage->getVimax()->getAsDouble() == 500);

                REQUIRE(inputVoltage->getVimax()->getAsString() == "VCC");
            }
        }
        delete library;
    }

    GIVEN("An empty InputVoltage instance, and a Library Parent with voltage_unit set to 1mV")
    {
        LibView::model::data_structure::InputVoltage* inputVoltage = new LibView::model::data_structure::InputVoltage(modelFacade);
        modelFacade->addAttribute("");


        LibView::model::data_structure::Library* library = new LibView::model::data_structure::Library(modelFacade);
        modelFacade->addAttribute("");

        REQUIRE_NOTHROW(library->setVoltage_unit("1mV"));

        REQUIRE_NOTHROW(inputVoltage->setParent(library));

        library->setChild(inputVoltage);

        WHEN("vimin is set.")
        {
            REQUIRE_NOTHROW(inputVoltage->setVimin("4.2 + VDD"));

            THEN("The set value can be returned through the getter method.")
            {
                REQUIRE(inputVoltage->getVimin()->getAsDouble() == 5004.2);

                REQUIRE(inputVoltage->getVimin()->getAsString() == "4.2 + VDD");
            }
        }
        delete library;
    }

    GIVEN("An empty InputVoltage instance, and a Library Parent with voltage_unit set to 1V")
    {
        LibView::model::data_structure::InputVoltage* inputVoltage = new LibView::model::data_structure::InputVoltage(modelFacade);

        LibView::model::data_structure::Library* library = new LibView::model::data_structure::Library(modelFacade);
        modelFacade->addAttribute("");

        REQUIRE_NOTHROW(library->setVoltage_unit("1V"));

        REQUIRE_NOTHROW(inputVoltage->setParent(library));

        WHEN("vih is set.")
        {
            REQUIRE_NOTHROW(inputVoltage->setVih("VDD + 0.8"));

            THEN("The set value can be returned through the getter method.")
            {
                REQUIRE(inputVoltage->getVih()->getAsDouble() == 5.8);

                REQUIRE(inputVoltage->getVih()->getAsString() == "VDD + 0.8");
            }
        }
        delete inputVoltage;
    }
    delete presenter;
}
#endif
