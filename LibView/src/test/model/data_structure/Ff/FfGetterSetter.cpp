
#ifdef LIBVIEW_TEST

#include "catch2/catch.hpp"

#include <iostream>

#include"LibView/model/data_structure/Ff.h"

#include "LibView/model/ModelFacade.h"
#include "LibView/presenter/PresenterFacade.h"

SCENARIO("Ff Values can be set through setters and get through getters.", "[LibView::model::data_structure::Ff]")
{
    LibView::model::ModelFacade* modelFacade = new LibView::model::ModelFacade();
    LibView::presenter::PresenterFacade* presenter = new LibView::presenter::PresenterFacade(modelFacade);

    GIVEN("An empty Ff instance.")
    {
        

        
        LibView::model::data_structure::Ff ff(modelFacade);
        modelFacade->addAttribute("");

        WHEN("clocked_on is set.")
        {
            REQUIRE_NOTHROW(ff.setClocked_on("a"));

            THEN("The set value can be returned through the getter method.")
            {
                REQUIRE(ff.getClocked_on() == "a");
            }
        }

        WHEN("next_state is set.")
        {
            REQUIRE_NOTHROW(ff.setNext_state("a"));

            THEN("The set value can be returned through the getter method.")
            {
                REQUIRE(ff.getNext_state() == "a");
            }
        }

        WHEN("clear is set.")
        {
            REQUIRE_NOTHROW(ff.setClear("a"));

            THEN("The set value can be returned through the getter method.")
            {
                REQUIRE(ff.getClear() == "a");
            }
        }
        
        WHEN("preset is set.")
        {
            REQUIRE_NOTHROW(ff.setPreset("a"));

            THEN("The set value can be returned through the getter method.")
            {
                REQUIRE(ff.getPreset() == "a");
            }
        }

    }
        delete presenter;
}
#endif 



