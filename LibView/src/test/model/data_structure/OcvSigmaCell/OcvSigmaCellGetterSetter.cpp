
#ifdef LIBVIEW_TEST

#include "catch2/catch.hpp"

#include <iostream>

#include"LibView/model/data_structure/OcvSigmaCell.h"
#include "LibView/model/ModelFacade.h"
#include "LibView/presenter/PresenterFacade.h"

SCENARIO("OcvSigmaCell Values can be set through setters and get through getters.", "[LibView::model::data_structure::OcvSigmaCell]")
{
    LibView::model::ModelFacade* modelFacade = new LibView::model::ModelFacade();
    LibView::presenter::PresenterFacade* presenter = new LibView::presenter::PresenterFacade(modelFacade);


    GIVEN("An empty OcvSigmaCell instance.")
    {

        LibView::model::data_structure::OcvSigmaCell* osc = new LibView::model::data_structure::OcvSigmaCell(modelFacade);
        modelFacade->addAttribute("");

        WHEN("sigma_type is set.")
        {
            REQUIRE_NOTHROW(osc->setSigma_type("abc"));

            THEN("The set value can be returned through the getter method.")
            {
                REQUIRE(osc->getSigma_type() == "abc");
            }
        }

        delete osc;
    }
    delete presenter;
}
#endif