
#ifdef LIBVIEW_TEST

#include "catch2/catch.hpp"

#include <iostream>

#include "LibView/model/builder/BuildDirector.h"
#include "LibView/model/ModelFacade.h"
#include "LibView/presenter/PresenterFacade.h"
#include <filesystem>
#include "LibView/core/DebugLog.h"

#include "LibView/core/Macros.h"

SCENARIO("A library can be build through the BuildDirector and the LibraryBuilder.", "[LibView::model::data_structure::LibraryBuilder]")
{
    LibView::model::ModelFacade* modelFacade = new LibView::model::ModelFacade();
    LibView::presenter::PresenterFacade* presenter = new LibView::presenter::PresenterFacade(modelFacade);

    LibView::model::data_structure::BuildDirector* groupBuilder = new LibView::model::data_structure::BuildDirector(modelFacade);

    GIVEN("An empty GroupBuilder instance.")
    {
        char temp[1024];
        LIBVIEW_CWD(temp, sizeof(temp));
        std::string cwd(temp);
        #ifdef _WINDOWS
        size_t foo = cwd.find("build", 0);
        cwd = cwd.substr(0, foo - 1);
        #endif
        std::string path = cwd + "/LibView/src/test/resources/TestGroups/libraryTest.lib";


        LibView::model::data_structure::LibertyFileData* libertyFile = groupBuilder->loadLibertyFile(path);

        LibView::model::data_structure::Library* library = (LibView::model::data_structure::Library*)libertyFile->getLibraryGroup();


        WHEN("library is build.")
        {


            THEN("The set value can be returned through the getter method.")
            {
                REQUIRE(library->getDelay_model() == "table_lookup");

                std::vector<std::string> library_features;
                library_features.push_back("report_delay_calculation");
                library_features.push_back("report_power_calculation");
                REQUIRE(library->getLibrary_features() == library_features);


                REQUIRE(library->getTime_unit() == "1ns");

                REQUIRE(library->getVoltage_unit() == "1V");

                REQUIRE(library->getCurrent_unit() == "1mA");

                REQUIRE(library->getCapacitive_load_unit()->getStringPart() == "pf");
                REQUIRE(library->getCapacitive_load_unit()->getDoublePart() == 1);

                REQUIRE(library->getPulling_resistance_unit() == "1ohm");

                REQUIRE(library->getLeakage_power_unit() == "1uW");

                REQUIRE(library->getInput_threshold_pct_fall() == 50);

                REQUIRE(library->getInput_threshold_pct_rise() == 50);

                REQUIRE(library->getOutput_threshold_pct_fall() == 50);

                REQUIRE(library->getOutput_threshold_pct_rise() == 50);

                REQUIRE(library->getSlew_derate_from_library() == 1);

                REQUIRE(library->getSlew_lower_threshold_pct_fall() == 10);

                REQUIRE(library->getSlew_lower_threshold_pct_rise() == 10);

                REQUIRE(library->getSlew_upper_threshold_pct_fall() == 90);

                REQUIRE(library->getSlew_upper_threshold_pct_rise() == 90);

                REQUIRE(library->getNom_process() == 1);

                REQUIRE(library->getNom_temperature() == 125);

                REQUIRE(library->getNom_voltage() == 0.8);

                REQUIRE(library->getDefault_cell_leakage_power() == 0);

                REQUIRE(library->getDefault_fanout_load() == 0);

                REQUIRE(library->getDefault_inout_pin_cap() == 0);

                REQUIRE(library->getDefault_input_pin_cap() == 0);

                REQUIRE(library->getDefault_leakage_power_density() == 0);

                REQUIRE(library->getDefault_output_pin_cap() == 0);

                REQUIRE(library->getUndifinedAttribute("undifinedSimple") == "simple");

                REQUIRE(library->getUndifinedAttribute("undifinedComplex") == "com, plex");

                REQUIRE(library->getChild("test") != nullptr);
            }
        }
        delete libertyFile;
    }
    delete groupBuilder;
    delete presenter;
}
#endif