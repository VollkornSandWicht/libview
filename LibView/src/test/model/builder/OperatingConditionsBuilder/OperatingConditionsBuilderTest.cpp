
#ifdef LIBVIEW_TEST

#include "catch2/catch.hpp"

#include <iostream>

#include "LibView/model/builder/BuildDirector.h"
#include "LibView/model/ModelFacade.h"
#include "LibView/presenter/PresenterFacade.h"
#include <filesystem>
#include "LibView/core/DebugLog.h"

SCENARIO("A operating_conditions can be build through the BuildDirector and the OperatingConditionsBuilder.", "[LibView::model::data_structure::OperatingConditionsBuilder]")
{
    LibView::model::ModelFacade* modelFacade = new LibView::model::ModelFacade();
    LibView::presenter::PresenterFacade* presenter = new LibView::presenter::PresenterFacade(modelFacade);

    LibView::model::data_structure::BuildDirector* groupBuilder = new LibView::model::data_structure::BuildDirector(modelFacade);

    GIVEN("An empty GroupBuilder instance.")
    {
        char temp[1024];
        LIBVIEW_CWD(temp, sizeof(temp));
        std::string cwd(temp);
        #ifdef _WINDOWS
        size_t foo = cwd.find("build", 0);
        cwd = cwd.substr(0, foo - 1);
        #endif
        std::string path = cwd + "/LibView/src/test/resources/TestGroups/operating_conditionsTest.lib";


        LibView::model::data_structure::LibertyFileData* libertyFile = groupBuilder->loadLibertyFile(path);

        LibView::model::data_structure::OperatingConditions* operatingConditions = (LibView::model::data_structure::OperatingConditions*)libertyFile->getLibraryGroup()->getChild("typical");


        WHEN("operating_conditions is build.")
        {


            THEN("The set value can be returned through the getter method.")
            {
                REQUIRE(operatingConditions->getProcess() == 1);

                REQUIRE(operatingConditions->getTemperature() == 125);

                REQUIRE(operatingConditions->getVoltage() == 0.8);

                REQUIRE(operatingConditions->getUndifinedAttribute("undifinedSimple") == "simple");

                REQUIRE(operatingConditions->getUndifinedAttribute("undifinedComplex") == "com, plex");
            }
        }
        delete libertyFile;
    }
    delete groupBuilder;
    delete presenter;
}
#endif