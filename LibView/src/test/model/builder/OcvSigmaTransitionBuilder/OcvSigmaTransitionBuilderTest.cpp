
#ifdef LIBVIEW_TEST

#include "catch2/catch.hpp"

#include <iostream>

#include "LibView/model/builder/BuildDirector.h"
#include "LibView/model/ModelFacade.h"
#include "LibView/presenter/PresenterFacade.h"
#include <filesystem>
#include "LibView/core/DebugLog.h"

SCENARIO("A transition table can be build through the BuildDirector and the ostBuilder.", "[LibView::model::data_structure::ostBuilder]")
{
    LibView::model::ModelFacade* modelFacade = new LibView::model::ModelFacade();
    LibView::presenter::PresenterFacade* presenter = new LibView::presenter::PresenterFacade(modelFacade);

    LibView::model::data_structure::BuildDirector* groupBuilder = new LibView::model::data_structure::BuildDirector(modelFacade);

    GIVEN("An empty GroupBuilder instance and a transition table group with a scalar.")
    {
        char temp[1024];
        LIBVIEW_CWD(temp, sizeof(temp));
        std::string cwd(temp);
        #ifdef _WINDOWS
        size_t foo = cwd.find("build", 0);
        cwd = cwd.substr(0, foo - 1);
        #endif
        std::string path = cwd + "/LibView/src/test/resources/TestGroups/ocv_sigma_transitionTestscalar.lib";

        LibView::model::data_structure::LibertyFileData* libertyFile = groupBuilder->loadLibertyFile(path);

        LibView::model::data_structure::OcvSigmaTransition* ost = (LibView::model::data_structure::OcvSigmaTransition*)libertyFile->getLibraryGroup()->getChild("AND2_X1_0")->getChild("A1")->getChild("")->getChild("scalar");


        WHEN("basic table is build.")
        {


            THEN("The set value can be returned through the getter method.")
            {
                REQUIRE(ost->getValues()->toString() == "[ 1.000000 ]");

                REQUIRE(ost->getIntermediate_values()->toString() == "[ 2.000000 ]");
            }
        }
        delete libertyFile;
    }

    GIVEN("An empty GroupBuilder instance and a transition table group with a 2d array.")
    {
        char temp[1024];
        LIBVIEW_CWD(temp, sizeof(temp));
        std::string cwd(temp);
        #ifdef _WINDOWS
        size_t foo = cwd.find("build", 0);
        cwd = cwd.substr(0, foo - 1);
        #endif
        std::string path = cwd + "/LibView/src/test/resources/TestGroups/ocv_sigma_transitionTest1dArray.lib";

        LibView::model::data_structure::LibertyFileData* libertyFile = groupBuilder->loadLibertyFile(path);

        LibView::model::data_structure::OcvSigmaTransition* ost = (LibView::model::data_structure::OcvSigmaTransition*)libertyFile->getLibraryGroup()->getChild("AND2_X1_0")->getChild("A1")->getChild("")->getChild("pwr_tin_oload_7x7");


        WHEN("basic table is build.")
        {


            THEN("The set value can be returned through the getter method.")
            {
                REQUIRE(ost->getIndex_1()->toString() == "[ 0.000400, 0.009027, 0.039310, 0.097140, 0.187200, 0.313700, 0.480000 ]");

                REQUIRE(ost->getValues()->toString() == "[ 0.001858, 0.001891, 0.001902, 0.001910, 0.001916, 0.001911, 0.001910 ]");

                REQUIRE(ost->getIntermediate_values()->toString() == "[ 0.001898, 0.001908, 0.001923, 0.001930, 0.001938, 0.001941, 0.001929 ]");
            }
        }
        delete libertyFile;
    }

    GIVEN("An empty GroupBuilder instance and a transition table group with a 2d array.")
    {
        char temp[1024];
        LIBVIEW_CWD(temp, sizeof(temp));
        std::string cwd(temp);
        #ifdef _WINDOWS
        size_t foo = cwd.find("build", 0);
        cwd = cwd.substr(0, foo - 1);
        #endif
        std::string path = cwd + "/LibView/src/test/resources/TestGroups/ocv_sigma_transitionTest2dArray.lib";

        LibView::model::data_structure::LibertyFileData* libertyFile = groupBuilder->loadLibertyFile(path);

        LibView::model::data_structure::OcvSigmaTransition* ost = (LibView::model::data_structure::OcvSigmaTransition*)libertyFile->getLibraryGroup()->getChild("AND2_X1_0")->getChild("A1")->getChild("")->getChild("pwr_tin_oload_7x7");


        WHEN("basic table is build.")
        {


            THEN("The set value can be returned through the getter method.")
            {
                REQUIRE(ost->getIndex_1()->toString() == "[ 0.000400, 0.009027, 0.039310, 0.097140, 0.187200, 0.313700, 0.480000 ]");

                REQUIRE(ost->getIndex_2()->toString() == "[ 0.000400, 0.002192, 0.008481, 0.020490, 0.039200, 0.065450, 0.100000 ]");

                REQUIRE(ost->getValues()->toString() == "[ 0.001858, 0.001891, 0.001902, 0.001910, 0.001916, 0.001911, 0.001910 ]\n[ 0.001762, 0.001761, 0.001810, 0.001825, 0.001824, 0.001811, 0.001809 ]\n[ 0.001898, 0.001908, 0.001923, 0.001930, 0.001938, 0.001941, 0.001929 ]\n[ 0.002438, 0.002439, 0.002429, 0.002423, 0.002413, 0.002420, 0.002414 ]\n[ 0.003383, 0.003325, 0.003321, 0.003307, 0.003287, 0.003285, 0.003286 ]\n[ 0.004782, 0.004716, 0.004655, 0.004606, 0.004601, 0.004570, 0.004570 ]\n[ 0.006675, 0.006562, 0.006469, 0.006380, 0.006337, 0.006317, 0.006284 ]\n");

                REQUIRE(ost->getIntermediate_values()->toString() == "[ 0.000000, 0.001891, 0.001902, 0.001910, 0.001916, 0.001911, 0.001910 ]\n[ 0.001762, 0.001761, 0.001810, 0.001825, 0.001824, 0.001811, 0.001809 ]\n[ 0.001898, 0.001908, 0.001923, 0.001930, 0.001938, 0.001941, 0.001929 ]\n[ 0.002438, 0.002439, 0.002429, 0.002423, 0.002413, 0.002420, 0.002414 ]\n[ 0.003383, 0.003325, 0.003321, 0.003307, 0.003287, 0.003285, 0.003286 ]\n[ 0.004782, 0.004716, 0.004655, 0.004606, 0.004601, 0.004570, 0.004570 ]\n[ 0.006675, 0.006562, 0.006469, 0.006380, 0.006337, 0.006317, 1.000000 ]\n");

                REQUIRE(ost->getUndifinedAttribute("undifinedSimple") == "simple");

                REQUIRE(ost->getUndifinedAttribute("undifinedComplex") == "com, plex");

                REQUIRE(ost->getSigma_type() == "\"early\"");
            }
        }
        delete libertyFile;
    }
    delete groupBuilder;
    delete presenter;
}
#endif