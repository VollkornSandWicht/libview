
#ifdef LIBVIEW_TEST

#include "catch2/catch.hpp"

#include <iostream>

#include "LibView/model/builder/BuildDirector.h"
#include "LibView/model/ModelFacade.h"
#include "LibView/presenter/PresenterFacade.h"
#include <filesystem>
#include "LibView/core/DebugLog.h"

SCENARIO("A power_lut_template can be build through the BuildDirector and the PowerLutTemplateBuilder.", "[LibView::model::data_structure::PowerLutTemplateBuilder]")
{
    LibView::model::ModelFacade* modelFacade = new LibView::model::ModelFacade();
    LibView::presenter::PresenterFacade* presenter = new LibView::presenter::PresenterFacade(modelFacade);

    LibView::model::data_structure::BuildDirector* groupBuilder = new LibView::model::data_structure::BuildDirector(modelFacade);

    GIVEN("An empty GroupBuilder instance.")
    {
        char temp[1024];
        LIBVIEW_CWD(temp, sizeof(temp));
        std::string cwd(temp);
        #ifdef _WINDOWS
        size_t foo = cwd.find("build", 0);
        cwd = cwd.substr(0, foo - 1);
        #endif
        std::string path = cwd + "/LibView/src/test/resources/TestGroups/power_lut_templateTest.lib";

        //std::cout << path << std::endl;

        LibView::model::data_structure::LibertyFileData* libertyFile = groupBuilder->loadLibertyFile(path);

        LibView::model::data_structure::PowerLutTemplate* plt = (LibView::model::data_structure::PowerLutTemplate*)libertyFile->getLibraryGroup()->getChild("pwr_tin_oload_7x7");


        WHEN("power_lut_template is build.")
        {


            THEN("The set value can be returned through the getter method.")
            {
                REQUIRE(plt->getVariable_1() == "input_transition_time");

                REQUIRE(plt->getVariable_2() == "total_output_net_capacitance");

                REQUIRE(plt->getVariable_3() == "total_output_net_test");

                REQUIRE(plt->getIndex_1()->toString() == "[ 1.000000, 2.000000, 3.000000, 4.000000, 5.000000, 6.000000, 7.000000 ]");

                REQUIRE(plt->getIndex_2()->toString() == "[ 1.000000, 2.000000, 3.000000, 4.000000, 5.000000, 6.000000, 8.000000 ]");

                REQUIRE(plt->getIndex_3()->toString() == "[ 1.000000, 2.000000, 3.000000, 4.000000, 5.000000, 6.000000, 9.000000 ]");

                REQUIRE(plt->getUndifinedAttribute("undifinedSimple") == "simple");

                REQUIRE(plt->getUndifinedAttribute("undifinedComplex") == "com, plex");
            }
        }
        delete libertyFile;
    }
    delete groupBuilder;
    delete presenter;
}
#endif