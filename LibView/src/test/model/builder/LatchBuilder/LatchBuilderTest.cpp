
#ifdef LIBVIEW_TEST

#include "catch2/catch.hpp"

#include <iostream>

#include "LibView/model/builder/BuildDirector.h"
#include "LibView/model/ModelFacade.h"
#include "LibView/presenter/PresenterFacade.h"
#include <filesystem>
#include "LibView/core/DebugLog.h"

SCENARIO("A latch can be build through the BuildDirector and the LatchBuilder.", "[LibView::model::data_structure::LatchBuilder]")
{
    LibView::model::ModelFacade* modelFacade = new LibView::model::ModelFacade();
    LibView::presenter::PresenterFacade* presenter = new LibView::presenter::PresenterFacade(modelFacade);

    LibView::model::data_structure::BuildDirector* groupBuilder = new LibView::model::data_structure::BuildDirector(modelFacade);

    GIVEN("An empty GroupBuilder instance.")
    {
        char temp[1024];
        LIBVIEW_CWD(temp, sizeof(temp));
        std::string cwd(temp);
        #ifdef _WINDOWS
        size_t foo = cwd.find("build", 0);
        cwd = cwd.substr(0, foo - 1);
        #endif
        std::string path = cwd + "/LibView/src/test/resources/TestGroups/latchTest.lib";

        //std::cout << path << std::endl;

        LibView::model::data_structure::LibertyFileData* libertyFile = groupBuilder->loadLibertyFile(path);

        LibView::model::data_structure::Latch* latch = (LibView::model::data_structure::Latch*)libertyFile->getLibraryGroup()->getChild("AND2_X1_0")->getChild("IQ2,IQN2");


        WHEN("latch is build.")
        {


            THEN("The set value can be returned through the getter method.")
            {
                REQUIRE(latch->getEnable() == "\"(!CLK)\"");

                REQUIRE(latch->getData_in() == "\"(E|TE)\"");

                REQUIRE(latch->getUndifinedAttribute("undifinedSimple") == "simple");

                REQUIRE(latch->getUndifinedAttribute("undifinedComplex") == "com, plex");
            }
        }
        delete libertyFile;
    }
    delete groupBuilder;
    delete presenter;
}
#endif