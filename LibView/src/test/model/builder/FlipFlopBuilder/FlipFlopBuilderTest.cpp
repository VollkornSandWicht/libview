
#ifdef LIBVIEW_TEST

#include "catch2/catch.hpp"

#include <iostream>

#include "LibView/model/builder/BuildDirector.h"
#include "LibView/model/ModelFacade.h"
#include "LibView/presenter/PresenterFacade.h"
#include <filesystem>

SCENARIO("A flipflop can be build through the BuildDirector and the FlipFlopBuilder.", "[LibView::model::data_structure::FlipFlopBuilder]")
{
    LibView::model::ModelFacade* modelFacade = new LibView::model::ModelFacade();
    LibView::presenter::PresenterFacade* presenter = new LibView::presenter::PresenterFacade(modelFacade);

    LibView::model::data_structure::BuildDirector* groupBuilder = new LibView::model::data_structure::BuildDirector(modelFacade);

    GIVEN("An empty GroupBuilder instance.")
    {
        char temp[1024];
        LIBVIEW_CWD(temp, sizeof(temp));
        std::string cwd(temp);
        #ifdef _WINDOWS
        size_t foo = cwd.find("build", 0);
        cwd = cwd.substr(0, foo - 1);
        #endif
        std::string path = cwd + "/LibView/src/test/resources/TestGroups/ffTest.lib";

        //std::cout << path << std::endl;

        LibView::model::data_structure::LibertyFileData* libertyFile = groupBuilder->loadLibertyFile(path);

        LibView::model::data_structure::Ff* flipFlop = (LibView::model::data_structure::Ff*)libertyFile->getLibraryGroup()->getChild("AND2_X1_0")->getChild("IQ1,IQN1");


        WHEN("flipflop is build.")
        {


            THEN("The set value can be returned through the getter method.")
            {
                REQUIRE(flipFlop->getClocked_on() == "\"CLK\"");

                REQUIRE(flipFlop->getNext_state() == "\"D\"");

                REQUIRE(flipFlop->getClear() == "\"(!RN)\"");

                REQUIRE(flipFlop->getPreset() == "\"(!SN)\"");

                REQUIRE(flipFlop->getUndifinedAttribute("undifinedSimple") == "simple");

                REQUIRE(flipFlop->getUndifinedAttribute("undifinedComplex") == "com, plex");

            }
        }
        delete libertyFile;
    }
    delete groupBuilder;
    delete presenter;
}
#endif