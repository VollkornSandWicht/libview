
#ifdef LIBVIEW_TEST

#include "catch2/catch.hpp"

#include <iostream>

#include "LibView/model/builder/BuildDirector.h"
#include "LibView/model/ModelFacade.h"
#include "LibView/presenter/PresenterFacade.h"
#include <filesystem>
#include "LibView/core/DebugLog.h"

SCENARIO("A input_voltage can be build through the BuildDirector and the InputVoltageBuilder.", "[LibView::model::data_structure::InputVoltageBuilder]")
{
    LibView::model::ModelFacade* modelFacade = new LibView::model::ModelFacade();
    LibView::presenter::PresenterFacade* presenter = new LibView::presenter::PresenterFacade(modelFacade);

    LibView::model::data_structure::BuildDirector* groupBuilder = new LibView::model::data_structure::BuildDirector(modelFacade);

    GIVEN("An empty GroupBuilder instance.")
    {
        char temp[1024];
        LIBVIEW_CWD(temp, sizeof(temp));
        std::string cwd(temp);
        #ifdef _WINDOWS
        size_t foo = cwd.find("build", 0);
        cwd = cwd.substr(0, foo - 1);
        #endif
        std::string path = cwd + "/LibView/src/test/resources/TestGroups/input_voltageTest.lib";

        //std::cout << path << std::endl;

        LibView::model::data_structure::LibertyFileData* libertyFile = groupBuilder->loadLibertyFile(path);

        LibView::model::data_structure::InputVoltage* inputVoltage = (LibView::model::data_structure::InputVoltage*)libertyFile->getLibraryGroup()->getChild("default");


        WHEN("input_voltage is build.")
        {


            THEN("The set value can be returned through the getter method.")
            {
                REQUIRE(inputVoltage->getVil()->getAsDouble() == 0);

                REQUIRE(inputVoltage->getVih()->getAsDouble() == 0.8);

                REQUIRE(inputVoltage->getVimax()->getAsDouble() == 0.8);

                REQUIRE(inputVoltage->getVimin()->getAsDouble() == 0);

                REQUIRE(inputVoltage->getUndifinedAttribute("undifinedSimple") == "simple");

                REQUIRE(inputVoltage->getUndifinedAttribute("undifinedComplex") == "com, plex");
            }
        }
        delete libertyFile;
    }
    delete groupBuilder;
    delete presenter;
}
#endif