
#ifdef LIBVIEW_TEST

#include "catch2/catch.hpp"

#include <iostream>

#include "LibView/model/builder/BuildDirector.h"
#include "LibView/model/ModelFacade.h"
#include "LibView/presenter/PresenterFacade.h"
#include <filesystem>
#include "LibView/core/DebugLog.h"

SCENARIO("A lu_table_template can be build through the BuildDirector and the LuTableTemplateBuilder.", "[LibView::model::data_structure::LuTableTemplateBuilder]")
{
    LibView::model::ModelFacade* modelFacade = new LibView::model::ModelFacade();
    LibView::presenter::PresenterFacade* presenter = new LibView::presenter::PresenterFacade(modelFacade);

    LibView::model::data_structure::BuildDirector* groupBuilder = new LibView::model::data_structure::BuildDirector(modelFacade);

    GIVEN("An empty GroupBuilder instance.")
    {
        char temp[1024];
        LIBVIEW_CWD(temp, sizeof(temp));
        std::string cwd(temp);
        #ifdef _WINDOWS
        size_t foo = cwd.find("build", 0);
        cwd = cwd.substr(0, foo - 1);
        #endif
        std::string path = cwd + "/LibView/src/test/resources/TestGroups/lu_table_templateTest.lib";

        //std::cout << path << std::endl;

        LibView::model::data_structure::LibertyFileData* libertyFile = groupBuilder->loadLibertyFile(path);

        LibView::model::data_structure::LuTableTemplate* ltt = (LibView::model::data_structure::LuTableTemplate*)libertyFile->getLibraryGroup()->getChild("cnst_ctin_rtin_3x3");


        WHEN("lu_table_template is build.")
        {


            THEN("The set value can be returned through the getter method.")
            {
                REQUIRE(ltt->getVariable_1() == "constrained_pin_transition");

                REQUIRE(ltt->getVariable_2() == "related_pin_transition");

                REQUIRE(ltt->getVariable_3() == "related_pin_test");

                REQUIRE(ltt->getIndex_1()->toString() == "[ 1.000000, 2.000000, 3.000000 ]");

                REQUIRE(ltt->getIndex_2()->toString() == "[ 4.000000, 5.000000, 6.000000 ]");

                REQUIRE(ltt->getIndex_3()->toString() == "[ 7.000000, 8.000000, 9.000000 ]");

                REQUIRE(ltt->getUndifinedAttribute("undifinedSimple") == "simple");

                REQUIRE(ltt->getUndifinedAttribute("undifinedComplex") == "com, plex");
            }
        }
        delete libertyFile;
    }
    delete groupBuilder;
    delete presenter;
}
#endif