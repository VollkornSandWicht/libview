
#ifdef LIBVIEW_TEST

#include "catch2/catch.hpp"

#include <iostream>

#include "LibView/model/builder/BuildDirector.h"
#include "LibView/model/ModelFacade.h"
#include "LibView/presenter/PresenterFacade.h"
#include <filesystem>
#include "LibView/core/DebugLog.h"

SCENARIO("A pin can be build through the BuildDirector and the PinBuilder.", "[LibView::model::data_structure::PinBuilder]")
{
    LibView::model::ModelFacade* modelFacade = new LibView::model::ModelFacade();
    LibView::presenter::PresenterFacade* presenter = new LibView::presenter::PresenterFacade(modelFacade);

    LibView::model::data_structure::BuildDirector* groupBuilder = new LibView::model::data_structure::BuildDirector(modelFacade);

    GIVEN("An empty GroupBuilder instance.")
    {
        char temp[1024];
        LIBVIEW_CWD(temp, sizeof(temp));
        std::string cwd(temp);
        #ifdef _WINDOWS
        size_t foo = cwd.find("build", 0);
        cwd = cwd.substr(0, foo - 1);
        #endif
        std::string path = cwd + "/LibView/src/test/resources/TestGroups/pinTest.lib";

        //std::cout << path << std::endl;

        LibView::model::data_structure::LibertyFileData* libertyFile = groupBuilder->loadLibertyFile(path);

        LibView::model::data_structure::Pin* pin = (LibView::model::data_structure::Pin*)libertyFile->getLibraryGroup()->getChild("AND2_X1_0")->getChild("A1");


        WHEN("pin is build.")
        {


            THEN("The set value can be returned through the getter method.")
            {
                REQUIRE(pin->getCapacitance() == 0.001509);

                REQUIRE(pin->getMax_capacitance() == 0.1);

                REQUIRE(pin->getMin_capacitance() == 0.0004);

                REQUIRE(pin->getMin_pulse_width_low() == 0.02803);

                REQUIRE(pin->getMin_pulse_width_high() == 0.033);

                REQUIRE(pin->getDirection() == "input");

                REQUIRE(pin->getInput_voltage() == "default");

                REQUIRE(pin->getOutput_voltage() == "default");

                REQUIRE(pin->getClock() == "true");

                REQUIRE(pin->getNextstate_type() == "data");

                REQUIRE(pin->getFunction() == "\"(A1&A2)\"");

                REQUIRE(pin->getThree_state() == "\"(!EN)\"");

                REQUIRE(pin->getDriver_waveform_fall() == "\"driver_waveform_default_fall\"");

                REQUIRE(pin->getDriver_waveform_rise() == "\"driver_waveform_default_rise\"");

                REQUIRE(pin->getUndifinedAttribute("undifinedSimple") == "simple");

                REQUIRE(pin->getUndifinedAttribute("undifinedComplex") == "com, plex");
            }
        }
        delete libertyFile;
    }
    delete groupBuilder;
    delete presenter;
}
#endif