
#ifdef LIBVIEW_TEST

#include "catch2/catch.hpp"

#include <iostream>

#include "LibView/model/builder/BuildDirector.h"
#include "LibView/model/ModelFacade.h"
#include "LibView/presenter/PresenterFacade.h"
#include <filesystem>
#include "LibView/core/DebugLog.h"

SCENARIO("A normalized_driver_waveform can be build through the BuildDirector and the NormalizedDriverWaveformBuilder.", "[LibView::model::data_structure::NormalizedDriverWaveformBuilder]")
{
    LibView::model::ModelFacade* modelFacade = new LibView::model::ModelFacade();
    LibView::presenter::PresenterFacade* presenter = new LibView::presenter::PresenterFacade(modelFacade);

    LibView::model::data_structure::BuildDirector* groupBuilder = new LibView::model::data_structure::BuildDirector(modelFacade);

    GIVEN("An empty GroupBuilder instance and a normalized driver waveform group with a 1d array.")
    {
        char temp[1024];
        LIBVIEW_CWD(temp, sizeof(temp));
        std::string cwd(temp);
        #ifdef _WINDOWS
        size_t foo = cwd.find("build", 0);
        cwd = cwd.substr(0, foo - 1);
        #endif
        std::string path = cwd + "/LibView/src/test/resources/TestGroups/normalized_driver_waveformTest1dArray.lib";

        //std::cout << path << std::endl;

        LibView::model::data_structure::LibertyFileData* libertyFile = groupBuilder->loadLibertyFile(path);

        LibView::model::data_structure::NormalizedDriverWaveform* ndw = (LibView::model::data_structure::NormalizedDriverWaveform*)libertyFile->getLibraryGroup()->getChild("|\"driver_waveform_default_fall\"|ndw_ntin_nvolt_8x2");


        WHEN("normalized_driver_waveform is build.")
        {


            THEN("The set value can be returned through the getter method.")
            {
                REQUIRE(ndw->getDriver_waveform_name() == "\"driver_waveform_default_fall\"");

                REQUIRE(ndw->getIndex_1()->toString() == "[ 0.000000, 1.000000 ]");

                REQUIRE(ndw->getValues()->toString() == "[ 0.000000, 0.000500 ]");
            }
        }
        delete libertyFile;
    }

    GIVEN("An empty GroupBuilder instance and a normalized driver waveform group with a 2d array.")
    {
        char temp[1024];
        LIBVIEW_CWD(temp, sizeof(temp));
        std::string cwd(temp);
        #ifdef _WINDOWS
        size_t foo = cwd.find("build", 0);
        cwd = cwd.substr(0, foo - 1);
        #endif
        std::string path = cwd + "/LibView/src/test/resources/TestGroups/normalized_driver_waveformTest2dArray.lib";

        //std::cout << path << std::endl;

        LibView::model::data_structure::LibertyFileData* libertyFile = groupBuilder->loadLibertyFile(path);

        LibView::model::data_structure::NormalizedDriverWaveform* ndw = (LibView::model::data_structure::NormalizedDriverWaveform*)libertyFile->getLibraryGroup()->getChild("|\"driver_waveform_default_fall\"|ndw_ntin_nvolt_8x2");


        WHEN("normalized_driver_waveform is build.")
        {


            THEN("The set value can be returned through the getter method.")
            {
                REQUIRE(ndw->getDriver_waveform_name() == "\"driver_waveform_default_fall\"");

                REQUIRE(ndw->getIndex_1()->toString() == "[ 0.000400, 0.009027, 0.039310, 0.097140, 0.106700, 0.187200, 0.313700, 0.480000 ]");

                REQUIRE(ndw->getIndex_2()->toString() == "[ 0.000000, 1.000000 ]");

                REQUIRE(ndw->getValues()->toString() == "[ 0.000000, 0.000500 ]\n[ 0.000000, 0.011280 ]\n[ 0.000000, 0.049140 ]\n[ 0.000000, 0.121400 ]\n[ 0.000000, 0.133400 ]\n[ 0.000000, 0.234000 ]\n[ 0.000000, 0.392100 ]\n[ 0.000000, 0.600000 ]\n");

                REQUIRE(ndw->getUndifinedAttribute("undifinedSimple") == "simple");

                REQUIRE(ndw->getUndifinedAttribute("undifinedComplex") == "com, plex");

            }
        }
        delete libertyFile;
    }
    delete groupBuilder;
    delete presenter;
}
#endif