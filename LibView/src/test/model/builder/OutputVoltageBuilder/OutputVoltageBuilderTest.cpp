
#ifdef LIBVIEW_TEST

#include "catch2/catch.hpp"

#include <iostream>

#include "LibView/model/builder/BuildDirector.h"
#include "LibView/model/ModelFacade.h"
#include "LibView/presenter/PresenterFacade.h"
#include <filesystem>
#include "LibView/core/DebugLog.h"

SCENARIO("A output_voltage can be build through the BuildDirector and the OutputVoltageBuilder.", "[LibView::model::data_structure::OutputVoltageBuilder]")
{
    LibView::model::ModelFacade* modelFacade = new LibView::model::ModelFacade();
    LibView::presenter::PresenterFacade* presenter = new LibView::presenter::PresenterFacade(modelFacade);

    LibView::model::data_structure::BuildDirector* groupBuilder = new LibView::model::data_structure::BuildDirector(modelFacade);

    GIVEN("An empty GroupBuilder instance.")
    {
        char temp[1024];
        LIBVIEW_CWD(temp, sizeof(temp));
        std::string cwd(temp);
        #ifdef _WINDOWS
        size_t foo = cwd.find("build", 0);
        cwd = cwd.substr(0, foo - 1);
        #endif
        std::string path = cwd + "/LibView/src/test/resources/TestGroups/output_voltageTest.lib";

        //std::cout << path << std::endl;

        LibView::model::data_structure::LibertyFileData* libertyFile = groupBuilder->loadLibertyFile(path);

        LibView::model::data_structure::OutputVoltage* outputVoltage = (LibView::model::data_structure::OutputVoltage*)libertyFile->getLibraryGroup()->getChild("default");


        WHEN("output_voltage is build.")
        {


            THEN("The set value can be returned through the getter method.")
            {
                REQUIRE(outputVoltage->getVol()->getAsDouble() == 0);

                REQUIRE(outputVoltage->getVoh()->getAsDouble() == 0.8);

                REQUIRE(outputVoltage->getVomax()->getAsDouble() == 0.8);

                REQUIRE(outputVoltage->getVomin()->getAsDouble() == 0);

                REQUIRE(outputVoltage->getUndifinedAttribute("undifinedSimple") == "simple");

                REQUIRE(outputVoltage->getUndifinedAttribute("undifinedComplex") == "com, plex");
            }
        }
        delete libertyFile;
    }
    delete groupBuilder;
    delete presenter;
}
#endif