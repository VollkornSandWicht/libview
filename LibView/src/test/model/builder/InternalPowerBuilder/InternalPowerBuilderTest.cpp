
#ifdef LIBVIEW_TEST

#include "catch2/catch.hpp"

#include <iostream>

#include "LibView/model/builder/BuildDirector.h"
#include "LibView/model/ModelFacade.h"
#include "LibView/presenter/PresenterFacade.h"
#include <filesystem>
#include "LibView/core/DebugLog.h"

SCENARIO("A internal_power can be build through the BuildDirector and the InternalPowerBuilder.", "[LibView::model::data_structure::InternalPowerBuilder]")
{
    LibView::model::ModelFacade* modelFacade = new LibView::model::ModelFacade();
    LibView::presenter::PresenterFacade* presenter = new LibView::presenter::PresenterFacade(modelFacade);

    LibView::model::data_structure::BuildDirector* groupBuilder = new LibView::model::data_structure::BuildDirector(modelFacade);

    GIVEN("An empty GroupBuilder instance.")
    {
        char temp[1024];
        LIBVIEW_CWD(temp, sizeof(temp));
        std::string cwd(temp);
        #ifdef _WINDOWS
        size_t foo = cwd.find("build", 0);
        cwd = cwd.substr(0, foo - 1);
        #endif
        std::string path = cwd + "/LibView/src/test/resources/TestGroups/internal_powerTest.lib";

        //std::cout << path << std::endl;

        LibView::model::data_structure::LibertyFileData* libertyFile = groupBuilder->loadLibertyFile(path);

        LibView::model::data_structure::Cell* cell = (LibView::model::data_structure::Cell*)libertyFile->getLibraryGroup()->getChild("AND2_X1_0");
        LibView::model::data_structure::Pin* pin = (LibView::model::data_structure::Pin*)cell->getChild("A1");
        LibView::model::data_structure::InternalPower* internalPower = (LibView::model::data_structure::InternalPower*)pin->getChild("|\"A1\"|");
        WHEN("internal_power is build.")
        {


            THEN("The set value can be returned through the getter method.")
            {
                REQUIRE(internalPower->getRelated_pin() == "\"A1\"");

                REQUIRE(internalPower->getUndifinedAttribute("undifinedSimple") == "simple");

                REQUIRE(internalPower->getUndifinedAttribute("undifinedComplex") == "com, plex");
            }
        }
        delete libertyFile;
    }
    delete groupBuilder;
    delete presenter;
}
#endif