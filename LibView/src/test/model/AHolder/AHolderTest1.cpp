#ifdef LIBVIEW_TEST

#include "catch2/catch.hpp"
#include "LibView/model/AHolder.h"
#include "LibView/model/LibViewPair.h"


SCENARIO("AHolder")
{
    GIVEN("An holder instance")
    {
        LibView::model::AHolder aHolder;
        LibView::model::LibViewPair<std::string, LibView::index_t> pair;
        LibView::model::LibViewPair<std::string, LibView::index_t> pair2;
        LibView::index_t index1 = LibView::index_t::requestUnique();
        LibView::index_t index2 = LibView::index_t::requestUnique();
        pair.setFirst("test");
        pair.setSecond(index1);        
        pair2.setFirst("test2");
        pair2.setSecond(index2);
        aHolder.addToLastAdded(pair);
        aHolder.addToLastRemoved(pair2);
        
        WHEN("last stored items are cleared together")
        {
            aHolder.clearLastStoredItems();
            THEN("the lists are empty")
            {
                REQUIRE(aHolder.getLastAdded().size() == 0);
                REQUIRE(aHolder.getLastRemoved().size() == 0);
            }
        }

        WHEN("last stored items are cleared after eachother")
        {
            aHolder.resetLastAdded();
            aHolder.resetLastRemoved();
            THEN("the lists are empty")
            {
                REQUIRE(aHolder.getLastAdded().size() == 0);
                REQUIRE(aHolder.getLastRemoved().size() == 0);
            }
        }
        
        WHEN("the lists should be returned")
        {

            THEN("the lists are returned")
            {
                REQUIRE(aHolder.getLastAdded().size()== 1);
                REQUIRE(aHolder.getLastRemoved().size() == 1);
            }
        }
    }
}
#endif