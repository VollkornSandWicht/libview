#ifdef LIBVIEW_TEST

#include <catch2/catch.hpp>
#include "LibView/model/LibViewPair.h"


SCENARIO("A LibViewPair can be instantiated with 2 primitive types", "[LibView::model::LibViewPair]")
{
    GIVEN("Nothing")
    {
        WHEN("An Instance is created")
        {
            LibView::model::LibViewPair<int, double> pair;

            THEN("The object has a memoy address")
            {
                REQUIRE(&pair != nullptr);
            }
        }
    }

    GIVEN("An Instance of LibViewPair")
    {
        LibView::model::LibViewPair<int, double> pair;

        WHEN("The first value is set using setFirst()")
        {
            pair.setFirst(1337);
            
            THEN("The same value can be retrieved using getFirst()")
            {
                REQUIRE(pair.getFirst() == 1337);
            }
        }

        WHEN("The second value is set using setSecond()")
        {
            pair.setSecond(13.5);

            THEN("The same value can be retrieved using getSecond()")
            {
                REQUIRE(pair.getSecond() == 13.5);
            }
        }

    }
}


#endif
