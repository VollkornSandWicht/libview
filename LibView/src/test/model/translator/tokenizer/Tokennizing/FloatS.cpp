#ifdef LIBVIEW_TEST


#include "catch2/catch.hpp"

#include "LibView/model/translator/tokenizer/OnDemandTokenizer.h" //Testet class


struct FloatSTest {

	static void checkString(std::string tokenString) {

		LibView::model::translator::tokenizer::OnDemandTokenizer odTok = LibView::model::translator::tokenizer::OnDemandTokenizer(&tokenString);

		REQUIRE(odTok.hasNextToken());
		LibView::model::translator::token::Token createdToken = odTok.getNextToken();
		REQUIRE(!(odTok.hasNextToken()));

		REQUIRE(createdToken.getKey() == LibView::model::translator::token::TokenKeys::FloatS);
		REQUIRE(createdToken.getValue() == tokenString);

	}
};


SCENARIO("The tokenizer reads a scientific float.")
{


	GIVEN("Scientic number by integer.")
	{
		FloatSTest::checkString("1e-1");
	}

	GIVEN("Scientic number by float.")
	{
		FloatSTest::checkString("0.1e-1");
	}

	GIVEN("Positiv exponent integer.")
	{
		FloatSTest::checkString("1e-1");
	}

	GIVEN("Positiv exponent flaot.")
	{
		FloatSTest::checkString("0.1e1");
	}
	GIVEN("Multi digit exponent.")
	{
		FloatSTest::checkString("0.1e-10");
	}
}
#endif