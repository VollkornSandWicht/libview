#ifdef LIBVIEW_TEST


#include "catch2/catch.hpp"

#include "LibView/model/translator/tokenizer/OnDemandTokenizer.h" //Testet class


struct OperatorTest {

	static void checkString(std::string tokenString) {

		LibView::model::translator::tokenizer::OnDemandTokenizer odTok = LibView::model::translator::tokenizer::OnDemandTokenizer(&tokenString);

		REQUIRE(odTok.hasNextToken());
		LibView::model::translator::token::Token createdToken = odTok.getNextToken();
		REQUIRE(!(odTok.hasNextToken()));

		REQUIRE(createdToken.getKey() == LibView::model::translator::token::TokenKeys::Operator);
		REQUIRE(createdToken.getValue() == tokenString);

	}
};


SCENARIO("The tokenizer reads operator signs.")
{


	GIVEN("+ string.")
	{
		OperatorTest::checkString("+");
	}

	GIVEN("- string.")
	{
		OperatorTest::checkString("-");
	}

	GIVEN("* string.")
	{
		OperatorTest::checkString("*");
	}

	GIVEN("/ string.")
	{
		OperatorTest::checkString("/");
	}
}
#endif