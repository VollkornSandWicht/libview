#ifdef LIBVIEW_TEST


#include "catch2/catch.hpp"

#include "LibView/model/translator/tokenizer/OnDemandTokenizer.h" //Testet class

struct IntegerTest {

	static void checkString(std::string tokenString) {

		LibView::model::translator::tokenizer::OnDemandTokenizer odTok = LibView::model::translator::tokenizer::OnDemandTokenizer(&tokenString);

		REQUIRE(odTok.hasNextToken());
		LibView::model::translator::token::Token createdToken = odTok.getNextToken();
		REQUIRE(!(odTok.hasNextToken()));

		REQUIRE(createdToken.getKey() == LibView::model::translator::token::TokenKeys::Integer);
		REQUIRE(createdToken.getValue() == tokenString);
	}
};

SCENARIO("The tokenizer reads in a integer.")
{
	GIVEN("0 string.")
	{
		IntegerTest::checkString("0");
	}

	GIVEN("1 string.")
	{
		IntegerTest::checkString("1");
	}

	GIVEN("2 string.")
	{
		IntegerTest::checkString("2");
	}

	GIVEN("3 string.")
	{
		IntegerTest::checkString("3");
	}

	GIVEN("4 string.")
	{
		IntegerTest::checkString("4");
	}

	GIVEN("5 string.")
	{
		IntegerTest::checkString("5");
	}

	GIVEN("6 string.")
	{
		IntegerTest::checkString("6");
	}

	GIVEN("7 string.")
	{
		IntegerTest::checkString("7");
	}

	GIVEN("8 string.")
	{
		IntegerTest::checkString("8");
	}

	GIVEN("9 string.")
	{
		IntegerTest::checkString("9");
	}

	GIVEN("Double digit string 10.")
	{
		IntegerTest::checkString("10");
	}

	GIVEN("Double digit string 99.")
	{
		IntegerTest::checkString("99");
	}

	GIVEN("Double digit string 45.")
	{
		IntegerTest::checkString("45");
	}

	GIVEN("Multi digit string 123456789.")
	{
		IntegerTest::checkString("123456789");
	}

	GIVEN("Leading 0.")
	{
		IntegerTest::checkString("01");
	}

	GIVEN("Leading 0�s.")
	{
		IntegerTest::checkString("001");
	}

	GIVEN("Negativ dashed string.")
	{
		IntegerTest::checkString("-1");
	}

	GIVEN("Positiv add sign string.")
	{
		IntegerTest::checkString("+1");
	}
}

#endif