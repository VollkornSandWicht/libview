#ifdef LIBVIEW_TEST


#include "catch2/catch.hpp"

#include "LibView/model/translator/tokenizer/OnDemandTokenizer.h" //Testet class


struct SeperatorFieldRangeTest {

	static void checkString(std::string tokenString) {

		LibView::model::translator::tokenizer::OnDemandTokenizer odTok = LibView::model::translator::tokenizer::OnDemandTokenizer(&tokenString);

		REQUIRE(odTok.hasNextToken());
		LibView::model::translator::token::Token createdToken = odTok.getNextToken();
		REQUIRE(!(odTok.hasNextToken()));

		REQUIRE(createdToken.getKey() == LibView::model::translator::token::TokenKeys::SeperatorFieldRange);
		REQUIRE(createdToken.getValue() == tokenString);

	}
};


SCENARIO("The tokenizer reads a seperator for a field range.")
{


	GIVEN(": string.")
	{
		SeperatorFieldRangeTest::checkString(":");
	}
}
#endif