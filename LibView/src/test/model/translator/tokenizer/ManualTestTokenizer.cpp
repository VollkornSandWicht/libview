#ifdef LIBVIEW_TEST


#include "catch2/catch.hpp"

#include "LibView/model/translator/tokenizer/OnDemandTokenizer.h" //Testet class

//Helpers
#include <iostream>

#include <stdio.h>


SCENARIO("A libFile is tokenized and the tokens are printed.")
{

	GIVEN("An example liberty file.")
	{
		int i = 0;
		std::string test =  "+\nnormalized_driver_waveform(ndw_ntin_nvolt_8x2) { \n"
			"index_1(\"0.0004, 0.009027, 0.03931, 0.09714, 0.1067, 0.1872, 0.3137, 0.48\");\n"
			"index_2(\"0, 1\");\n"
			"values(\"0, 0.0005\", \\"
			"\"0, 0.01128\", \\ \n"
			"\"0, 0.04914\", \\"
			"\"0, 0.1214\", \\"
			"\"0, 0.1334\", \\"
			"\"0, 0.234\", \\"
			"\"0, 0.3921\",\\"
			"\"0, 0.6\");"
			"}";
			
		LibView::model::translator::tokenizer::OnDemandTokenizer tokenizer(&test);		
		while (tokenizer.hasNextToken()) {			
			tokenizer.getNextToken();
		}
	}
}
#endif // LIBVIEW_DEBUG