#ifdef LIBVIEW_TEST


#include "catch2/catch.hpp"


//Helpers
#include <iostream>
#include <fstream> 
#include <stdio.h>
#include <stdlib.h>
#include "LibView/model/translator/reader/PreCalculatingCharacterReader.h" //Testet class

SCENARIO("The given file is special.")
{

	GIVEN("An example text file.")
	{

		//setup of the test file if not existing

		std::string text = "";


		WHEN("One calls the reader with a given empty text.")
		{
			THEN("The reader declares not having any characters.")
			{
				LibView::model::translator::reader::PreCalculatingCharacterReader reader(&text);

				REQUIRE(!reader.hasNextCharacter());
			}
		}

		std::string text2 = "a";


		WHEN("One calls the reader with a one character text.")
		{
			THEN("The reader declares having one character, then none.")
			{
				LibView::model::translator::reader::PreCalculatingCharacterReader reader2(&text2);

				REQUIRE(reader2.hasNextCharacter());
				REQUIRE(reader2.getNextCharacter() == 'a');
				REQUIRE(!reader2.hasNextCharacter());
			}

		}


	}
}
#endif // LIBVIEW_DEBUG