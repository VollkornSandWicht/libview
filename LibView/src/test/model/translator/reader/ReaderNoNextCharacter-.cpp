#ifdef LIBVIEW_TEST


#include "catch2/catch.hpp"


#include "LibView/exception/model/reader/NoNextCharacterException.h"

//Helpers
#include <iostream>
#include <fstream> 
#include <stdio.h>
#include <stdlib.h>
#include "LibView/model/translator/reader/PreCalculatingCharacterReader.h" //Testet class

SCENARIO("The reader class is misused by calling getNextCharacter without validation from hasNextCharacter.")
{

	GIVEN("Texts.")
	{

		//setup of the test file if not existing




		WHEN("One calls the reader with single no characater.")
		{
			std::string text = "";
			THEN("The reader throws an error trying to get the next character.")
			{
				LibView::model::translator::reader::PreCalculatingCharacterReader reader(&text);

				REQUIRE(!reader.hasNextCharacter());
				
				REQUIRE_THROWS_AS(reader.getNextCharacter(), LibView::exception::NoNextCharacterException);
			}
		}




		WHEN("One calls the reader with multiple characaters and one ignores no next character available function.")
		{
			std::string text = "a";
			

			THEN("The reader throws an error trying to get the next character, but recognizes that there is a character.")
			{
				LibView::model::translator::reader::PreCalculatingCharacterReader reader(&text);

				REQUIRE(reader.hasNextCharacter());
				reader.getNextCharacter();
				REQUIRE(!reader.hasNextCharacter());
				REQUIRE_THROWS_AS(reader.getNextCharacter(), LibView::exception::NoNextCharacterException);
			}
		}



	}
}
#endif // LIBVIEW_DEBUG