#ifdef LIBVIEW_TEST


#include "catch2/catch.hpp"

#include "LibView/model/translator/ast/Attribute.h" //Testet class

#include "LibView/model/translator/token/Token.h"
#include "LibView/model/translator/token/TokenKeys.h"
#include <vector>

//Helpers
#include <iostream>

#include <stdio.h>

//wrote before isEqual for tokens, requires could be shortend

SCENARIO("A attribute statement is to be created internally.")
{
	LibView::model::translator::token::Token name = LibView::model::translator::token::Token(LibView::model::translator::token::TokenKeys::Name, 1, 1, "attributeName") ;
	std::vector<LibView::model::translator::token::Token> values;
	GIVEN("A Token for the name.")
	{
		LibView::model::translator::ast::Attribute attribute = LibView::model::translator::ast::Attribute(name, values);

		LibView::model::translator::token::Token savedName = attribute.getM_Name();

		REQUIRE(savedName.getColumn() == name.getColumn());
		REQUIRE(savedName.getRow() == name.getRow());
		REQUIRE(savedName.getKey() == name.getKey());
		REQUIRE(savedName.getValue() == name.getValue());
		//the name was not changed in any way
	}
	LibView::model::translator::token::Token val1 = LibView::model::translator::token::Token(LibView::model::translator::token::TokenKeys::Integer, 1, 2, "1");
	values.push_back(val1);



	GIVEN("A Token for the name and one token as a value.")
	{
		LibView::model::translator::ast::Attribute attribute = LibView::model::translator::ast::Attribute(name, values);
		
		LibView::model::translator::token::Token savedName = attribute.getM_Name();

		REQUIRE(savedName.getColumn() == name.getColumn());
		REQUIRE(savedName.getRow() == name.getRow());
		REQUIRE(savedName.getKey() == name.getKey());
		REQUIRE(savedName.getValue() == name.getValue());
		//the name was not changed in any way

		std::vector<LibView::model::translator::token::Token> savedValues = attribute.getM_Value();

		REQUIRE(savedValues.size() == 1);

		REQUIRE(savedValues.front().getColumn() == val1.getColumn());
		REQUIRE(savedValues.front().getRow() == val1.getRow());
		REQUIRE(savedValues.front().getKey() == val1.getKey());
		REQUIRE(savedValues.front().getValue() == val1.getValue());

	}
	LibView::model::translator::token::Token val2 = LibView::model::translator::token::Token(LibView::model::translator::token::TokenKeys::Name, 1, 2, "V");
	values.push_back(val2);

	GIVEN("A Token for the name and two tokens as values.")
	{
		LibView::model::translator::ast::Attribute attribute = LibView::model::translator::ast::Attribute(name, values);

		LibView::model::translator::token::Token savedName = attribute.getM_Name();

		REQUIRE(savedName.getColumn() == name.getColumn());
		REQUIRE(savedName.getRow() == name.getRow());
		REQUIRE(savedName.getKey() == name.getKey());
		REQUIRE(savedName.getValue() == name.getValue());
		//the name was not changed in any way

		std::vector<LibView::model::translator::token::Token> savedValues = attribute.getM_Value();

		REQUIRE(savedValues.size() == 2);

		REQUIRE(savedValues.at(0).getColumn() == val1.getColumn());
		REQUIRE(savedValues.at(0).getRow() == val1.getRow());
		REQUIRE(savedValues.at(0).getKey() == val1.getKey());
		REQUIRE(savedValues.at(0).getValue() == val1.getValue());

		REQUIRE(savedValues.at(1).getColumn() == val2.getColumn());
		REQUIRE(savedValues.at(1).getRow() == val2.getRow());
		REQUIRE(savedValues.at(1).getKey() == val2.getKey());
		REQUIRE(savedValues.at(1).getValue() == val2.getValue());
	}
}
#endif // LIBVIEW_DEBUG