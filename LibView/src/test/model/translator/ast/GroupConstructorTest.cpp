#ifdef LIBVIEW_TEST


#include "catch2/catch.hpp"

#include "LibView/model/translator/ast/Group.h" //Testet class

#include "LibView/model/translator/token/Token.h"
#include "LibView/model/translator/token/TokenKeys.h"


//Helpers
#include <iostream>

#include <stdio.h>

SCENARIO("Use of the group constructor") {
	
	//setup
	std::string type = "groupType";
	uint64_t row = 1;
	uint64_t column = 1;
	LibView::model::translator::token::TokenKeys key = LibView::model::translator::token::TokenKeys::Name;

	std::string name = "groupName";
	std::vector<LibView::model::translator::token::Token> nameList;
	LibView::model::translator::token::Token nameToken = LibView::model::translator::token::Token(key, row, column, name);
	nameList.push_back(nameToken);

	LibView::model::translator::token::Token typeToken = LibView::model::translator::token::Token(key, row, column, type);

	GIVEN("A type token and a list for the name tokens") {
		LibView::model::translator::ast::Group group = LibView::model::translator::ast::Group(typeToken, nameList);

		REQUIRE(group.getName().size() == 1);
		REQUIRE(group.getName()[0].isEqual(nameToken));
		REQUIRE(group.getType().isEqual(typeToken));
		REQUIRE(group.getChildren().size() == 0);
		//REQUIRE(group.getParent() == nullptr); //fix getParent return type;
	}
}

#endif // LIBVIEW_DEBUG