#pragma once
#include "LibViewException.h"

namespace LibView
{
    namespace exception
    {
        /**
         * Map index Exception class for the LibView Project. Inherits from LibViewException
         * Is thrown when a given index is already in map structure
         */
        class MapIndexAlreadyPresentException :
            public LibViewException
        {
        public:

            /**
             * Default constructor of MapIndexAlreadyPresentException.
             */
            MapIndexAlreadyPresentException();


            /**
             * Overloaded constructor of MapIndexAlreadyPresentException. Initializes the location description to a given string.
             *
             * @param location (const char*) : The location description for the exception.
             */
            MapIndexAlreadyPresentException(const char* location);

            /**
             *
             *
             *
             *
             */
            virtual const char* getName()  const noexcept override;

            
        };

    }
}