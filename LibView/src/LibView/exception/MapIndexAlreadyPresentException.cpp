#include "MapIndexAlreadyPresentException.h"


namespace LibView
{
    namespace exception
    {
        MapIndexAlreadyPresentException::MapIndexAlreadyPresentException() : LibViewException()
        {
            generateDescription();
        }
        MapIndexAlreadyPresentException::MapIndexAlreadyPresentException(const char* location) : LibViewException(location)
        {
            generateDescription();
        }
        const char* MapIndexAlreadyPresentException::getName()  const noexcept
        {
            return "MapIndexAlreadyPresentException";
        }
        
    }
}