#pragma once
#include "LibViewException.h"

namespace LibView
{
    namespace exception
    {
        /**
         * Map index Exception class for the LibView Project. Inherits from LibViewException
         * Is thrown when a given index is not found in map structure
         */
        class MapIndexNotPresentException :
            public LibViewException
        {
        public:

            /**
             * Default constructor of MapIndexNotPresentException.
             */
            MapIndexNotPresentException();


            /**
             * Overloaded constructor of MapIndexNotPresentException. Initializes the location description to a given string.
             *
             * @param location (const char*) : The location description for the exception.
             */
            MapIndexNotPresentException(const char* location);

            /**
             *
             *
             *
             *
             */
            virtual const char* getName()  const noexcept override;

            
        };

    }
}