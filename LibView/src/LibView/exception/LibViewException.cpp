#include "LibViewException.h"



namespace LibView
{
    namespace exception
    {
        LibViewException::LibViewException() : m_Location("Unknown location")
        {
        }
        LibViewException::LibViewException(const char* location) : m_Location(location)
        {
        }
        const char* LibViewException::what() const throw ()
        {
            return m_Description.c_str();
        }
        void LibViewException::generateDescription()
        {
            m_Name = getName();
            m_Description = m_Name + " thrown at " + m_Location;
        }
    }
}