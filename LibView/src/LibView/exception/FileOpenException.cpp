#include "FileOpenException.h"


namespace LibView
{
    namespace exception
    {
        FileOpenException::FileOpenException() : LibViewException()
        {
            generateDescription();
        }
        FileOpenException::FileOpenException(const char* location) : LibViewException(location)
        {
            generateDescription();
        }
        const char* FileOpenException::getName()  const noexcept
        {
            return "FileOpenException";

        }
        
    }
}