#include "FileCloseException.h"


namespace LibView
{
    namespace exception
    {
        FileCloseException::FileCloseException() : LibViewException()
        {
            generateDescription();
        }
        FileCloseException::FileCloseException(const char* location) : LibViewException(location)
        {
            generateDescription();
        }
        const char* FileCloseException::getName()  const noexcept
        {
            return "FileCloseException";
        }
       
    }
}