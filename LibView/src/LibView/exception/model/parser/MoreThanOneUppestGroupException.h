#pragma once
#include "LibView/exception/LibViewException.h"
#include "LibView/exception/ExceptionMacros.h"

namespace LibView
{
	namespace exception
	{
		LIBVIEW_DECLARE_EXCEPTION(MoreThanOneUppestGroupException);
	}
}