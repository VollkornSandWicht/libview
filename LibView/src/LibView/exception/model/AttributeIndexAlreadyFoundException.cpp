#include "AttributeIndexAlreadyFoundException.h"
namespace LibView
{
    namespace exception
    {
        LIBVIEW_DEFINE_EXCEPTION(AttributeIndexAlreadyFoundException);     
    }
}