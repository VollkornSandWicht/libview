#pragma once
#include "LibView/exception/LibViewException.h"


namespace LibView
{
    namespace exception
    {
        /**
         * File close Exception class for the LibView Project. Inherits from LibViewException
         */
        class AttributePathAlreadyFoundException :
            public LibViewException
        {
        public:

            /**
             * Default constructor of AttributePathAlreadyFoundException.
             */
            AttributePathAlreadyFoundException();


            /**
             * Overloaded constructor of AttributePathAlreadyFoundException. Initializes the location description to a given string.
             *
             * @param location (const char*) : The location description for the exception.
             */
            AttributePathAlreadyFoundException(const char* location);

            /**
             *
             *
             *
             *
             */
            virtual const char* getName()  const noexcept override;

           

        };
    }
}