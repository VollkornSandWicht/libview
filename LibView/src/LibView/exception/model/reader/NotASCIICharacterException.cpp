#include "NotASCIICharacterException.h"

namespace LibView
{
	namespace exception
	{
		LIBVIEW_DEFINE_EXCEPTION(NOTASCIICharacterException);
	}
}