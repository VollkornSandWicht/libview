#include "AttributeSelectionBox.h"

#include "LibView/presenter/ProgramState.h"
#include "LibView/core/LibFileIndex.h"

#ifndef LIBVIEW_TEST


namespace LibView
{
    namespace view
    {
        AttributeSelectionBox::AttributeSelectionBox(QWidget* parent, IView* pView) :
            Qt_AttributeSelectionBox(parent),
            m_TreeRoots(std::vector<QStandardItem*>()),
            m_SearchBarPlaceholder(findChild<QWidget*>("SearchBarPlaceholder", Qt::FindChildOption::FindChildrenRecursively)),
            m_pView(pView),
            m_MainLayout(new QGridLayout(this)),
            // m_Rows(std::vector<AttributeVisibilityToggle*>()),
            m_SearchBar(new SearchBar(pView, this)),
            m_NumberOfRows(0),
            m_AttributeVisibilityToggles(std::map<index_t, AttributeVisibilityToggleTreeItem*>()),
            m_TreeWidget(findChild<QTreeView*>("TreeView", Qt::FindChildOption::FindChildrenRecursively)),
            m_TreeModel(nullptr)
        {
            initUI();
        }

        void AttributeSelectionBox::addAttributeRow(const std::string& attributeName, const index_t attributeIndex)
        {
            // AttributeVisibilityToggleTreeItem*
            //     newAttributeToggle = new AttributeVisibilityToggle(attributeName, this, m_pView);
            // m_Rows.push_back(newAttributeToggle);
            // m_MainLayout->addWidget(newAttributeToggle, m_NumberOfRows, 0, 1, 1);
            // m_NumberOfRows++;

        }

        IView* AttributeSelectionBox::getViewPointer() const
        {
            return m_pView;
        }
        void AttributeSelectionBox::setAttributeList(const presenter::ProgramState& state)
        {
            if(m_TreeModel != nullptr)
            {
                m_TreeModel->clear();
            }

            m_AttributeVisibilityToggles.clear();

            // Build tree out of list of paths
            // TODO refactor and optimises


            struct HierarchyElement
            {
                std::string base;
                std::string name;
                int hierarchy;
                int parentIndex = -1;
                QStandardItem* item;
                index_t index;
            };

            std::vector<std::vector<HierarchyElement>> hierarchyElements;

            int totalTreeColumns = 0;

            const LIBVIEW_AVIS_MAP& values = state.getAttributeList();

            // Identify base paths for each path and create hierarchical vector containing elements linking to their parents in the previous hierarchy level


            for(const std::pair<index_t, bool>& element : values)
            {
                std::string path = state.getAttribute(element.first);
                std::vector<std::string> hierarchy;
                std::vector<std::string> hierarchyBase;
                std::vector<int> hierarchyParents;

                hierarchyParents.push_back(-1);

                std::string currentString;


                for(size_t i = 0; i < path.length(); i++)
                {
                    if(path.at(i) == '/' || i == path.length() - 1)
                    {
                        if(i == path.length() - 1)
                        {
                            currentString += path.at(i);
                        }


                        hierarchy.push_back(currentString);

                        if(hierarchyBase.size() > 1)
                        {
                            hierarchyBase.push_back(hierarchyBase[hierarchyBase.size() - 1] + "/" + hierarchy[hierarchyBase.size() - 1]);
                        }
                        else if(hierarchyBase.size() > 0)
                        {
                            hierarchyBase.push_back(hierarchy[hierarchyBase.size() - 1]);
                        }
                        else
                        {
                            hierarchyBase.push_back("");
                        }


                        currentString.clear();
                    }
                    else
                    {
                        currentString += path.at(i);
                    }
                }

                //hierarchy.push_back(currentString);



                int sizeDifference = hierarchy.size() - hierarchyElements.size();

                if(sizeDifference > 0)
                {
                    for(int i = 0; i < sizeDifference; i++)
                    {

                        hierarchyElements.push_back(std::vector<HierarchyElement>());

                    }
                }

                for(size_t i = 0; i < hierarchy.size(); i++)
                {
                    int foundNameIndex = -1;
                    bool foundName = false;
                    bool checkThis = false;

                    if(i == hierarchy.size() - 1)
                    {
                        checkThis = true;
                    }

                    for(size_t j = 0; j < hierarchyElements[i].size(); j++)
                    {
                        foundNameIndex = j;

                        if(hierarchyElements[i][j].name == hierarchy[i] && hierarchyElements[i][j].base == hierarchyBase[i])
                        {
                            foundName = true;

                            if(checkThis)
                            {
                                hierarchyElements[i][j].index = element.first;
                                //hierarchyElements[i][j].index = element.first;
                                //hierarchyElements[i][j].index = element.first;
                            }
                            break;
                        }
                    }


                    if(!foundName)
                    {
                        HierarchyElement el;
                        el.name = hierarchy[i];
                        el.hierarchy = i;
                        el.base = hierarchyBase[i];
                        //todo add index to all parents

                        if(i == hierarchy.size() - 1)
                        {
                            el.index = (element.first);
                        }
                        else
                        {
                            el.index = (index_t::requestNull());
                        }
                        el.parentIndex = hierarchyParents[i];

                        hierarchyElements[i].push_back(el);

                        hierarchyParents.push_back(hierarchyElements[i].size() - 1);
                    }
                    else
                    {
                        hierarchyParents.push_back(foundNameIndex);
                    }

                    totalTreeColumns++;
                }
            }

            delete m_TreeModel;

            if(values.size() == 0)
            {
                m_TreeModel = nullptr;
                return;
            }

            m_TreeModel = new AttributeVisibilityToggleTreeItemModel(hierarchyElements[0].size(), 2, getViewPointer());

            int currentRow = 0;


            std::map<int, std::map<int, QList< QStandardItem*>>> nodeColumnMap;

            // generate check box items and tree elements to push into the tree widget
            
            for(size_t hierarchy = 0; hierarchy < hierarchyElements.size(); hierarchy++)
            {


                for(size_t node = 0; node < hierarchyElements[hierarchy].size(); node++)
                {

                    QStandardItem* childItem = new QStandardItem(hierarchyElements[hierarchy][node].name.c_str());
                    hierarchyElements[hierarchy][node].item = childItem;


                    if(hierarchy == 0)
                    {
                        AttributeVisibilityToggleTreeItem* checkBoxItem = new AttributeVisibilityToggleTreeItem(hierarchyElements[0][node].index);
                        checkBoxItem->setCheckState(Qt::CheckState::Checked);

                        m_AttributeVisibilityToggles[checkBoxItem->getIndex()] = checkBoxItem;

                        checkBoxItem->setCheckable(true);
                        m_TreeModel->setItem(currentRow, 0, childItem);
                        m_TreeModel->setItem(currentRow, 1, checkBoxItem);

                        currentRow++;

                        m_TreeRoots.push_back(childItem);
                    }
                    else if(hierarchy > 0)
                    {
                        int parentIndex = hierarchyElements[hierarchy][node].parentIndex;

                        hierarchyElements[hierarchy - 1][parentIndex].item->appendRow(childItem);

                        AttributeVisibilityToggleTreeItem* checkBoxItem = new AttributeVisibilityToggleTreeItem(hierarchyElements[hierarchy][node].index);
                        checkBoxItem->setCheckState(Qt::CheckState::Checked);

                        nodeColumnMap[hierarchy - 1][parentIndex].push_back(checkBoxItem);

                        m_AttributeVisibilityToggles[checkBoxItem->getIndex()] = checkBoxItem;

                    }
                }
            }

            //std::vector<QStandardItem*> alreadyInserted;

            // populate the tree with the created elements
            for(std::pair<int, std::map<int, QList< QStandardItem*>>> hierarchies : nodeColumnMap)
            {
                for(std::pair<int, QList<QStandardItem*>> nodes : hierarchies.second)
                {
                    int hierarchy = hierarchies.first;
                    int node = nodes.first;

                    //if(std::find(alreadyInserted.begin(), alreadyInserted.end(), hierarchyElements[hierarchy]//[node].item) != alreadyInserted.end())
                    //{
                    hierarchyElements[hierarchy][node].item->appendColumn(nodes.second);
                    //   alreadyInserted.push_back(hierarchyElements[hierarchy][node].item);
                   //}
                   //else
                   //{
                   //    for(QStandardItem* item : nodes.second)
                   //    {
                   //        delete item;
                   //    }
                   //}

                   // TODO Check Qt recieves too many columns?


                }
                //hierarchyElements[hierarchy - 1][parentIndex].item->appendColumn(list);
            }

            /*

            for(int r = 0; r < 5; r++)
            {
                for(int c = 0; c < 2; c++)
                {
                    QStandardItem* item = new QStandardItem(QString("Row:%0, Column:%1").arg(r).arg(c));

                    if(c == 0)
                    {
                        for(int i = 0; i < 3; i++)
                        {
                            QStandardItem* child = new QStandardItem(QString("Item %0").arg(i));
                            child->setEditable(false);
                            item->appendRow(child);
                        }

                        model->setItem(r, c, item);
                    }
                }

            }

            */

            m_TreeModel->setHorizontalHeaderItem(0, new QStandardItem("Attribute"));
            m_TreeModel->setHorizontalHeaderItem(1, new QStandardItem("Visibility"));

            m_TreeWidget->setModel(m_TreeModel);

            m_TreeModel->setInit();



        }

        void AttributeSelectionBox::updateAttributeVisibilities(const presenter::ProgramState& state)
        {
            if(m_TreeModel == nullptr)
            {
                return;
            }

            m_TreeModel->setDeInit();



            for(const std::pair<index_t, bool>& indexVisibilityPair : state.getAttributeList())
            {
                m_AttributeVisibilityToggles.at(indexVisibilityPair.first)->setCheckState(indexVisibilityPair.second ? Qt::CheckState::Checked : Qt::CheckState::Unchecked);
            }

            m_TreeModel->setInit();
        }

        void AttributeSelectionBox::initUI()
        {
            QGridLayout* layout = new QGridLayout(m_SearchBarPlaceholder);
            layout->addWidget(m_SearchBar, 0, 0, 1, 1);
            m_SearchBarPlaceholder->setLayout(layout);
            layout->setSpacing(0);
            layout->setMargin(0);
        }
    }
}

#endif