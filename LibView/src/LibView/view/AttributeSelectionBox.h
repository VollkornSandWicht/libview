#pragma once

#ifndef LIBVIEW_TEST

#include"LibView/qt/Qt_AttributeSelectionBox.h"
#include "AttributeVisibilityToggleTreeItem.h"
#include "LibView/view/SearchBar.h"
#include "LibView/core/LibFileIndex.h"
#include <string>
#include <vector>

#include "ILibViewGuiElement.h"
#include <QTreeView>
#include <QStandardItemModel>
#include "AttributeVisibilityToggleTreeItemModel.h"

namespace LibView
{
    namespace view
    {
        class AttributeSelectionBox : public Qt_AttributeSelectionBox, public ILibViewGuiElement
        {
        private:
            QGridLayout* m_MainLayout;
            SearchBar* m_SearchBar;
            //std::vector<AttributeVisibilityToggleTreeItem*> m_Rows;
            int m_NumberOfRows;
            IView* m_pView;

            QWidget* m_SearchBarPlaceholder;
            QTreeView* m_TreeWidget;

            AttributeVisibilityToggleTreeItemModel* m_TreeModel;

            std::vector<QStandardItem*> m_TreeRoots;

            std::map <index_t, AttributeVisibilityToggleTreeItem*> m_AttributeVisibilityToggles;

            void initUI();

        public:
            AttributeSelectionBox(QWidget* parent = Q_NULLPTR, IView* pView = nullptr);

            void addAttributeRow(const std::string& attributeName, const index_t attributeIndex);
        
            IView* getViewPointer() const override;

            void setAttributeList(const presenter::ProgramState& state);

            void updateAttributeVisibilities(const presenter::ProgramState& state);

        };
    }
}
#endif