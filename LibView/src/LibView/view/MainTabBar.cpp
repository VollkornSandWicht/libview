#ifndef LIBVIEW_TEST

#include "MainTabBar.h"

#include <QLabel>

namespace LibView
{
    namespace view
    {
        void MainTabBar::initUI()
        {
            QGridLayout* contentDisplayTabWidgetLayout = new QGridLayout(m_ContentDisplayTabWidget);
            QGridLayout* libFileDisplayTabWidgetLayout = new QGridLayout(m_LibFileDisplayTabWidget);


            contentDisplayTabWidgetLayout->addWidget(m_ContentDisplayFileSelection, 0, 0, 1, 1);
            libFileDisplayTabWidgetLayout->addWidget(m_LibFileDisplay, 0, 0, 1, 1);

            





            contentDisplayTabWidgetLayout->addWidget(m_ContentDisplay, 1, 0, 1, 1);
            //libFileDisplayTabWidgetLayout->addWidget(m_LibFileDisplay, 0, 0, 1, 1);

            m_ContentDisplay->setText("(No content provided)");
            m_ContentDisplayTabWidget->setLayout(contentDisplayTabWidgetLayout);
            
            m_LibFileDisplayTabWidget->setLayout(libFileDisplayTabWidgetLayout);
            


        }


        MainTabBar::MainTabBar(QWidget* parent, IView* viewPointer) :
            Qt_MainTabBar(parent),
            m_TabWidget(findChild<QTabWidget*>("MainTabBar", Qt::FindChildOption::FindChildrenRecursively)),
            m_ContentDisplayTabWidget(nullptr),
            m_LibFileDisplayTabWidget(nullptr),
            m_ContentDisplay(new ContentDisplay()),
            m_LibFileDisplay(new LibFileDataDisplay()),
            m_ContentDisplayFileSelection(new LibFileSelectionComboBox(this, viewPointer))
        {
            m_ContentDisplayTabWidget = m_TabWidget->widget(1);
            m_LibFileDisplayTabWidget = m_TabWidget->widget(0);



            m_ContentDisplay->setParent(m_ContentDisplayTabWidget);
            m_LibFileDisplay->setParent(m_LibFileDisplayTabWidget);

            initUI();
        }
        void MainTabBar::setContentDisplayContent(const std::string& text)
        {
            m_ContentDisplay->setText(text);
        }
        void MainTabBar::remove(const index_t& index)
        {
            m_ContentDisplayFileSelection->remove(index);
            m_LibFileDisplay->remove(index);
        
        }
        void MainTabBar::loadLibFile(const presenter::ProgramState& state, const model::data_structure::LibertyFileData* fileData)
        {
            m_ContentDisplayFileSelection->loadLibFile(fileData);
            m_LibFileDisplay->loadLibFile(state, fileData);
        }
        void MainTabBar::setLibFileVisibility(const index_t& index, bool visibility)
        {
            m_LibFileDisplay->setLibFileVisibility(index, visibility);
        }
        void MainTabBar::redrawLibFiles(const presenter::ProgramState& state)
        {
            m_LibFileDisplay->redrawLibFiles(state);
        }
        void MainTabBar::setContentDisplay(const std::string& text)
        {
            m_ContentDisplay->setText(text);
        }
    }
}


#endif