#pragma once

#include <QStandardItem>

#include "LibView/core/LibFileIndex.h"


namespace LibView
{
    namespace view
    {
        class AttributeVisibilityToggleTreeItem : public QStandardItem
        {
            

        private:

            index_t m_Index;


        public:

            AttributeVisibilityToggleTreeItem(const index_t& index);
            inline index_t& getIndex() { return m_Index; }
        };
    }
}