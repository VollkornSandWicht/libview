#include "LibViewMainWindow.h"


#ifndef LIBVIEW_TEST


namespace LibView
{

    namespace view
    {

        void LibViewMainWindow::initUI()
        {
            setObjectName(QString::fromUtf8("LibView"));
            resize(1200, 600);

            m_Centralwidget->setObjectName(QString::fromUtf8("centralwidget"));
            m_Centralwidget->setLayout(m_CentralWidgetLayout);
            setCentralWidget(m_Centralwidget);

            m_Menubar->setObjectName(QString::fromUtf8("menubar"));
            m_Menubar->setGeometry(QRect(0, 0, 1200, 21));
            setMenuBar(m_Menubar);

            m_Statusbar->setObjectName(QString::fromUtf8("statusbar"));
            setStatusBar(m_Statusbar);



            m_CentralWidgetLayout->addWidget(m_Splitter, 0, 0, 1, 1);
            
            m_Splitter->addWidget(m_SideMenu);
            m_Splitter->addWidget(m_MainTabBar);
            
            //m_CentralWidgetLayout->addWidget(m_SideMenu, 0, 0, 1, 1);
            //m_CentralWidgetLayout->addWidget(m_MainTabBar, 0, 1, 1, 1);
            //m_CentralWidgetLayout->setColumnStretch(0, 0);
            //m_CentralWidgetLayout->setColumnStretch(1, 1);













            QMetaObject::connectSlotsByName(this);
        }

        void LibViewMainWindow::setContentDisplay(const std::string& text)
        {
            m_MainTabBar->setContentDisplay(text);
        }

        LibViewMainWindow::LibViewMainWindow(IView* viewPointer) :
            m_Splitter(new QSplitter()),
            m_Centralwidget(new QWidget(this)),
            m_Menubar(new QMenuBar(this)),
            m_Statusbar(new QStatusBar(this)),
            m_SideMenu(new SideMenu(this, viewPointer)),
            m_CentralWidgetLayout(new QGridLayout()),
            m_MainTabBar(new MainTabBar(this, viewPointer)),
            m_ViewPointer(viewPointer)

        {
            m_Splitter->setParent(m_Centralwidget);
            m_SideMenu->setParent(m_Splitter);
            m_MainTabBar->setParent(m_Splitter);
            m_CentralWidgetLayout->setParent(m_Centralwidget);

            //setStyleSheet("QWidget { border: 1px dashed red; }");

            initUI();
        }

        void LibViewMainWindow::setContentDisplayContent(const std::string& text)
        {
            m_MainTabBar->setContentDisplayContent(text);
        }

        void LibViewMainWindow::clearLibFiles()
        {
            // TODO : m_MainTabBar->clearLibFiles();
            // TODO : m_SideMenu->clearLibFiles();
        }

        void LibViewMainWindow::remove(const index_t& index)
        {
            m_MainTabBar->remove(index);
            m_SideMenu->remove(index);
        }

        void LibViewMainWindow::loadLibFile(const presenter::ProgramState& state, const model::data_structure::LibertyFileData * fileData)
        {
            m_MainTabBar->loadLibFile(state, fileData);
            m_SideMenu->loadLibFile(fileData);
        }

        void LibViewMainWindow::loadAttributeList(const presenter::ProgramState& state)
        {
            m_SideMenu->loadAttributeList(state);
        }

        void LibViewMainWindow::setLibFileVisibility(const index_t& index, bool visibility)
        {
            m_MainTabBar->setLibFileVisibility(index, visibility);
        }

        void LibViewMainWindow::updateAttributeVisibilities(const presenter::ProgramState& state)
        {
            m_SideMenu->updateAttributeVisibilities(state);
        }

        void LibViewMainWindow::redrawLibFiles(const presenter::ProgramState& state)
        {
            m_MainTabBar->redrawLibFiles(state);
        }

        void LibViewMainWindow::init()
        {
            m_SideMenu->init();
        }



        IView* LibViewMainWindow::getViewPointer() const
        {
            return m_ViewPointer;
        }
    }

}

#endif