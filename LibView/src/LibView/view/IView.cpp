/**
 * Project LibView
 * @author Leonard Wolf (2207778),  Mirko Sowa (2263258), Daniel Martins Fiebich (2219734), Christoph Böhrer (2070080),Justin Guth (2218548)
 * @version 1.0
 */
#ifndef LIBVIEW_TEST
#include "IView.h"
#include <string>
#include <vector>
 /**
  * IView implementation
  */

namespace LibView
{
	namespace view
	{
		IView::IView(){}
		


		/**
		 * @return bool
		 */
		bool IView::promptConfirmationMessage() {
			return false;
		}



		/**
		 * receives th eorder in which the view hast to draw the liberty files according to the sorting parameter
		 *
		 * @param sortedLibFileList
		 */
		void IView::receiveSortedLibFileList(std::vector<int64_t> sortedLibFileList) {

		}

		/**
		 * received the filtered attribute list after a search term selection
		 *
		 * @param filteredAttributeList
		 */
		void IView::receiveFilteredAttributeList(std::vector<int64_t> filteredAttributeList) {

		}
	}
}
#endif