#pragma once

#ifndef LIBVIEW_TEST

#include "LibView/qt/Qt_ContentDisplay.h"
#include "LibView/model/data_structure/LibertyFileData.h"

namespace LibView
{
    namespace view
    {
        class ContentDisplay : public Qt_ContentDisplay
        {
        private: 

            QTextBrowser* m_TextBrowser;

        public:

            ContentDisplay(QWidget* parent = Q_NULLPTR);
            void setText(const std::string& text);
            void loadLibFile(const model::data_structure::LibertyFileData* fileData);
            void remove(const index_t& index);

        };
    }
}


#endif // !LIBVIEW_TEST
