#include "ContentDisplay.h"

#ifndef LIBVIEW_TEST

namespace LibView
{
    namespace view
    {
        ContentDisplay::ContentDisplay(QWidget* parent) :
            Qt_ContentDisplay(parent),
            m_TextBrowser(findChild<QTextBrowser*>("TextBrowser", Qt::FindChildOption::FindChildrenRecursively))
        {}

        void ContentDisplay::setText(const std::string & text)
        {
            m_TextBrowser->setText(text.c_str());
        }

        void ContentDisplay::loadLibFile(const model::data_structure::LibertyFileData* fileData)
        {
            
        }

        void ContentDisplay::remove(const index_t& index)
        {

        }

    }
}
#endif