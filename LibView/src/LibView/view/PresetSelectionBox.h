#pragma once

#ifndef LIBVIEW_TEST
#include "LibView/qt/Qt_PresetSelectionBox.h"
#include <QVBoxLayout>

#include "ViewKnower.h"
#include "PresetButton.h"


namespace LibView
{
    namespace view
    {
        



        class PresetSelectionBox : public Qt_PresetSelectionBox, public ViewKnower
        {
        private:
            QVBoxLayout* m_MainLayout;

        public:
            PresetSelectionBox(IView* view, QWidget* parent = Q_NULLPTR);

            void addPresetRow(const std::string& presetName, const index_t& index);
            
            void initUI();
        
            void loadPresets();

        };
    }
}
#endif