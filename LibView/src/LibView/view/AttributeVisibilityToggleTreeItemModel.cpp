#include "AttributeVisibilityToggleTreeItemModel.h"

#include "LibView/core/DebugLog.h"

namespace LibView
{
    namespace view
    {
        AttributeVisibilityToggleTreeItemModel::AttributeVisibilityToggleTreeItemModel(int rows, int columns, IView* view) :
            ViewKnower(view),
            QStandardItemModel(rows, columns),
            m_IsInitialised(false)
        {
            connect(this, SIGNAL(itemChanged(QStandardItem*)), this, SLOT(onItemChanged(QStandardItem*)));
        }

        void AttributeVisibilityToggleTreeItemModel::onItemChanged(QStandardItem* item)
        {
            if(!m_IsInitialised)
            {
                return;
            }

            LIBVIEW_LOG("Item change detected!");
            index_t  index = ((AttributeVisibilityToggleTreeItem*) item)->getIndex();
            LIBVIEW_LOG(index.index());


            getView()->requestAttributeVisibilityToggle(
                index,
                item->checkState() == Qt::CheckState::Checked ? true : false
            );

        }
    }
}