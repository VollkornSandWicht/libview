#pragma once

#ifndef LIBVIEW_TEST

#include "LibView/qt/Qt_LibFileSelectionBox.h"
#include "LibView/view/LibFileVisibilityToggle.h"
#include "LibView/core/LibFileIndex.h"
#include "ILibViewGuiElement.h"
#include <QScrollArea>

namespace LibView
{
    namespace view
    {
        class LibFileSelectionBox : public Qt_LibFileSelectionBox, public ILibViewGuiElement
        {
        private:
            QScrollArea* m_ScrollArea;
            std::vector<LibFileVisibilityToggle*> m_Rows;
            int m_NumberOfRows;

            IView* m_View;

        public:

            LibFileSelectionBox(QWidget* parent = Q_NULLPTR, IView* viewPointer = nullptr);

            void addLibFileRow(const std::string& libFileName, const index_t libFileIndex);
        
            void remove(const index_t& index);
        
            IView* getViewPointer() const override;
        };
    }
}
#endif