#pragma once

#include "IView.h"


namespace LibView {
	namespace view {
		class ILibViewGuiElement {

		public:

			virtual IView* getViewPointer() const = 0;
		};
	}
}