#ifndef LIBVIEW_TEST

#include "SearchBar.h"

namespace LibView
{
    namespace view
    {
        SearchBar::SearchBar(IView* view, QWidget* parent) :
            Qt_SearchBar(parent),
            ViewKnower(view),
            m_LineEdit(findChild<QLineEdit*>("SearchLineEdit", Qt::FindChildOption::FindChildrenRecursively)),
            m_SearchButton(findChild<QPushButton*>("SearchBarSearchButton", Qt::FindChildOption::FindChildrenRecursively))
        {
            layout()->setMargin(0);
            setMaximumHeight(25);

            connect(m_SearchButton, SIGNAL(clicked()), this, SLOT(onSearchRequested()));
        }



        std::string SearchBar::getSearchContent() const
        {
            return m_LineEdit->text().toStdString();
        }
        bool SearchBar::isSearchEmpty() const
        {
            return m_LineEdit->text().isEmpty();
        }

        void SearchBar::onSearchRequested()
        {
            getView()->onSearchRequested(m_LineEdit->text().toStdString());
        }
    }
}



#endif // !LIBVIEW_TEST
