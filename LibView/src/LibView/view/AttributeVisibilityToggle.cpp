#ifndef LIBVIEW_TEST

#include "AttributeVisibilityToggle.h"



namespace LibView
{
    namespace view
    {
        AttributeVisibilityToggle::AttributeVisibilityToggle(const std::string& name, QWidget* parent, IView* pView) :
            Qt_AttributeVisibilityToggle(parent),
            m_Name(name),
            m_View(pView),
            m_AttributeVisibilityToggle(findChild<QCheckBox*>("AttributeVisibilityToggleCheckBox", Qt::FindChildOption::FindChildrenRecursively))
        {
          
        }

        IView* AttributeVisibilityToggle::getViewPointer() const
        {
            return m_View;
        }

        void AttributeVisibilityToggle::initUI()
        {
            m_AttributeVisibilityToggle->setText(m_Name.c_str());
        }
    }
}

#endif
