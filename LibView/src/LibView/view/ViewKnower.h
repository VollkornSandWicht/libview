#pragma once

#include "IView.h"

namespace LibView
{
    namespace view
    {
        class ViewKnower
        {
        private:

            IView* m_View;

        public:

            inline ViewKnower(IView* view) { m_View = view; }
            
            inline IView* getView() { return m_View; }
        };
    }
}