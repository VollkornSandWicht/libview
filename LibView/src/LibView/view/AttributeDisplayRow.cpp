#include "AttributeDisplayRow.h"
#ifndef LIBVIEW_TEST

namespace LibView
{
    namespace view
    {
        AttributeDisplayRow::AttributeDisplayRow(index_t& index, std::string name, std::string value, QWidget* parent) : Qt_AttributeDisplayRow(parent),
            m_Index(index),
            m_AttributeNameLabel(findChild<QLabel*>("attributeName",Qt::FindChildOption::FindChildrenRecursively)),
            m_AttributeValueLabel(findChild<QLabel*>("AttributeValue",Qt::FindChildOption::FindChildrenRecursively))
        {
            m_AttributeNameLabel->setText(name.c_str());
            m_AttributeValueLabel->setText(value.c_str());
        }

        AttributeDisplayRow::~AttributeDisplayRow()
        {}
    }
}
#endif // !LIBVIEW_TEST