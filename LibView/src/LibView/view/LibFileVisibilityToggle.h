#pragma once

#ifndef LIBVIEW_TEST

#include "LibView/qt/Qt_LibFileVisibilityToggle.h"
#include "LibView/core/LibFileIndex.h"

#include "ILibViewGuiElement.h"

namespace LibView
{
    namespace view
    {
        class LibFileVisibilityToggle : public Qt_LibFileVisibilityToggle, public ILibViewGuiElement
        {
            Q_OBJECT

        private:
            QCheckBox* m_LibFileVisibilityToggle;
            QWidget* m_RemoveButtonPlaceHolder;
            std::string m_Name;
            index_t m_Index;

            IView* m_View;

            void initUI();

        private slots:

            void requestLibFileVisibility();

        public:

            LibFileVisibilityToggle(const std::string& name, const index_t& index, QWidget* parent = Q_NULLPTR, IView* pView = nullptr);
       
            inline index_t getIndex() { return m_Index; }


            IView* getViewPointer() const override;
        };
    }
}

#endif