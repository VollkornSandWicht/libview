#pragma once

#ifndef LIBVIEW_TEST



#include "LibView/qt/Qt_MainTabBar.h"
#include "ContentDisplay.h"
#include "SideMenu.h"
#include "LibFileSelectionComboBox.h"
#include "LibFileDataDisplay.h"

namespace LibView
{
    namespace view
    {
        class MainTabBar : public Qt_MainTabBar
        {
        private:

            void initUI();

            ContentDisplay* m_ContentDisplay;
            LibFileDataDisplay* m_LibFileDisplay;
            LibFileSelectionComboBox* m_ContentDisplayFileSelection;
            QTabWidget* m_TabWidget;


            QWidget* m_ContentDisplayTabWidget;
            QWidget* m_LibFileDisplayTabWidget;


        public:
            
            MainTabBar(QWidget* parent = Q_NULLPTR, IView* viewPointer = nullptr);
            void setContentDisplay(const std::string& text);

            void setContentDisplayContent(const std::string& text);

            void remove(const index_t& index);
            void loadLibFile(const presenter::ProgramState& state, const model::data_structure::LibertyFileData* fileData);
            void setLibFileVisibility(const index_t& index, bool visibility);

            void redrawLibFiles(const presenter::ProgramState& state);
        };
    }
}

#endif