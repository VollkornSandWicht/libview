#pragma once


#ifndef LIBVIEW_TEST


#include "ILibViewGuiElement.h"

#include "LibView/qt/Qt_AttributeVisibilityToggle.h"

namespace LibView
{
    namespace view
    {
        class AttributeVisibilityToggle : public Qt_AttributeVisibilityToggle , public ILibViewGuiElement
        {
        private: 
            
            QCheckBox* m_AttributeVisibilityToggle;
            std::string m_Name;

            IView* m_View;

            void initUI();

        public:
            
            AttributeVisibilityToggle(const std::string& name, QWidget* parent = Q_NULLPTR, IView* pView = nullptr);
            
            IView* getViewPointer() const override;

        };
    }
}

#endif