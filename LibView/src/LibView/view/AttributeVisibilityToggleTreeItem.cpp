#include "AttributeVisibilityToggleTreeItem.h"

namespace LibView
{
    namespace view
    {
        AttributeVisibilityToggleTreeItem::AttributeVisibilityToggleTreeItem(const index_t& index) :
            QStandardItem(),
            m_Index(index)

        {
            setCheckable(true);
            
        }
    }
}
