#pragma once
#ifndef LIBVIEW_TEST
#include "LibView/qt/Qt_AttributeDisplayRow.h"

#include "LibView/core/LibFileIndex.h"

namespace LibView
{
    namespace view
    {
        class AttributeDisplayRow : public Qt_AttributeDisplayRow
        {
        private:
            QLabel* m_AttributeNameLabel;
            QLabel* m_AttributeValueLabel;
        
            index_t m_Index;
        
        public:
            AttributeDisplayRow(index_t& index, std::string name, std::string value, QWidget* parent = Q_NULLPTR);
            ~AttributeDisplayRow();

            inline void setAttributeName(const std::string& name){m_AttributeNameLabel->setText(QString(name.c_str()));}
            
            inline void setAttributeValue(const std::string& value){m_AttributeNameLabel->setText(QString(value.c_str()));}

            inline index_t& getIndex() { return m_Index; };
        };
    }
}
#endif