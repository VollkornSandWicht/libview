#ifndef LIBVIEW_TEST
#include "LibFileSelectionComboBox.h"

namespace LibView
{
    namespace view
    {
        void LibFileSelectionComboBox::initUI()
        {
            connect(m_LibFileSelectionComboBox, SIGNAL(activated(int)), this, SLOT(onBoxSelected(int)));
        }
        LibFileSelectionComboBox::LibFileSelectionComboBox(QWidget* parent, IView* viewPointer) :
            Qt_LibFileSelectionComboBox(parent),
            m_LibFileSelectionComboBox(findChild<QComboBox*>("LibFileSelectionComboBox", Qt::FindChildOption::FindChildrenRecursively)),
            m_ViewPointer(viewPointer)
        {
            initUI();
        }

        void LibFileSelectionComboBox::clear()
        {
            m_LibFileSelectionComboBox->clear();
            m_Indices.clear();
        }

        void LibFileSelectionComboBox::remove(const index_t& index)
        {
            // TODO FIX NOT ALL ELEMENTS DISAPPEARING

            size_t counter = 0;

            for(const index_t& indexIt : m_Indices)
            {
                if(indexIt == index)
                {
                    m_LibFileSelectionComboBox->removeItem(counter);

                    break;
                }
            
                ++counter;
            }

            m_Indices.erase(m_Indices.begin() + counter);
        }

        void LibFileSelectionComboBox::addLibFile(const std::string& name, const index_t& index)
        {
            m_LibFileSelectionComboBox->addItem(name.c_str());
            m_Indices.push_back(index);
        }

        IView* LibFileSelectionComboBox::getViewPointer() const
        {
            return nullptr;
        }

        void LibFileSelectionComboBox::loadLibFile(const model::data_structure::LibertyFileData* fileData)
        {
            addLibFile(fileData->getDisplayName(), fileData->getLibFileIndex());
        }


        void LibFileSelectionComboBox::onBoxSelected(int selectedIndex)
        {
            m_ViewPointer->onContentDisplayFileSelection(m_Indices.at(selectedIndex));
        }
    }
}
#endif