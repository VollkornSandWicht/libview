#include "LibFileSelectionBox.h"
#ifndef LIBVIEW_TEST


namespace LibView
{
    namespace view
    {
        LibFileSelectionBox::LibFileSelectionBox(QWidget* parent, IView* viewPointer) : Qt_LibFileSelectionBox(parent),
            m_Rows(std::vector<LibFileVisibilityToggle*>()),
            m_NumberOfRows(0),
            m_View(viewPointer),
            m_ScrollArea(findChild<QScrollArea*>("ScrollArea", Qt::FindChildOption::FindChildrenRecursively))
        {
            QWidget* contentWidget = new QWidget(m_ScrollArea);
            QVBoxLayout* scrollLayout = new QVBoxLayout(contentWidget);
            scrollLayout->setAlignment(Qt::AlignTop);
            scrollLayout->setContentsMargins(5, 0, 0, 0);
            scrollLayout->setSpacing(0);
            contentWidget->setLayout(scrollLayout);
            m_ScrollArea->setWidget(contentWidget);
        }



        void LibFileSelectionBox::addLibFileRow(const std::string& libFileName, const index_t libFileIndex) 
        {
            LibFileVisibilityToggle* newLibFiletoggle = new LibFileVisibilityToggle(libFileName,libFileIndex,  this, m_View);
            m_Rows.push_back(newLibFiletoggle);
            m_ScrollArea->widget()->layout()->addWidget(newLibFiletoggle);
            m_ScrollArea->widget()->layout()->setMargin(0);
            m_NumberOfRows++;
        
        }
        void LibFileSelectionBox::remove(const index_t& index)
        {
            size_t counter = 0;
            LibFileVisibilityToggle* toRemove = nullptr;


            for(LibFileVisibilityToggle* toggle : m_Rows)
            {
                if(toggle->getIndex() == index)
                {
                    toRemove = toggle;
                    break;
                }

                ++counter;
            }

            toRemove->deleteLater();
            m_Rows.erase(m_Rows.begin() + counter);
        }
        IView* LibFileSelectionBox::getViewPointer() const
        {
            return m_View;
        }
    }
}
#endif