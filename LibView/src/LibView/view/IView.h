#pragma once

#include <vector>
#include <string>

#include "LibView/presenter/PresenterFacade_decl.h"
#include "LibView/presenter/ProgramState.h"


namespace LibView
{
	namespace view
	{
		class IView
		{
		public:

			IView();


			/**
			 * prompt the user with a libFile selection dialog and return a list of files
			 */
			virtual std::vector<std::string> promptLibFileSelection() = 0;

			/**
			 * prompt the user with a confirmation message
			 */
			bool promptConfirmationMessage();

			/**
			 * receives the current program state to get the needed information to draw the display
			 * @param programState
			 */
			virtual void receiveProgramState(const presenter::ProgramState& programState)  = 0;

			/**
			 * receives th eorder in which the view hast to draw the liberty files according to the sorting parameter
			 *
			 * @param sortedLibFileList
			 */
			void receiveSortedLibFileList(std::vector <int64_t> sortedLibFileList);

			/**
			 * received the filtered attribute list after a search term selection
			 *
			 * @param filteredAttributeList
			 */
			void receiveFilteredAttributeList(std::vector<int64_t> filteredAttributeList);


			/**
			 * Call the file load request in the presenter
			 */
			virtual void notifyPresenterOfFileLoadRequest() = 0;

			/**
			 * returns a pointer to the presenter
			 */
			virtual presenter::PresenterFacade* getPresenterPointer() = 0;

			/**
			 * Requests the presenter of a selection made in the content display
			 */
			virtual void onContentDisplayFileSelection(const index_t& index) = 0;

			/**
			 * Sets the contents of the file content display to a given text
			 */
			virtual void setContentDisplayContent(const std::string& text) = 0;

			/**
			 * notifies the presenter of a requested removal of a file
			 */
			virtual void libFileRemovalRequested(const index_t& index) = 0;

			/**
			 * notifies presenter of a requested file visibility
			 */
			virtual void requestLibFileVisibility(const index_t& index, bool visibility) = 0;

			/**
			 * sets the file visibility in the display according to the parameters
			 */
			virtual void setLibFileVisibility(const index_t& index, bool visibility) = 0;

			/**
			 * notfies presenter of a requested change in attribute visibilities
			 */
			virtual void requestAttributeVisibilityToggle(const index_t& index, bool visibility) = 0;

			/**
			 * updates the attribute visibilities in the display according to the current state in the presenter
			 */
			virtual void updateAttributeVisibilities(const presenter::ProgramState& state) = 0;

			/**
			 * returns a list of presets retrieved from the presenter
			 */
			virtual const std::vector<model::APreset*>& getPresetList() const = 0;

			/**
			 * notifies the presenter of a requested file load
			 */
			virtual void requestPresetLoad(const index_t& index) = 0;

			/**
			 * notifies the presenter of a search request
			 */
			virtual void onSearchRequested(const std::string& reg) = 0;
		};
	}
}