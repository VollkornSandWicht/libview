#pragma once


#ifndef LIBVIEW_TEST

#include "LibView/qt/Qt_SearchBar.h"
#include "ViewKnower.h"
#include <string>

namespace LibView
{
    namespace view
    {
        class SearchBar : public Qt_SearchBar, public ViewKnower
        {
            Q_OBJECT

        private:

            QLineEdit* m_LineEdit;
            QPushButton* m_SearchButton;

        public:

            SearchBar(IView* view, QWidget* parent = Q_NULLPTR);

            std::string getSearchContent() const;
            bool isSearchEmpty() const;

        private slots: 

            void onSearchRequested();

        };
    }
}





#endif