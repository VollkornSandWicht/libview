#pragma once

#include "LibView/core/LibFileIndex.h"
#include "ViewKnower.h"


#include <QPushButton>

namespace LibView
{
    namespace view
    {
        class PresetButton : public QPushButton, public ViewKnower
        {
            Q_OBJECT


        private:

            index_t m_Index;
             

        public:

            PresetButton(IView* pView, const index_t& index, const std::string& name, QWidget* parent = Q_NULLPTR);

        private slots:

            void onButtonClick();
        };
    }
}