/**
 *  _             _              _
 * (_)           | |            | |
 *  _  _ __    __| |  ___ __  __| |_
 * | || '_ \  / _` | / _ \\ \/ /| __|
 * | || | | || (_| ||  __/ >  < | |_
 * |_||_| |_| \__,_| \___|/_/\_\ \__|
 *                           ______
 *                          |______|
 *
 * @author Justin Guth at gitlab.com/justin-guth.de
 *
 * Repository at: https://gitlab.com/justin-guth.de/indextype.git
 *
 */








#pragma once

#include <stdint.h>
#include <functional>


namespace JG
{
    namespace util
    {

        template <typename T_Type>
        class IndexTypeTemplate
        {
        private:
            static T_Type s_Counter;

            T_Type m_IndexValue;
            bool m_IsNull;

            IndexTypeTemplate(T_Type value);
            IndexTypeTemplate(T_Type value, bool isNull);

        public:
            /**
             * Default constructor; creates Null Value
             */
            IndexTypeTemplate();

            /**
             * Copy constructor.
             */
            IndexTypeTemplate(const IndexTypeTemplate<T_Type>& other);

            /**
             * Assignment Operator.
             *
             * @param other (const IndexTypeTemplate<T_Type>&) : A reference to the other object.
             *
             * @return IndexTypeTemplate<T_Type> : A reference to the assigned object
             */
            IndexTypeTemplate<T_Type> operator=(const IndexTypeTemplate<T_Type>& other);

            /**
             * Equivalence operator
             *
             * @param other (const IndexTypeTemplate<T_Type>&) : A reference to the other object.
             *
             * @return bool : Whether the objects are equivalent
             */
            bool operator==(const IndexTypeTemplate<T_Type>& other) const;

            /**
             * Less than operator
             *
             * @param other (const IndexTypeTemplate<T_Type>&) : A reference to the other object.
             *
             * @return bool : Whether the object is less than the other
             */
            bool operator<(const IndexTypeTemplate<T_Type>& other) const;

            /**
             * Greater than operator
             *
             * @param other (const IndexTypeTemplate<T_Type>&) : A reference to the other object.
             *
             * @return bool : Whether the object is greater than the other
             */
            bool operator>(const IndexTypeTemplate<T_Type>& other) const;

            /**
             * Antivalence operator
             *
             * @param other (const IndexTypeTemplate<T_Type>&) : A reference to the other object.
             *
             * @return bool : Whether the objects are not equivalent
             */
            bool operator!=(const IndexTypeTemplate<T_Type>& other) const;

            /**
             * Returns wether the object is null
             *
             * @return bool : Whether the object is null
             */
            inline bool isNull() const { return m_IsNull; }

            /**
             * Returns the integer index of the object
             *
             * @return bool : The integer index
             */
            inline T_Type index() const { return m_IndexValue; }

            /**
             * Returns a new unique index
             *
             * @return IndexTypeTemplate<T_Type> : The index
             */
            static IndexTypeTemplate<T_Type> requestUnique();

            /**
             * Returns a new null index
             *
             * @return IndexTypeTemplate<T_Type> : The null index
             */
            static IndexTypeTemplate<T_Type> requestNull();



            #ifdef LIBVIEW_TEST
            static void reset();
            #endif
        };

        template <typename T_Type>
        inline IndexTypeTemplate<T_Type>::IndexTypeTemplate() : m_IndexValue(0),
            m_IsNull(true)
        {}

        template <typename T_Type>
        inline IndexTypeTemplate<T_Type>::IndexTypeTemplate(T_Type value) : m_IndexValue(value),
            m_IsNull(false)
        {}

        template <typename T_Type>
        inline IndexTypeTemplate<T_Type>::IndexTypeTemplate(T_Type value, bool isNull) : m_IndexValue(value),
            m_IsNull(isNull)
        {}

        template <typename T_Type>
        inline IndexTypeTemplate<T_Type>::IndexTypeTemplate(const IndexTypeTemplate<T_Type>& other) : IndexTypeTemplate(other.index(), other.isNull())
        {}

        template <typename T_Type>
        inline IndexTypeTemplate<T_Type> IndexTypeTemplate<T_Type>::operator=(const IndexTypeTemplate<T_Type>& other)
        {
            m_IndexValue = other.index();
            m_IsNull = other.isNull();

            return *this;
        }

        template <typename T_Type>
        inline bool IndexTypeTemplate<T_Type>::operator==(const IndexTypeTemplate<T_Type>& other) const
        {
            // Check if both have the same null flag
            if(m_IsNull == other.m_IsNull)
            {
                // if both are null
                if(m_IsNull == true)
                {
                    return true;
                }

                if(m_IsNull == false)
                {

                    return m_IndexValue == other.m_IndexValue;
                }
            }

            return false;
        }

        template <typename T_Type>
        inline bool IndexTypeTemplate<T_Type>::operator<(const IndexTypeTemplate<T_Type>& other) const
        {
            if(m_IsNull || other.m_IsNull)
            {
                return false;
            }

            return m_IndexValue < other.m_IndexValue;
        }

        template <typename T_Type>
        inline bool IndexTypeTemplate<T_Type>::operator>(const IndexTypeTemplate<T_Type>& other) const
        {
            if(m_IsNull || other.m_IsNull)
            {
                return false;
            }

            return m_IndexValue > other.m_IndexValue;
        }

        template <typename T_Type>
        inline bool IndexTypeTemplate<T_Type>::operator!=(const IndexTypeTemplate<T_Type>& other) const
        {
            return !(this->operator==(other));
        }

        template <typename T_Type>
        inline IndexTypeTemplate<T_Type> IndexTypeTemplate<T_Type>::requestUnique()
        {
            IndexTypeTemplate<T_Type> result(s_Counter);
            IndexTypeTemplate<T_Type>::s_Counter++;
            return result;
        }

        template <typename T_Type>
        inline IndexTypeTemplate<T_Type> IndexTypeTemplate<T_Type>::requestNull()
        {
            IndexTypeTemplate<T_Type> result(0, true);
            return result;
        }

        template <typename T_Type>
        T_Type IndexTypeTemplate<T_Type>::s_Counter = 0;

        template <typename T_Type>
        struct IndexTypeHashTemplate
        {
            std::size_t operator()(const IndexTypeTemplate<T_Type>& index) const
            {
                return (size_t) std::hash<T_Type>{}(index.index());
            }
        };

        #ifdef LIBVIEW_TEST
        template<typename T_Type>
        inline void IndexTypeTemplate<T_Type>::reset()
        {
            s_Counter = 0;
        }
        #endif


        typedef IndexTypeTemplate<uint64_t> index_t;
        typedef IndexTypeHashTemplate<uint64_t> index_t_hash;

    } // namespace util

} // namespace JG

namespace LibView
{
    using namespace JG::util;
}