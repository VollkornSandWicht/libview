#pragma once

#include <QWidget>
#include "uic/ui_Qt_SideMenu.h"

class Qt_SideMenu : public QWidget 
{
	Q_OBJECT

public:
  Qt_SideMenu(QWidget *parent = Q_NULLPTR);
  ~Qt_SideMenu(); 

private:
  Ui::Qt_SideMenu ui;
};
