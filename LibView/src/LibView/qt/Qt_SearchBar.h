#pragma once

#include <QWidget>
#include "uic/ui_Qt_SearchBar.h"

class Qt_SearchBar : public QWidget 
{
	Q_OBJECT

public:
  Qt_SearchBar(QWidget *parent = Q_NULLPTR);
  ~Qt_SearchBar(); 

private:
  Ui::Qt_SearchBar ui;
};
