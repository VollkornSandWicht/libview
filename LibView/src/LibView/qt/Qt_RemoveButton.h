#pragma once

#include <QWidget>
#include "uic/ui_Qt_RemoveButton.h"

class Qt_RemoveButton : public QWidget 
{
	Q_OBJECT

public:
  Qt_RemoveButton(QWidget *parent = Q_NULLPTR);
  ~Qt_RemoveButton(); 

private:
  Ui::Qt_RemoveButton ui;
};
