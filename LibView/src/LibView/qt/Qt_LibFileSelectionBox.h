#pragma once

#include <QWidget>
#include "uic/ui_Qt_LibFileSelectionBox.h"

class Qt_LibFileSelectionBox : public QWidget 
{
	Q_OBJECT

public:
  Qt_LibFileSelectionBox(QWidget *parent = Q_NULLPTR);
  ~Qt_LibFileSelectionBox(); 

private:
  Ui::Qt_LibFileSelectionBox ui;
};
