#pragma once

#include <QWidget>
#include "uic/ui_Qt_LibFileVisibilityToggle.h"

class Qt_LibFileVisibilityToggle : public QWidget 
{
	Q_OBJECT

public:
  Qt_LibFileVisibilityToggle(QWidget *parent = Q_NULLPTR);
  ~Qt_LibFileVisibilityToggle(); 

private:
  Ui::Qt_LibFileVisibilityToggle ui;
};
