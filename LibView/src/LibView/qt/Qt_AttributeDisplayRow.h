#pragma once

#include <QWidget>
#include "uic/ui_Qt_AttributeDisplayRow.h"

class Qt_AttributeDisplayRow : public QWidget 
{
	Q_OBJECT

public:
  Qt_AttributeDisplayRow(QWidget *parent = Q_NULLPTR);
  ~Qt_AttributeDisplayRow(); 

private:
  Ui::Qt_AttributeDisplayRow ui;
};
