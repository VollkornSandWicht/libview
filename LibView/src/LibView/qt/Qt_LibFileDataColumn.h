#pragma once

#include <QWidget>
#include "uic/ui_Qt_LibFileDataColumn.h"

class Qt_LibFileDataColumn : public QWidget 
{
	Q_OBJECT

public:
  Qt_LibFileDataColumn(QWidget *parent = Q_NULLPTR);
  ~Qt_LibFileDataColumn(); 

private:
  Ui::Qt_LibFileDataColumn ui;
};
