#pragma once

#include <QWidget>
#include "uic/ui_Qt_ContentDisplay.h"

class Qt_ContentDisplay : public QWidget 
{
	Q_OBJECT

public:
  Qt_ContentDisplay(QWidget *parent = Q_NULLPTR);
  ~Qt_ContentDisplay(); 

private:
  Ui::Qt_ContentDisplay ui;
};
