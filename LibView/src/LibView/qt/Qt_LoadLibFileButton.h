#pragma once

#include <QWidget>
#include "uic/ui_Qt_LoadLibFileButton.h"

class Qt_LoadLibFileButton : public QWidget 
{
	Q_OBJECT

public:
  Qt_LoadLibFileButton(QWidget *parent = Q_NULLPTR);
  ~Qt_LoadLibFileButton(); 

private:
  Ui::Qt_LoadLibFileButton ui;
};
