#pragma once

#include <QWidget>
#include "uic/ui_Qt_PresetSelectionBox.h"

class Qt_PresetSelectionBox : public QWidget 
{
	Q_OBJECT

public:
  Qt_PresetSelectionBox(QWidget *parent = Q_NULLPTR);
  ~Qt_PresetSelectionBox(); 

private:
  Ui::Qt_PresetSelectionBox ui;
};
