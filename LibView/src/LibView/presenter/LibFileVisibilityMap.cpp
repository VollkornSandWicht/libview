/**
 * Project LibView
 * @author Leonard Wolf (2207778),  Mirko Sowa (2263258), Daniel Martins Fiebich (2219734), Christoph Böhrer (2070080),Justin Guth (2218548)
 * @version 1.0
 */


#include "LibFileVisibilityMap.h"

/**
 * LibFileVisibilityMap implementation
 * 
 * LibfileVisibilityMap, storing which Libfiles should currently be displayed/active.
 */
namespace LibView
{
    namespace presenter
    {
    }
}