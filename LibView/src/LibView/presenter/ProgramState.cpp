#include "ProgramState.h"

namespace LibView
{
    namespace presenter
    {
        ProgramState::ProgramState(
            const AttributeVisibilityMap& attributeVisibilityMap,
            const LibFileVisibilityMap& libFileVisibilityMap,          
            const model::ModelState& modelState
        ) :
            m_attributeVisibility(attributeVisibilityMap),
            m_libFileVisibility(libFileVisibilityMap),
            m_ModelState(modelState),
            m_LibFilesAdded(false),
            m_LibFilesRemoved(false)
        {}


        bool ProgramState::getLibFileVisibility(const index_t& index) const
        {
            return m_libFileVisibility.getVisibility(index);
        }

        bool ProgramState::getAttributeVisibility(const index_t& index) const
        {
            return m_attributeVisibility.getVisibility(index);
        }

        bool ProgramState::getAttributeVisibility(const std::string& path) const
        {
            return m_attributeVisibility.getVisibility(m_ModelState.getAttribute(path));
        }

        model::data_structure::LibertyFileData* ProgramState::getLibFile(const index_t& index) const
        {
            return m_ModelState.getLibFile(index);
        }

        std::vector<model::data_structure::LibertyFileData*> ProgramState::getLibFileList() const
        {
            return m_ModelState.getLibFileList();
        }

        std::string ProgramState::getAttribute(const index_t& index) const
        {
            return m_ModelState.getAttribute(index);
        }

        index_t ProgramState::getAttribute(const std::string& path) const
        {
            return m_ModelState.getAttribute(path);
        }

        std::vector<index_t> ProgramState::getLastAddedLibFiles() const
        {
            return m_ModelState.getLastAddedLibFiles();
        }

        std::vector<index_t> ProgramState::getLastRemovedLibFiles() const
        {
            return m_ModelState.getLastRemovedLibFiles();
        }

        std::vector<model::path_index_pair> ProgramState::getLastAddedAttributes() const
        {
            return m_ModelState.getLastAddedAttributes();
        }

        std::vector<model::path_index_pair> ProgramState::getLastRemovedAttributes() const
        {
            return m_ModelState.getLastRemovedAttributes();
        }

        const LIBVIEW_AVIS_MAP& ProgramState::getAttributeList() const
        {
            return m_attributeVisibility.getMap();
        }
    }
}