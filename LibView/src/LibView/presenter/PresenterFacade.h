#pragma once
/**
 * Project LibView
 * @author Leonard Wolf (2207778),  Mirko Sowa (2263258), Daniel Martins Fiebich (2219734), Christoph Böhrer (2070080), Justin Guth (2218548)
 * @version 1.0
 */


#ifndef LIBVIEW_TEST
#include "LibView/view/IView.h"
#else
#include "mock_IView.h"
#endif // !1



#include "LibView/model/ModelFacade_decl.h"
#include "LibView/model/data_structure/LibertyFileData.h"
#include <vector>
#include "LibView/model/ModelState.h"

namespace LibView
{
    namespace presenter
    {
        class PresenterFacade
        {
        public:


            /**
             * Constructor method for the PresenterFacade, takes a ModelFacade as input.
             *
             * @param modelFacade (ModelFacade) : Model implementation to be linked to the presenter
             */
            //PresenterFacade(model::ModelFacade& modelFacade);
            PresenterFacade(model::ModelFacade* modelFacade);


            ~PresenterFacade();

            /**
             * Receives signal that a new loading of a file was requested.
             */
            void requestFileLoad();

            /**
             * Removes a given LibFile from the prob
             * Receives the signal that a LibFile should be removed from the programm.
             *
             * @param index (index_t) : Index of the LibFile to be removed
             */
            void requestRemovalofLibFile(const index_t& index);

            /**
             * Sorts the loaded LibFiles according to the value in a group, in ascending or descending order.
             *
             * @param groupToSortBy (index_t) : Sorting parameter is group index (therefore value stored inside)
             * @param ascending (bool) : True for ascending order, False for descending order
             */
            void requestSortingOfLibFile(const index_t& groupToSortBy, bool ascending);



            /**
             * Changes the visibility of a given LibFile.
             *
             * @param index (index_t) : Index of the LibFile
             * @param visible (bool) : True for visible, False for invisible
             */
            void requestLibertyFileVisibilityToggle(const index_t& index, bool visible);

            /**
             * Changes the visibility of a given attribute.
             * Changes all children visibility aswell.
             *
             * @param index (index_t) : Index of the attribute
             * @param visible (bool) : True for visible, False for invisible
             */
            void requestAttributeVisibilityToggle(const index_t& index, bool visible);

            /**
             * Displays only attributes matching with the given search term.
             *
             * @param searchTerm (std::string) : Term by which to filter attributes
             */
            void requestSearchList(std::string searchTerm);

            /**
             * Changes the currently selected graph type.
             *
             * @param graphType (std::string) : New graph type //TODO define, change type
             */
            void requestGraphTypeChange(std::string graphType); //TODO change type


            /**
             * Gets the children LibFile indicies of a given LibFile index.
             *
             * @param index (index_t) : Index of LibFile to get children from
             *
             * @return std::vector<index_t> : All children indicies of the given LibFile
             */
            std::vector<index_t> requestChildrenIndices(const index_t& index) const;

            /**
             * Updates presenter when model state has changed:
             * 1. Updates attributes visibility
             * 2. Updates LibFile visibility
             * 3. Updates Presets
             * 4. Updates Preset data
             * 5. Pushes ProgramState to IView
             *
             * @param modelState (model::ModelState) : New state of the model with changes
             */
            void notifyOfModelStateUpdate(model::ModelState& modelState);


            /**
            * Initialise the Presenter facade with a pointer to the view
            */
            void init(view::IView* view);

            /**
             * Request the display of the file contents associated with a given lib file
             */
            void requestContentDisplay(const index_t& index);

            /**
             * Request a selection of a preset
             */
            void requestPresetSelection(const index_t& index);

            /**
             * Request the display of attribute containing the query or matching its regex
             */
            void requestSearch(const std::string& reg);

            /**
             * Request a list of all existing presets
             */
            const std::vector<model::APreset*>& getPresetList() const;

        private:
            
            //model::ModelFacade& m_ModelFacade; // Obsolete
            model::ModelFacade* m_pModelFacade;
            view::IView* m_pView;
            const ProgramState m_programState;
            AttributeVisibilityMap m_attributeVisibility;
            LibFileVisibilityMap m_libFileVisibility;
            



            /**
             * Updates the entries in the AttributeVisibilityMap according to the model state.
             *
             * @param modelState (model::ModelState) : New state of the model with changes
             */
            void updateAttributeVisibilityMap(model::ModelState& modelState);

            /**
             * Updates the entries in the LibfileVisibilityMap according to the model state.
             *
             * @param modelState (model::ModelState) : New state of the model with changes
             */
            void updateLibFileVisibilityMap(model::ModelState& modelState);

            /**
             * Updates the related attributes for a Preset according to the model state.
             *
             * @param modelState (model::ModelState) : New state of the model with changes
             */
            void updatePresets(model::ModelState& modelState);

            /**
             * Calculates every PresetData for newly added/removed LibFile.
             * Adds/Removes its entry in the according map in the PresetDataStorage.
             *
             * @param modelState (model::ModelState) : New state of the model with changes
             */
            void updatePresetData(model::ModelState& modelState);

            /**
             * Gets the children in the Model and enables/disables their visibilty aswell
             *
             * @param index (index_t) : Index of the parent attribute
             * @param visible (bool) : True for visible, False for invisible
             */
            void setAttributeChildrenVisibilities(const index_t& index, bool visible);


            /**
             * Makes all Attributes invisible
             */
            void setAllAttributesInvisible();
        };
    }
}