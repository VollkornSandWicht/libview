#pragma once

#include <vector>
#include <string>

#include "LibView/presenter/PresenterFacade_decl.h"
#include "LibView/presenter/ProgramState.h"


namespace LibView
{
	namespace model
	{
		class APreset;
	}

	namespace view
	{
		class IView
		{
		public:

			inline IView() {};

			virtual std::vector<std::string> promptLibFileSelection() = 0;

			virtual bool promptConfirmationMessage() = 0;

			/**
			 * receives the current program state to get the needed information to draw the gui
			 * @param programState
			 */
			virtual void receiveProgramState(const presenter::ProgramState& programState) = 0;

			/**
			 * receives th eorder in which the view hast to draw the liberty files according to the sorting parameter
			 *
			 * @param sortedLibFileList
			 */
			virtual void receiveSortedLibFileList(std::vector <int64_t> sortedLibFileList) = 0;

			/**
			 * received the filtered attribute list after a search term selection
			 *
			 * @param filteredAttributeList
			 */
			virtual void receiveFilteredAttributeList(std::vector<int64_t> filteredAttributeList) = 0;

			virtual void notifyPresenterOfFileLoadRequest() = 0;

			virtual presenter::PresenterFacade* getPresenterPointer() = 0;

			virtual void onContentDisplayFileSelection(const index_t& index) = 0;

			virtual void setContentDisplayContent(const std::string& text) = 0;

			virtual void libFileRemovalRequested(const index_t& index) = 0;

			virtual void requestLibFileVisibility(const index_t& index, bool visibility) = 0;

			virtual void setLibFileVisibility(const index_t& index, bool visibility) = 0;

			virtual void requestAttributeVisibilityToggle(const index_t& index, bool visibility) = 0;

			virtual void updateAttributeVisibilities(const presenter::ProgramState& state) = 0;

			virtual const std::vector<model::APreset*>& getPresetList() const = 0;

			virtual void requestPresetLoad(const index_t& index) = 0;

			virtual void onSearchRequested(const std::string& reg) = 0;

		private:
			presenter::PresenterFacade* m_Presenter = nullptr;
		};
	}
}