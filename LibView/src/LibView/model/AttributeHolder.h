#pragma once

/**
 * Project LibView
 * @author Leonard Wolf (2207778),  Mirko Sowa (2263258), Daniel Martins Fiebich (2219734), Christoph Böhrer (2070080), Justin Guth (2218548)
 * @version 1.0
 */

#include <regex>

#include "AHolder.h"
#include "AttributeMap.h"

#include "APreset.h"

namespace LibView
{
    namespace model
    {

        /**
        * With AHolder<index_t (index of Group), std::string (path of Group)>
        */
        class AttributeHolder : public AHolder
        {
        public:

            /**
             * removes an given Attribute from the Application.
             * decrements the multiplicity in the Attribute Map, and if the counter reaches 0 removes it from the map. add the attribute to lastRemovedList.
             *
             *
             * @param attributeHierarchy is the hierarchy of the attribute that is to be removed.
             */
            void removeAttribute(std::string attributeHierarchy);

            /**
             * while loading/creating LibFileobjects add each new created Attribute to last added
             *
             * adds an given Attributeto the Application.
             * increments the multiplicity in the Attribute Map or creates new entry if it hasn't occured yet.
             * @param attributeHierachy 
             //TODO change parameter to attribute pointer -- really?
             */
            void addAttribute(const std::string& attributeHierarchy);

            
            /**
             * @param regexString
             * @return std::vector<index: index_t>
             */
            std::vector<index_t> getMatchingAttributeList(std::string regexString);

            /**
             * returns list of children of attribute
             */
            std::vector<index_t> getChildren(const index_t& index);


            /**
             * returns the path of an attribute with a given index 
             */
            std::string getAttribute(const index_t& index) const;
            
            
            /**
             * returns the index of an attribute with a given path
             */
            index_t getAttribute(const std::string& index) const;

        private: 

            AttributeMap m_AttributeMap;


        };


    }
}