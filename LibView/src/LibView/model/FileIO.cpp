#include "FileIO.h"
#include "LibView/core/Macros.h"
#include "LibView/exception/FileCloseException.h"
#include "LibView/exception/FileOpenException.h"

#include <filesystem>
#include <fstream>

namespace LibView
{
    namespace model
    {
        std::string FileIO::readFile(const std::string& path)
        {
            return readFile(path.c_str());
        }

        std::string FileIO::readFile(const char* path)
        {
            if (!std::filesystem::is_regular_file(path))
                throw exception::FileOpenException(LIBVIEW_LOCATION);

            std::ifstream in(path);
            std::string content(std::istreambuf_iterator<char>(in), {});

            return content;
        }
    }
}
