#pragma once

#include "LibView/core/LibFileIndex.h"

#include <string>
#include <vector>

#include <regex>

namespace LibView
{
    namespace model
    {
        class APreset
        {
        private:


            index_t m_Index;
            std::string m_Name;
            std::vector<std::string> m_Paths;

        public:


            inline const std::vector<std::string>& getPaths() const { return m_Paths; }


            /**
             * returns whether the path is part of the preset
             */
            inline bool containsPath(const std::string& path) const {

                for(std::string reg : m_Paths)
                {

                    if(std::regex_match(path, std::regex(reg)))
                    {
                        return true;
                    }
                }
                
                
                return false;
            }
            
            /**
             * returns the name of the preset
             */
            inline const std::string& getName() const { return m_Name; }
            
            
            /**
             * returns the index of the preset
             */
            inline const index_t& index() const { return m_Index; }

        protected:

            /**
             * adds a regex expression accepting a path
             */
            inline void addPath(const std::string& path) { m_Paths.push_back(path); }
            
            /**
             * default constructor. initialises name of preset
             */
            inline APreset(const std::string& name) :
                m_Index(index_t::requestUnique()),
                m_Paths(std::vector<std::string>()),
                m_Name(name)
            {};
        };
    }
}