#pragma once

#include "ModelFacade_decl.h"

namespace LibView
{
    namespace model 
    {

        /**
         * makes model facade easily accessible
         */
        class ModelFacadeKnower
        {
        private:
            
            ModelFacade* m_Model;

        public:

            /**
             * return pointer to model facade
             */
            inline ModelFacade* getModel() { return m_Model; };

            ModelFacadeKnower(ModelFacade* pModel);

        };
    }
}