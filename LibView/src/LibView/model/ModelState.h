#pragma once
/**
 * Project LibView
 * @author Leonard Wolf (2207778),  Mirko Sowa (2263258), Daniel Martins Fiebich (2219734), Christoph Böhrer (2070080), Justin Guth (2218548)
 * @version 1.0
 */

#include <stdint.h>
#include "LibView/model/data_structure/LibertyFileData.h"
#include "LibView/model/LibFileHolder.h"
#include "LibView/model/AttributeHolder.h"
#include "LibView/model/AttributeMap.h"

namespace LibView
{
    namespace model
    {

        /**
         * provides references to the data within the model
         */
        class ModelState
        {
            friend class ModelFacade;


        public:



            ModelState();

            /**
             * Returns the data of a LibFile related to a given index.
             *
             * @param index (index_t) : Index of LibFile to get data from
             *
             * @return model::data_structure::LibertyFileData* : Constant reference to the matching LibertyFileData
             */
            model::data_structure::LibertyFileData* getLibFile(const index_t& index) const;


            /**
             * Returns the data of a LibertyFileGroup related to a given index.
             *
             * @param index (index_t) : Index of LibertyFileGroup to get data from
             *
             * @return model::data_structure::LibertyFileGroup* : Constant reference to the matching LibertyFileGroup
             */
            std::string getAttribute(const index_t& index) const;
            index_t getAttribute(const std::string& index) const;




            /**
             * Returns the map of all attributes.
             *
             * @return AttributeMap : Map of attributes
             */
            const AttributeMap* getAttributes() const;

            /**
             * Returns the last added attributes.
             *
             * @return std::vector<index_t> : List of attributes indicies
             */
            std::vector<model::path_index_pair> getLastAddedAttributes() const;

            /**
             * Returns the last removed attributes.
             *
             * @return std::vector<index_t> : List of attributes indicies
             */
            std::vector<model::path_index_pair> getLastRemovedAttributes() const;

            /**
             * Returns the last added LibFiles.
             *
             * @return std::vector<index_t> : List of LibFiles indicies
             */
            std::vector<index_t> getLastAddedLibFiles() const;

            /**
             * Returns the last removed LibFiles.
             *
             * @return std::vector<index_t> : List of LibFiles indicies
             */
            std::vector<index_t> getLastRemovedLibFiles() const;


            std::vector<model::data_structure::LibertyFileData*> getLibFileList() const;

            /**
             * Updates  last removed/last added lists for LibFiles
             *
             * @param addedLibFiles (std::vector<index_t>) : last added LibFiles
             * @param removedLibFiles (std::vector<index_t>) : last removed LibFiles
             */
            void updateLastChangedLibFiles(
                std::vector<index_t> addedLibFiles,
                std::vector<index_t> removedLibFiles);
            
            /**
             * Updates last removed/last added lists for attributes
             *
             * @param addedAttributes (std::vector<index_t>) : last added attributes
             * @param removedAttributes (std::vector<index_t>) : last removed attributes
             */
            //void updateLastChangedAttributes(
            //    std::vector<index_t> addedAttributes,
            //    std::vector<index_t> removedAttributes);

        private:

            std::vector<index_t> m_LastAddedLibFiles;
            std::vector<index_t> m_LastRemovedLibFiles;
            

            model::data_structure::LibertyFileData* m_LibFiles = nullptr;
            model::AttributeMap* m_AttributeMap = nullptr;
            model::LibFileHolder* m_LibFileHolder = nullptr;
            //model::AttributeHolder* m_AttributeHolder = nullptr;
        };

    }
}
