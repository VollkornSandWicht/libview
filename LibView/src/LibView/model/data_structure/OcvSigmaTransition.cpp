/**
 * Project LibView
 * @author Leonard Wolf (2207778),  Mirko Sowa (2263258), Daniel Martins Fiebich (2219734), Christoph Böhrer (2070080), Justin Guth (2218548)
 * @version 1.0
 */

 /**
  * OcvSigmaTransition implementation
  *
  * class for ocv_sigma_transitions groups that need the additional information of the sigma_type and the getter and setter for sigma_type
  */

#include "OcvSigmaTransition.h"

namespace LibView
{
    namespace model
    {
        namespace data_structure
        {
            OcvSigmaTransition::OcvSigmaTransition(ModelFacade* pModel)
                : TransitionTable(pModel),
                // Never use variables as initialiser values
                sigma_type("")
            {}

            OcvSigmaTransition::~OcvSigmaTransition()
            {}

            /**
             * @return std::string
             */
            std::string OcvSigmaTransition::getSigma_type()
            {
                return sigma_type;

            }

            /**
             * @param value
             */
            void OcvSigmaTransition::setSigma_type(std::string value)
            {
                orderedAttributes.push_back("sigma_type");
                orderedAttributeValues.push_back(value);
                sigma_type = value;
            }
        }
    }
}