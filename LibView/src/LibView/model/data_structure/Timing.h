#pragma once

/**
 * Project LibView
 * @author Leonard Wolf (2207778),  Mirko Sowa (2263258), Daniel Martins Fiebich (2219734), Christoph Böhrer (2070080), Justin Guth (2218548)
 * @version 1.0
 */

#include "LibertyFileGroup.h"

namespace LibView
{
    namespace model
    {
        namespace data_structure
        {
            class Timing : public LibertyFileGroup {
            public:
                Timing(ModelFacade* pModel);
                ~Timing();

                std::string getTiming_sense();

                /**
                 * @param value
                 */
                void setTiming_sense(std::string value);

                std::string getTiming_type();

                /**
                 * @param value
                 */
                void setTiming_type(std::string value);

                std::string getRelated_pin();

                /**
                 * @param value
                 */
                void setRelated_pin(std::string value);

            private:
                std::string related_pin;

                std::string timing_sense;

                std::string timing_type;
            };
        }
    }
}