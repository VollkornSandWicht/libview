/**
 * Project LibView
 * @author Leonard Wolf (2207778),  Mirko Sowa (2263258), Daniel Martins Fiebich (2219734), Christoph Böhrer (2070080), Justin Guth (2218548)
 * @version 1.0
 */

 /**
  * LibertyFileData implementation
  *
  * class that contains meta data about a liberty file lie the name, file path and the unique  index of the file  assigned by the programm. As well as getter operations for all of these.
  */

#include "LibertyFileData.h"

namespace LibView
{
    namespace model
    {
        namespace data_structure
        {
            /**
            * constructor for LibertyFileData with a filepath as reference
            */
            LibertyFileData::LibertyFileData(const std::string& filePath)
                : 
                filePath(filePath),
                m_libraryGroup(nullptr),
                libFileIndex(index_t::requestUnique()) //replace with reference
            {
                displayName = assigneDisplayName();
            }
            /*/
            LibertyFileData::LibertyFileData() : 
                // Never use variables as initialiser values
                filePath(""),
                m_libraryGroup(nullptr),
                libFileIndex(index_t::requestUnique()), //replace with reference
                // Never use variables as initialiser values

                displayName("")
            {}
            */

            /**
            * LibertyFileData destructur freeing m_LibraryGroup and therfore all classes holding information of the Liberty File 
            */
            LibertyFileData::~LibertyFileData()
            {
                delete m_libraryGroup;
            }

            /**
             * @return displayName as std::string
             */
            std::string LibertyFileData::getDisplayName() const
            {
                return displayName;
            }

            /**
             * @return m_libraryGroup as LibertyFileGroup*
             */
            LibertyFileGroup* LibertyFileData::getLibraryGroup() const
            {
                return m_libraryGroup;
            }

            /**
            * @param LibertyFileGroup* to set m_libraryGroup with
            */
            void LibertyFileData::setLibraryGroup(LibertyFileGroup* libraryGroup)
            {
                m_libraryGroup = libraryGroup;
            }

            /**
             * @return libFileIndex as index_t
             */ 
            index_t LibertyFileData::getLibFileIndex() const
            {
                return libFileIndex;
            }

            /**
             * @return filePath as std::string
             */
            std::string LibertyFileData::getFilePath()
            {
                return filePath;
            }

            //void LibertyFileData::setFilePath(std::string path)
            //{
            //    filePath = path;
            //    displayName = assigneDisplayName();
            //}

            /**
             * @return the name of a Liberty File according to it's filepath
             */
            std::string LibertyFileData::assigneDisplayName()
            {
                size_t from = filePath.find_last_of(forwardSlash) + 1;
                size_t to = filePath.find_last_of(dot);
                return filePath.substr(from, to - from);;
            }
        }
    }
}