/**
 * Project LibView
 * @author Leonard Wolf (2207778),  Mirko Sowa (2263258), Daniel Martins Fiebich (2219734), Christoph Böhrer (2070080), Justin Guth (2218548)
 * @version 1.0
 */

 /**
  * Pin implementation
  *
  * class that contains attributes and the according getter and setter for the pin group
  */

#include "Pin.h"

namespace LibView
{
    namespace model
    {
        namespace data_structure
        {
            Pin::Pin(ModelFacade* pModel)
                : LibertyFileGroup(pModel),
                // Never use variables as initialiser values
                isSetCapacitance(false),
                capacitance(0),
                direction(""),
                driver_waveform_rise(""),
                driver_waveform_fall(""),
                input_voltage(""),
                function(""),
                isSetMax_capacitance(false),
                max_capacitance(0),
                isSetMin_capacitance(false),
                min_capacitance(0),
                output_voltage(""),
                clock(""),
                min_pulse_width_low(0),
                min_pulse_width_high(0),
                nextstate_type(""),
                three_state("")
            {}

            Pin::~Pin()
            {}

            /**
             * @return double
             */
            double Pin::getCapacitance()
            {
                return capacitance;
            }

            /**
             * @param value
             */
            void Pin::setCapacitance(double value)
            {
                orderedAttributes.push_back("capacitance");
                orderedAttributeValues.push_back(std::to_string(value));
                isSetCapacitance = true;
                capacitance = value;
            }

            /**
             * @return std::string
             */
            std::string Pin::getDirection()
            {
                return direction;
            }

            /**
             * @param value
             */
            void Pin::setDirection(std::string value)
            {
                orderedAttributes.push_back("direction");
                orderedAttributeValues.push_back(value);
                direction = value;
            }

            /**
             * @return std::string
             */
            std::string Pin::getDriver_waveform_rise()
            {
                return driver_waveform_rise;

            }

            /**
             * @param value
             */
            void Pin::setDriver_waveform_rise(std::string value)
            {
                orderedAttributes.push_back("driver_waveform_rise");
                orderedAttributeValues.push_back(value);
                driver_waveform_rise = value;
            }

            /**
             * @return std::string
             */
            std::string Pin::getDriver_waveform_fall()
            {
                return driver_waveform_fall;
            }

            /**
             * @param value
             */
            void Pin::setDriver_waveform_fall(std::string value)
            {
                orderedAttributes.push_back("driver_waveform_fall");
                orderedAttributeValues.push_back(value);
                driver_waveform_fall = value;
            }

            /**
             * @return std::string
             */
            std::string Pin::getInput_voltage()
            {
                return input_voltage;

            }

            /**
             * @param value
             */
            void Pin::setInput_voltage(std::string value)
            {
                orderedAttributes.push_back("input_voltage");
                orderedAttributeValues.push_back(value);
                input_voltage = value;
            }

            /**
             * @return double
             */
            double Pin::getMax_capacitance()
            {
                return max_capacitance;
            }

            /**
             * @param value
             */
            void Pin::setMax_capacitance(double value)
            {
                orderedAttributes.push_back("max_capacitance");
                orderedAttributeValues.push_back(std::to_string(value));
                isSetMax_capacitance = true;
                max_capacitance = value;
            }

            /**
             * @return double
             */
            double Pin::getMin_capacitance()
            {
                return min_capacitance;
            }

            /**
             * @param value
             */
            void Pin::setMin_capacitance(double value)
            {
                orderedAttributes.push_back("min_capacitance");
                orderedAttributeValues.push_back(std::to_string(value));
                isSetMin_capacitance = true;
                min_capacitance = value;
            }

            /**
             * @return std::string
             */
            std::string Pin::getOutput_voltage()
            {
                return output_voltage;
            }

            /**
             * @param value
             */
            void Pin::setOutput_voltage(std::string value)
            {
                orderedAttributes.push_back("output_voltage");
                orderedAttributeValues.push_back(value);
                output_voltage = value;
            }

            /**
             * @return std::string
             */
            std::string Pin::getFunction()
            {
                return function;

            }

            /**
             * @param value
             */
            void Pin::setFunction(std::string value)
            {
                orderedAttributes.push_back("function");
                orderedAttributeValues.push_back(value);
                function = value;
            }
            std::string Pin::getClock()
            {
                return clock;
            }
            void Pin::setClock(std::string value)
            {
                orderedAttributes.push_back("clock");
                orderedAttributeValues.push_back(value);
                clock = value;
            }
            std::string Pin::getThree_state()
            {
                return three_state;
            }
            void Pin::setThree_state(std::string value)
            {
                orderedAttributes.push_back("three_state");
                orderedAttributeValues.push_back(value);
                three_state = value;
            }
            std::string Pin::getNextstate_type()
            {
                return nextstate_type;
            }
            void Pin::setNextstate_type(std::string value)
            {
                orderedAttributes.push_back("nextstate_type");
                orderedAttributeValues.push_back(value);
                nextstate_type = value;
            }
            double Pin::getMin_pulse_width_low()
            {
                return min_pulse_width_low;
            }
            void Pin::setMin_pulse_width_low(double value)
            {
                orderedAttributes.push_back("min_pulse_width_low");
                orderedAttributeValues.push_back(std::to_string(value));
                min_pulse_width_low = value;
            }
            double Pin::getMin_pulse_width_high()
            {
                return min_pulse_width_high;
            }
            void Pin::setMin_pulse_width_high(double value)
            {
                orderedAttributes.push_back("min_pulse_width_high");
                orderedAttributeValues.push_back(std::to_string(value));
                min_pulse_width_high = value;
            }
        }
    }
}