/**
 * Project LibView
 * @author Leonard Wolf (2207778),  Mirko Sowa (2263258), Daniel Martins Fiebich (2219734), Christoph Böhrer (2070080), Justin Guth (2218548)
 * @version 1.0
 */




/**
 * LeakagePower implementation
 * 
 * class that contains attributes and the according getter and setter for the leakage_power group
 */
#include "LeakagePower.h"

namespace LibView
{
    namespace model
    {
        namespace data_structure
        {
            LeakagePower::LeakagePower(ModelFacade* pModel)
                : LibertyFileGroup(pModel),
                // Never use variables as initialiser values

                when(""),
                isSetValue(false),
                value(0)
            {}

            LeakagePower::~LeakagePower()
            {}

            /**
             * @return double
             */
            double LeakagePower::getValue() {
                return value;
            }

            /**
             * @param value
             */
            void LeakagePower::setValue(double value) {
                orderedAttributes.push_back("value");
                orderedAttributeValues.push_back(std::to_string(value));
                isSetValue = true;
                this->value = value;
            }

            /**
             * @return std::string
             */
            std::string LeakagePower::getWhen() {
                return when;
            }

            /**
             * @param value
             */
            void LeakagePower::setWhen(std::string value) {

                // TODO : Check if found 
                // TODO: Check what to do exactly
                orderedAttributes.push_back("when");
                orderedAttributeValues.push_back(value);
                
                std::size_t found = path.find_last_of("(");

                if(found != std::string::npos) // CHECK IF FOUND
                {
                    std::string insert = "|";
                    insert += value + "|";
                    path.insert(found + 1, insert);
                    when = value;
                }

                /*
                */

                // OLD CODE
                // path.insert(path.find_last_of("("), "|" + value + "|");
                // when = value;
            }
        }
    }
}