#pragma once

/**
 * Project LibView
 * @author Leonard Wolf (2207778),  Mirko Sowa (2263258), Daniel Martins Fiebich (2219734), Christoph Böhrer (2070080), Justin Guth (2218548)
 * @version 1.0
 */

 //#include "Library_decl.h"
#include <string>
#include<map>
#include<vector>
#include "LibView/model/LibViewPair.h"
#include "LibView/model/ModelFacadeKnower.h"

namespace LibView
{
    namespace model
    {
        namespace data_structure
        {

            class LibertyFileGroup : public ModelFacadeKnower
            {
            public:

                LibertyFileGroup(ModelFacade* pModel);

                ~LibertyFileGroup();

                std::string getName() const;

                void setName(const std::string& name);

                //std::string getGroupName() const;

                //void setGroupName(const std::string& name);

                std::string getPath() const;

                /**
                 * @param value
                 */
                void setPath(const std::string&);

                //std::string toString();

                //int64_t getGroupIndex();

                /**
                 * @param libFileIndex
                 * @param attributeIndex

                double getDelta(int64_t libFileIndex, int64_t attributeIndex);

                /**
                 * @param delta
                 * @param libFileIndex
                 * @param attributeIndex

                void setDelta(double delta, int64_t libFileIndex, int64_t attributeIndex);
                */
                LibertyFileGroup* getParent() const;

                void setParent(LibertyFileGroup*);

                //std::vector<LibertyFileGroup*> getChildren(std::string path) const;
                std::vector<LibertyFileGroup*> getChildren() const;

                LibertyFileGroup* getChild(std::string path) const;

                void setChild(LibertyFileGroup*);

                std::string getUndifinedAttribute(const std::string& name) const;

                void setUndifinedAttribute(const std::string&, const std::string& value);

                std::string getDisplayName() const;

                std::vector<std::string> getOrderedAttributes() const;

                std::vector<std::string> getOrderedAttributeValues() const;

            protected:

                /**
                * the hierarchical path of the group in the file
                */
                std::string path;

                /**
                * to safe attributes not defined by the data structure
                * first string is the name of the undifined attribute second its values
                */
                std::map<std::string, std::string> undifinedAttributes;

                /**
                * unique index for every group assigned by the program
                */
                //int64_t groupIndex;

                //static int64_t indexCounter;

                /**
                 * aberrations to the same attributes from another liberty file
                 * first int ist the LibertyFileIndex second the AttributeIndex and the double ist the delta for the attribute
                 */
                //std::map<int64_t, std::map<int64_t, double>> deltas;

                std::vector<LibertyFileGroup*> m_Children;

                LibertyFileGroup* m_parent;

                size_t getLastOf(const char token) const;

                //size_t getFirstOf(const char token) const;

                const int openingBrace = 40;

                const int closingBrace = 41;

                const int forwardSlash = 47;

                // ! Never use members in initialiser lists
                const std::string empty = "";

                const int verticalBar = 124;

                //std::string undindinedAttributesToString() const;

                const std::string colon = " : ";

                const std::string endOfLine = " ;\n";

                //std::string cleanName() const;

                const std::string emptyGroupName = "()";

                const std::string groupOpening = " {\n";

                const std::string groupClosing = "}\n";

                std::vector<std::string> orderedAttributes;

                std::vector<std::string> orderedAttributeValues;
            };
        }
    }
}