#pragma once

#include "LibView/model/data_structure/LibertyFileGroup.h"
#include "LibView/model/data_structure/DynamicDimensionArray.h"

namespace LibView
{
    namespace model
    {
        namespace data_structure
        {
            class Latch : public LibertyFileGroup
            {
            public:
                Latch(ModelFacade* pModel);
                ~Latch();

                std::string getEnable();

                void setEnable(std::string name);

                std::string getData_in();

                void setData_in(std::string name);

            private:

                std::string enable;
                std::string data_in;
            };
        }
    }
}

