/**
 * Project LibView
 * @author Leonard Wolf (2207778),  Mirko Sowa (2263258), Daniel Martins Fiebich (2219734), Christoph Böhrer (2070080), Justin Guth (2218548)
 * @version 1.0
 */

 /**
  * Library implementation
  *
  * class that contains attributes and the according getter and setter for the library group
  */

#include "Library.h"
#include "CapacitiveLoadUnitAttribute.h"

namespace LibView
{
    namespace model
    {
        namespace data_structure
        {
            Library::Library(ModelFacade* pModel)
                : LibertyFileGroup(pModel),
                // Never use variables as initialiser values REEEEEEEEEEEEEE

                delay_model(""),
                isSetLibrary_features(false),
                time_unit(""),
                voltage_unit(""),
                current_unit(""),
                pulling_resistance_unit(""),
                isSetCapacitive_load_unit(false),
                leakage_power_unit(""),
                isSetInput_threshold_pct_fall(false),
                input_threshold_pct_fall(0),
                isSetInput_threshold_pct_rise(false),
                input_threshold_pct_rise(0),
                isSetOutput_threshold_pct_fall(false),
                output_threshold_pct_fall(0),
                isSetOutput_threshold_pct_rise(false),
                output_threshold_pct_rise(0),
                isSetSlew_lower_threshold_pct_fall(false),
                slew_lower_threshold_pct_fall(0),
                isSetSlew_lower_threshold_pct_rise(false),
                slew_lower_threshold_pct_rise(0),
                isSetSlew_upper_threshold_pct_fall(false),
                slew_upper_threshold_pct_fall(0),
                isSetSlew_upper_threshold_pct_rise(false),
                slew_upper_threshold_pct_rise(0),
                isSetSlew_derate_from_library(false),
                slew_derate_from_library(0),
                isSetNom_process(false),
                nom_process(0),
                isSetNom_temperature(false),
                nom_temperature(0),
                isSetNom_voltage(false),
                nom_voltage(0),
                isSetDefault_cell_leakage_power(false),
                default_cell_leakage_power(0),
                isSetDefault_fanout_load(false),
                default_fanout_load(0),
                isSetDefault_inout_pin_cap(false),
                default_inout_pin_cap(0),
                isSetDefault_input_pin_cap(false),
                default_input_pin_cap(0),
                isSetDefault_output_pin_cap(false),
                default_output_pin_cap(0),
                isSetDefault_leakage_power_density(false),
                default_leakage_power_density(0)
            {}

            Library::~Library()
            {}

            /**
             * @return std::string
             */
            std::string Library::getDelay_model()
            {
                return delay_model;
            }

            /**
             * @param value
             */
            void Library::setDelay_model(std::string value)
            {
                orderedAttributes.push_back("delay_model");
                orderedAttributeValues.push_back(value);
                delay_model = value;
            }

            /**
             * @return std::vector<std::string>
             */
            std::vector<std::string> Library::getLibrary_features()
            {

                return library_features;
            }

            /**
             * @param value
             */
            void Library::setLibrary_features(std::vector <std::string> value)
            {
                isSetLibrary_features = true;
                orderedAttributes.push_back("library_features");
                std::string result = "";
                std::vector <std::string>::iterator iterator = value.begin();
                for(; iterator != value.end(); iterator++)
                {
                    result.append(*iterator);
                    if(iterator + 1 != value.end())
                        result.append(", ");
                }
                orderedAttributeValues.push_back(result);
                library_features = value;
            }

            /**
             * @return std::string
             */
            std::string Library::getTime_unit()
            {
                return time_unit;
            }

            /**
             * @param value
             */
            void Library::setTime_unit(std::string value)
            {
                orderedAttributes.push_back("time_unit");
                orderedAttributeValues.push_back(value);
                time_unit = value;
            }

            /**
             * @return std::string
             */
            std::string Library::getVoltage_unit()
            {
                return voltage_unit;
            }

            /**
             * @param value
             */
            void Library::setVoltage_unit(std::string value)
            {
                orderedAttributes.push_back("voltage_unit");
                orderedAttributeValues.push_back(value);
                voltage_unit = value;
            }

            std::string Library::getCurrent_unit()
            {
                return current_unit;
            }

            /**
             * @return std::string
             */
            void Library::setCurrent_unit(std::string value)
            {
                orderedAttributes.push_back("current_unit");
                orderedAttributeValues.push_back(value);
                current_unit = value;
            }

            std::string Library::getPulling_resistance_unit()
            {
                return pulling_resistance_unit;
            }

            /**
             * @param value
             */
            void Library::setPulling_resistance_unit(std::string value)
            {
                orderedAttributes.push_back("pulling_resistance_unit");
                orderedAttributeValues.push_back(value);
                pulling_resistance_unit = value;
            }

            /**
             * @return &CapacativeLoadUnitAttribute
             */
            CapacitiveLoadUnitAttribute* Library::getCapacitive_load_unit()
            {
                return &capacitive_load_unit;
            }

            /**
             * @param doublePart
             * @param enumPart
             */
            void Library::setCapacitive_load_unit(double doublePart, std::string stringPart)
            {
                isSetLibrary_features = true;
                capacitive_load_unit.setDoublePart(doublePart);
                capacitive_load_unit.setStringPart(stringPart);
                orderedAttributes.push_back("capacitive_load_unit");
                std::string result = "";
                result.append(std::to_string(capacitive_load_unit.getDoublePart()) + ", " + capacitive_load_unit.getStringPart());
                orderedAttributeValues.push_back(result);
            }

            /**
             * @return std::string
             */
            std::string Library::getLeakage_power_unit()
            {
                return leakage_power_unit;
            }

            /**
             * @param value
             */
            void Library::setLeakage_power_unit(std::string value)
            {
                orderedAttributes.push_back("leakage_power_unit");
                orderedAttributeValues.push_back(value);
                leakage_power_unit = value;
            }

            /**
             * @return double
             */
            double Library::getInput_threshold_pct_fall()
            {
                return input_threshold_pct_fall;
            }

            /**
             * @param value
             */
            void Library::setInput_threshold_pct_fall(double value)
            {
                orderedAttributes.push_back("input_threshold_pct_fall");
                orderedAttributeValues.push_back(std::to_string(value));
                isSetInput_threshold_pct_fall = true;
                input_threshold_pct_fall = value;
            }

            /**
             * @return double
             */
            double Library::getInput_threshold_pct_rise()
            {
                return input_threshold_pct_rise;
            }

            /**
             * @param value
             */
            void Library::setInput_threshold_pct_rise(double value)
            {
                orderedAttributes.push_back("input_threshold_pct_rise");
                orderedAttributeValues.push_back(std::to_string(value));
                isSetInput_threshold_pct_rise = true;
                input_threshold_pct_rise = value;
            }

            /**
             * @return double
             */
            double Library::getOutput_threshold_pct_fall()
            {
                return output_threshold_pct_fall;
            }

            /**
             * @param value
             */
            void Library::setOutput_threshold_pct_fall(double value)
            {
                orderedAttributes.push_back("output_threshold_pct_fall");
                orderedAttributeValues.push_back(std::to_string(value));
                isSetOutput_threshold_pct_fall = true;
                output_threshold_pct_fall = value;
            }

            /**
             * @return double
             */
            double Library::getOutput_threshold_pct_rise()
            {
                return output_threshold_pct_rise;
            }

            /**
             * @param value
             */
            void Library::setOutput_threshold_pct_rise(double value)
            {
                orderedAttributes.push_back("output_threshold_pct_rise");
                orderedAttributeValues.push_back(std::to_string(value));
                isSetOutput_threshold_pct_rise = true;
                output_threshold_pct_rise = value;
            }

            /**
             * @return double
             */
            double Library::getSlew_lower_threshold_pct_fall()
            {
                return slew_lower_threshold_pct_fall;
            }

            /**
             * @param value
             */
            void Library::setSlew_lower_threshold_pct_fall(double value)
            {
                orderedAttributes.push_back("slew_lower_threshold_pct_fall");
                orderedAttributeValues.push_back(std::to_string(value));
                isSetSlew_lower_threshold_pct_fall = true;
                slew_lower_threshold_pct_fall = value;
            }

            /**
             * @return double
             */
            double Library::getSlew_lower_threshold_pct_rise()
            {
                return slew_lower_threshold_pct_rise;
            }

            /**
             * @param value
             */
            void Library::setSlew_lower_threshold_pct_rise(double value)
            {
                isSetSlew_lower_threshold_pct_rise = true;
                orderedAttributes.push_back("slew_lower_threshold_pct_rise");
                orderedAttributeValues.push_back(std::to_string(value));
                slew_lower_threshold_pct_rise = value;
            }

            /**
             * @return double
             */
            double Library::getSlew_upper_threshold_pct_fall()
            {
                return slew_upper_threshold_pct_fall;
            }

            /**
             * @param value
             */
            void Library::setSlew_upper_threshold_pct_fall(double value)
            {
                orderedAttributes.push_back("slew_upper_threshold_pct_fall");
                orderedAttributeValues.push_back(std::to_string(value));
                isSetSlew_upper_threshold_pct_fall = true;
                slew_upper_threshold_pct_fall = value;
            }

            /**
             * @return double
             */
            double Library::getSlew_upper_threshold_pct_rise()
            {
                return slew_upper_threshold_pct_rise;
            }

            /**
             * @param value
             */
            void Library::setSlew_upper_threshold_pct_rise(double value)
            {
                orderedAttributes.push_back("slew_upper_threshold_pct_rise");
                orderedAttributeValues.push_back(std::to_string(value));
                isSetSlew_upper_threshold_pct_rise = true;
                slew_upper_threshold_pct_rise = value;
            }

            /**
             * @return double
             */
            double Library::getSlew_derate_from_library()
            {
                return slew_derate_from_library;
            }

            /**
             * @param value
             */
            void Library::setSlew_derate_from_library(double value)
            {
                orderedAttributes.push_back("slew_derate_from_library");
                orderedAttributeValues.push_back(std::to_string(value));
                isSetSlew_derate_from_library = true;
                slew_derate_from_library = value;
            }

            /**
             * @return double
             */
            double Library::getNom_process()
            {
                return nom_process;
            }

            /**
             * @param value
             */
            void Library::setNom_process(double value)
            {
                orderedAttributes.push_back("nom_process");
                orderedAttributeValues.push_back(std::to_string(value));
                isSetNom_process = true;
                nom_process = value;
            }

            /**
             * @return double
             */
            double Library::getNom_temperature()
            {
                return nom_temperature;
            }

            /**
             * @param value
             */
            void Library::setNom_temperature(double value)
            {
                orderedAttributes.push_back("nom_temperature");
                orderedAttributeValues.push_back(std::to_string(value));
                isSetNom_temperature = true;
                nom_temperature = value;
            }

            /**
             * @return double
             */
            double Library::getNom_voltage()
            {
                return nom_voltage;
            }

            /**
             * @param value
             */
            void Library::setNom_voltage(double value)
            {
                orderedAttributes.push_back("nom_voltage");
                orderedAttributeValues.push_back(std::to_string(value));
                isSetNom_voltage = true;
                nom_voltage = value;
            }

            /**
             * @return double
             */
            double Library::getDefault_cell_leakage_power()
            {
                return default_cell_leakage_power;
            }

            /**
             * @param value
             */
            void Library::setDefault_cell_leakage_power(double value)
            {
                orderedAttributes.push_back("default_cell_leakage_power");
                orderedAttributeValues.push_back(std::to_string(value));
                isSetDefault_cell_leakage_power = true;
                default_cell_leakage_power = value;
            }

            /**
             * @return double
             */
            double Library::getDefault_fanout_load()
            {
                return default_fanout_load;
            }

            /**
             * @param value
             */
            void Library::setDefault_fanout_load(double value)
            {
                orderedAttributes.push_back("default_fanout_load");
                orderedAttributeValues.push_back(std::to_string(value));
                isSetDefault_fanout_load = true;
                default_fanout_load = value;
            }

            /**
             * @return double
             */
            double Library::getDefault_inout_pin_cap()
            {
                return default_inout_pin_cap;
            }

            /**
             * @param value
             */
            void Library::setDefault_inout_pin_cap(double value)
            {
                orderedAttributes.push_back("default_inout_pin_cap");
                orderedAttributeValues.push_back(std::to_string(value));
                isSetDefault_inout_pin_cap = true;
                default_inout_pin_cap = value;
            }

            /**
             * @return double
             */
            double Library::getDefault_input_pin_cap()
            {
                return default_input_pin_cap;
            }

            /**
             * @param value
             */
            void Library::setDefault_input_pin_cap(double value)
            {
                orderedAttributes.push_back("default_input_pin_cap");
                orderedAttributeValues.push_back(std::to_string(value));
                isSetDefault_input_pin_cap = true;
                default_input_pin_cap = value;
            }

            /**
             * @return double
             */
            double Library::getDefault_leakage_power_density()
            {
                return default_leakage_power_density;
            }

            /**
            * @param value
            */
            void Library::setDefault_leakage_power_density(double value)
            {
                orderedAttributes.push_back("default_leakage_power_density");
                orderedAttributeValues.push_back(std::to_string(value));
                isSetDefault_leakage_power_density = true;
                default_leakage_power_density = value;
            }

            double Library::getDefault_output_pin_cap()
            {
                return default_output_pin_cap;
            }

            void Library::setDefault_output_pin_cap(double value)
            {
                orderedAttributes.push_back("default_output_pin_cap");
                orderedAttributeValues.push_back(std::to_string(value));
                isSetDefault_output_pin_cap = true;
                default_output_pin_cap = value;
            }
        }
    }
}