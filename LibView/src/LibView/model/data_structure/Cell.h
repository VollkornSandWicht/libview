#pragma once

/**
 * Project LibView
 * @author Leonard Wolf (2207778),  Mirko Sowa (2263258), Daniel Martins Fiebich (2219734), Christoph Böhrer (2070080), Justin Guth (2218548)
 * @version 1.0
 */

#include "LibertyFileGroup.h"

namespace LibView
{
    namespace model
    {
        namespace data_structure
        {
            class Cell : public LibertyFileGroup {
            public:
                Cell(ModelFacade* pModel);

                ~Cell();

                double getCell_leakage_power();

                /**
                 * @param value
                 */
                void setCell_leakage_power(double value);

            private:
                bool isSetCell_leakage_power;

                double cell_leakage_power;
            };
        }
    }
}