#pragma once

/**
 * Project LibView
 * @author Leonard Wolf (2207778),  Mirko Sowa (2263258), Daniel Martins Fiebich (2219734), Christoph Böhrer (2070080), Justin Guth (2218548)
 * @version 1.0
 */

#include <string>
#include <sstream>

namespace LibView
{
    namespace model
    {
        namespace data_structure
        {
            class CapacitiveLoadUnitAttribute {
            public:

                CapacitiveLoadUnitAttribute();
                ~CapacitiveLoadUnitAttribute();

                std::string getStringPart();

                /**
                 * @param value
                 */
                void setStringPart(std::string value);

                double getDoublePart();

                /**
                 * @param value
                 */
                void setDoublePart(double value);

                std::string toString();

            private:
                double doublePart;

                std::string stringPart;

                std::string empty = "";
            };
        }
    }
}