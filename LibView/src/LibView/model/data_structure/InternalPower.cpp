/**
 * Project LibView
 * @author Leonard Wolf (2207778),  Mirko Sowa (2263258), Daniel Martins Fiebich (2219734), Christoph Böhrer (2070080), Justin Guth (2218548)
 * @version 1.0
 */

 /**
  * InternalPower implementation
  *
  * class that can be extended with attributes and the according getter and setter for the internal_power  group
  */

#include "InternalPower.h"

namespace LibView
{
    namespace model
    {
        namespace data_structure
        {
            InternalPower::InternalPower(ModelFacade* pModel)
                : 
                LibertyFileGroup(pModel),
                // Never use variables as initialiser values
                related_pin("")
            {}

            InternalPower::~InternalPower()
            {}

            std::string InternalPower::getRelated_pin()
            {
                return related_pin;
            }

            void InternalPower::setRelated_pin(std::string value)
            {
                std::size_t found = path.find_last_of("(");
                orderedAttributes.push_back("related_pin");
                orderedAttributeValues.push_back(value);
                if(found != std::string::npos) // CHECK IF FOUND
                {
                    std::string insert = "|";
                    insert += value + "|";
                    path.insert(found + 1, insert);
                    related_pin = value;

                }
                // TODO: Check what to do exactly
                // OLD CODE
                // path.insert(path.find_last_of("("), "|" + value + "|");
                // related_pin = value;

            }
        }
    }
}

