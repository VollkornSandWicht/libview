/**
 * Project LibView
 * @author Leonard Wolf (2207778),  Mirko Sowa (2263258), Daniel Martins Fiebich (2219734), Christoph Böhrer (2070080), Justin Guth (2218548)
 * @version 1.0
 */

 /**
  * PowerLutTemplate implementation
  *
  * class that contains attributes and the according getter and setter for the power_lut_template group except for the index attributes which it inherits from the IndexTemplate class
  */

#include "PowerLutTemplate.h"

namespace LibView
{
    namespace model
    {
        namespace data_structure
        {
            PowerLutTemplate::PowerLutTemplate(ModelFacade* pModel)
                : IndexTemplate(pModel),
                // Never use variables as initialiser values
                variable_1(""),
                variable_2(""),
                variable_3("")
            {}

            PowerLutTemplate::~PowerLutTemplate()
            {}

            /**
             * @return std::string
             */
            std::string PowerLutTemplate::getVariable_1()
            {
                return variable_1;
            }

            /**
             * @param value
             */
            void PowerLutTemplate::setVariable_1(std::string value)
            {
                orderedAttributes.push_back("variable_1");
                orderedAttributeValues.push_back(value);
                variable_1 = value;
            }

            /**
             * @return std::string
             */
            std::string PowerLutTemplate::getVariable_2()
            {
                return variable_2;
            }

            /**
             * @param value
             */
            void PowerLutTemplate::setVariable_2(std::string value)
            {
                orderedAttributes.push_back("variable_2");
                orderedAttributeValues.push_back(value);
                variable_2 = value;
            }

            /**
             * @return std::string
             */
            std::string PowerLutTemplate::getVariable_3()
            {
                return variable_3;

            }

            /**
             * @param value
             */
            void PowerLutTemplate::setVariable_3(std::string value)
            {
                orderedAttributes.push_back("variable_3");
                orderedAttributeValues.push_back(value);
                variable_3 = value;
            }
        }
    }
}