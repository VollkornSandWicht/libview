#pragma once

/**
 * Project LibView
 * @author Leonard Wolf (2207778),  Mirko Sowa (2263258), Daniel Martins Fiebich (2219734), Christoph Böhrer (2070080), Justin Guth (2218548)
 * @version 1.0
 */

#include "LibertyFileGroup.h"
#include "LibView/model/data_structure/DynamicDimensionArray.h"

namespace LibView
{
    namespace model
    {
        namespace data_structure
        {
            class NormalizedDriverWaveform : public LibertyFileGroup {
            public:
                NormalizedDriverWaveform(ModelFacade* pModel);
                ~NormalizedDriverWaveform();

                std::string getDriver_waveform_name();

                /**
                 * @param value
                 */
                void setDriver_waveform_name(std::string value);

                DynamicDimensionArray* getIndex_1();

                /**
                 * @param value
                 */
                void setIndex_1(DynamicDimensionArray* value);

                DynamicDimensionArray* getIndex_2();

                /**
                 * @param value
                 */
                void setIndex_2(DynamicDimensionArray* value);

                DynamicDimensionArray* getValues();

                /**
                 * @param value
                 */
                void setValues(DynamicDimensionArray* value);

            private:
                std::string driver_waveform_name;

                DynamicDimensionArray* index_1;

                DynamicDimensionArray* index_2;

                DynamicDimensionArray* values;
            };
        }
    }
}