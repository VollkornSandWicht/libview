/**
 * Project LibView
 * @author Leonard Wolf (2207778),  Mirko Sowa (2263258), Daniel Martins Fiebich (2219734), Christoph Böhrer (2070080), Justin Guth (2218548)
 * @version 1.0
 */

 /**
  * IndexTemplate implementation
  *
  * abstract class for every group that have the index attributes, also contains the getter and setter  for said attributes
  */


#include "IndexTemplate.h"

namespace LibView
{
    namespace model
    {
        namespace data_structure
        {
            IndexTemplate::IndexTemplate(ModelFacade* pModel)
                : 
                LibertyFileGroup(pModel),
                index_1(nullptr),
                index_2(nullptr),
                index_3(nullptr)
            {}

            IndexTemplate::~IndexTemplate()
            {
                delete index_1;
                delete index_2;
                delete index_3;
            }

            /**
             * @return DynamicDimensionArray
             */
            DynamicDimensionArray* IndexTemplate::getIndex_1()
            {
                return index_1;
            }

            /**
             * @param value
             */
            void IndexTemplate::setIndex_1(DynamicDimensionArray* value)
            {
                orderedAttributes.push_back("index_1");
                orderedAttributeValues.push_back(value->toString());
                index_1 = value;
            }

            /**
             * @return DynamicDimensionArray
             */
            DynamicDimensionArray* IndexTemplate::getIndex_2()
            {
                return index_2;
            }

            /**
             * @param value
             */
            void IndexTemplate::setIndex_2(DynamicDimensionArray* value)
            {
                orderedAttributes.push_back("index_2");
                orderedAttributeValues.push_back(value->toString());
                index_2 = value;
            }

            /**
             * @return DynamicDimensionArray
             */
            DynamicDimensionArray* IndexTemplate::getIndex_3()
            {
                return index_3;
            }

            /**
             * @param value
             */
            void IndexTemplate::setIndex_3(DynamicDimensionArray* value)
            {
                orderedAttributes.push_back("index_3");
                orderedAttributeValues.push_back(value->toString());
                index_3 = value;
            }

            size_t IndexTemplate::getDimensionOfIndex_1()
            {
                return index_1->getSize();
            }

            size_t IndexTemplate::getDimensionOfIndex_2()
            {
                return index_2->getSize();
            }

            size_t IndexTemplate::getDimensionOfIndex_3()
            {
                return index_3->getSize();
            }
        }
    }
}