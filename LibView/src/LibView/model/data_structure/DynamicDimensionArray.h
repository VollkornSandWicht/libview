#pragma once

#include <vector>
#include <string>

namespace LibView
{
    namespace model
    {
        class DynamicDimensionArray
        {
        public:

            enum class Type
            {
                Scalar = 0,
                Array
            };

        private:

            union
            {
                double* m_Double = nullptr;
                std::vector<DynamicDimensionArray*>* m_Array;
            };

            size_t m_Dimension;
            size_t m_Size;
            Type m_Type;

            std::string toStringHelper() const;

        public:
            
            DynamicDimensionArray();
            DynamicDimensionArray(size_t dimension);
            DynamicDimensionArray(const DynamicDimensionArray& other);
            ~DynamicDimensionArray();

            DynamicDimensionArray& reserve(size_t size);
            DynamicDimensionArray& operator<<(double value);

            void setScalar(double value);

            inline size_t getDimension() { return m_Dimension; }

            double get() const;
            DynamicDimensionArray& operator[](size_t index);
            DynamicDimensionArray& operator=(const DynamicDimensionArray& other);
            DynamicDimensionArray& getPosition(size_t index) const;

            std::string toString() const;

            size_t getSize();

        };

    }
}
