/**
 * Project LibView
 * @author Leonard Wolf (2207778),  Mirko Sowa (2263258), Daniel Martins Fiebich (2219734), Christoph Böhrer (2070080), Justin Guth (2218548)
 * @version 1.0
 */

 /**
  * LibertyFileGroup implementation
  *
  * abstract group that contains all attributes every group in a liberty file has
  */

#include "LibertyFileGroup.h"
#include "LibView/model/ModelFacade.h"

namespace LibView
{
    namespace model
    {
        namespace data_structure
        {
            /**
            * constructor for LibertyFileGroup
            * @param ModelFacade* pointer to refrence ModelFacade
            */
            LibertyFileGroup::LibertyFileGroup(ModelFacade* pModel)
                :
                ModelFacadeKnower(pModel),
                path(""),
                m_parent(nullptr),
                m_Children(std::vector<LibertyFileGroup*>())
            {}


            /**
            * LibertyFileGroup destructur deleting all children of the group
            */
            LibertyFileGroup::~LibertyFileGroup()
            {
                getModel()->removeAttribute(getPath());

                for(LibertyFileGroup* child : m_Children)
                    delete child;
            }

            /**
             * @return name of the group as std::string
             */
            std::string LibertyFileGroup::getName() const
            {
                size_t from = getLastOf(openingBrace) + 1;
                size_t to = getLastOf(closingBrace);
                return path.substr(from, to - from);
            }


            /**
            *@param std::string to set the name 
            */
            void LibertyFileGroup::setName(const std::string& name)
            {
                size_t from = getLastOf(openingBrace) + 1;
                size_t to = getLastOf(closingBrace);
                path.replace(from, to - from, name);
            }

            /**
             * @return group name as std::string
             */
            //std::string LibertyFileGroup::getGroupName() const
            //{
            //    size_t from;
            //    size_t to;
            //    if(path == empty)
            //        return empty; //maybe exception
            //    else if(getLastOf(forwardSlash) != std::string::npos)
            //    {
            //        from = getLastOf(forwardSlash) + 1;
            //        to = getLastOf(openingBrace);
            //        return path.substr(from, to - from);
            //    }
            //    else
            //    {
            //        from = path.front();
            //        to = getFirstOf(openingBrace) - 1;
            //        return path.substr(from, to - from);
            //    }
            //}

            /**
            void LibertyFileGroup::setGroupName(const std::string& name)
            {
                size_t from;
                size_t to;
                if(path == empty)
                    path = name + emptyGroupName;
                else if(getLastOf(forwardSlash) != std::string::npos)
                {
                    from = getLastOf(forwardSlash) + 1;
                    to = getLastOf(openingBrace);
                    path.replace(from, to - from, name);
                }
                else
                {
                    from = path.front();
                    to = getFirstOf(openingBrace) - 1;
                    path.replace(from, to - from, name);
                }
            }
            */

            /**
             * @return hierarchical path of the group as std::string
             */
            std::string LibertyFileGroup::getPath() const
            {
                return path;
            }

            /**
             * @param std::string to set as path
             */
            void LibertyFileGroup::setPath(const std::string& path)
            {
                this->path = path;
            }

            /**
            * @return group in form of a std::string
            */
            //std::string LibertyFileGroup::toString() //WHITESPACES MISSING
            //{
            //    std::string output;
            //    output = cleanName() + undindinedAttributesToString() + groupClosing;
            //    return output;
            //}

            /**
             * @return int64_t

            int64_t LibertyFileGroup::getGroupIndex()
            {
                return groupIndex;
            }
            */

            /**
            /**
             * @param libFileIndex
             * @param attributeIndex
             * @return double

            double LibertyFileGroup::getDelta(int64_t libFileIndex, int64_t attributeIndex)
            {
                return 0.0;
            }

            /**
             * @param delta
             * @param libFileIndex
             * @param attributeIndex

            void LibertyFileGroup::setDelta(double delta, int64_t libFileIndex, int64_t attributeIndex)
            {

            }
            */

            /**
             * @return parent of the group as LibertyFileGroup*
             */
            LibertyFileGroup* LibertyFileGroup::getParent() const
            {
                return m_parent;
            }



            /**
             * @param LibertyFileGroup* parent group to this group
             */
            void LibertyFileGroup::setParent(LibertyFileGroup* parent)
            {
                m_parent = parent;
            }

            /**
            std::vector<LibertyFileGroup*> LibertyFileGroup::getChildren(std::string path) const// Warum Path
            {
                return m_Children;
            }
            */

            /**
             * @return std::vector<LibertyFileGroup*> the vector with all children of the group
             */
            std::vector<LibertyFileGroup*> LibertyFileGroup::getChildren() const
            {
                return m_Children;
            }


            /**
             * @return LibertyFileGroup* a specific child
             * @param std::string name of the child searched for
             */
            LibertyFileGroup* LibertyFileGroup::getChild(std::string name) const
            {
                for(LibertyFileGroup* child : m_Children)
                {
                    if(child->getName() == name)
                        return child;
                }
                return nullptr;
            }



            /**
            * @param LibertyFileGroup* child group of this group
            */
            void LibertyFileGroup::setChild(LibertyFileGroup* child)
            {
                m_Children.push_back(child);
            }



            /**
             * @param std::string name of the attribute 
             * @return std::string value of the attribute
             */
            std::string LibertyFileGroup::getUndifinedAttribute(const std::string& name) const
            {
                return undifinedAttributes.at(name);
            }




            /**
            * @param std::string name of the attribute
            * @param std::string value of the attribute
            */
            void LibertyFileGroup::setUndifinedAttribute(const std::string& name, const std::string& value)
            {
                

                orderedAttributes.push_back(name);
                std::string cleanedValue = value;
                size_t found = value.find_first_of(':');
                if(found != std::string::npos)
                    cleanedValue = cleanedValue.replace(0, 3, "");
                
                size_t temp = cleanedValue.size();
                cleanedValue = cleanedValue.replace(temp - 2, 2, "");
                orderedAttributeValues.push_back(cleanedValue);
                undifinedAttributes[name] = value;
            }

            /**
            * @return std::string groupName(name) as in Liberty Files without added information that makes the path unique
            */
            std::string LibertyFileGroup::getDisplayName() const
            {
                size_t from = getLastOf(forwardSlash);
                size_t to = getLastOf(closingBrace);
                std::string displayName = (path.substr(from + 1, to - from));
                if(displayName.find_first_of(verticalBar) != std::string::npos)
                {
                    from = displayName.find_first_of(verticalBar);
                    to = displayName.find_last_of(verticalBar);
                    displayName = displayName.replace(from, to - from + 1, "");
                }
                return displayName;
            }


            /**
            * @return std::vector<std::string> name of attributes in oreder they were added to the group
            */
            std::vector<std::string> LibertyFileGroup::getOrderedAttributes() const
            {
                return orderedAttributes;
            }


            /**
            * @return std::vector<std::string> values of attributes in oreder they were added to the group
            */
            std::vector<std::string> LibertyFileGroup::getOrderedAttributeValues() const
            {
                return orderedAttributeValues;
            }


            /**
            * @param const char token ASCII character searched for
            * @return size_t position of last found token given as a parameter in the path
            */
            size_t LibertyFileGroup::getLastOf(const char token) const
            {
                return path.find_last_of(token);
            }


            /**
            * @param const char token ASCII character searched for
            * @return size_t position of first found token given as a parameter in the path
            */
            //size_t LibertyFileGroup::getFirstOf(const char token) const
            //{
            //    return path.find_first_of(token);
            //}


            /**
            * Helper methode to translate the undifined attributes to std::string 
            * @return std::string of undifined attributes
            */
            //std::string LibertyFileGroup::undindinedAttributesToString() const 
            //{
            //    std::string output;
            //    std::map<std::string, std::string>::const_iterator iterator = undifinedAttributes.begin();
            //    for(; iterator != undifinedAttributes.end(); ++iterator)
            //    {
            //        output.append(iterator->first + iterator->second + endOfLine);
            //    }
            //    return output;
            //}


            //std::string LibertyFileGroup::cleanName() const
            //{
            //    std::string output;
            //    std::string cleanedName;
            //    size_t from;
            //    size_t to;
            //    if(path.find_first_of(verticalBar) != std::string::npos)
            //    {
            //        if(path.find_first_of(forwardSlash) != std::string::npos)
            //        {
            //            from = getLastOf(forwardSlash) + 1;
            //            to = getLastOf(closingBrace);
            //            cleanedName = path.substr(from, to - from);
            //        }
            //        from = cleanedName.find_first_of(verticalBar);
            //        to = cleanedName.find_last_of(verticalBar);
            //        cleanedName = cleanedName.erase(from, to - from);
            //    }
            //    else
            //    {
            //        if(path.find_first_of(forwardSlash) != std::string::npos)
            //        {
            //            from = getLastOf(forwardSlash) + 1;
            //            to = getLastOf(closingBrace);
            //            cleanedName = path.substr(from, to - from);
            //        }
            //        else
            //            cleanedName = path;
            //    }
            //    output.append(cleanedName + groupOpening);
            //    return output;
            //}
        }
    }
}