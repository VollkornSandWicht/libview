/**
 * Project LibView
 * @author Leonard Wolf (2207778),  Mirko Sowa (2263258), Daniel Martins Fiebich (2219734), Christoph Böhrer (2070080), Justin Guth (2218548)
 * @version 1.0
 */

 /**
  * OperatingConditions implementation
  *
  * class that contains attributes and the according getter and setter for the operating_conditions group
  */

#include "OperatingConditions.h"

namespace LibView
{
    namespace model
    {
        namespace data_structure
        {
            OperatingConditions::OperatingConditions(ModelFacade* pModel)
                : LibertyFileGroup(pModel),
                isSetProcess(false),
                process(0),
                isSetTemperature(false),
                temperature(0),
                isSetVoltage(false),
                voltage(0)
            {
            }

            OperatingConditions::~OperatingConditions()
            {}

            /**
             * @return double
             */
            double OperatingConditions::getProcess()
            {
                return process;
            }

            /**
             * @param value
             */
            void OperatingConditions::setProcess(double value)
            {
                orderedAttributes.push_back("process");
                orderedAttributeValues.push_back(std::to_string(value));
                process = value;
            }

            /**
             * @return double
             */
            double OperatingConditions::getTemperature()
            {
                return temperature;
            }

            /**
             * @param value
             */
            void OperatingConditions::setTemperature(double value)
            {
                orderedAttributes.push_back("temperature");
                orderedAttributeValues.push_back(std::to_string(value));
                temperature = value;
            }

            /**
             * @return double
             */
            double OperatingConditions::getVoltage()
            {
                return voltage;
            }

            /**
             * @param value
             */
            void OperatingConditions::setVoltage(double value)
            {
                orderedAttributes.push_back("voltage");
                orderedAttributeValues.push_back(std::to_string(value));
                voltage = value;
            }
        }
    }
}