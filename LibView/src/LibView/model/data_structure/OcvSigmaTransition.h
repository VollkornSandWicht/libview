#pragma once

/**
 * Project LibView
 * @author Leonard Wolf (2207778),  Mirko Sowa (2263258), Daniel Martins Fiebich (2219734), Christoph Böhrer (2070080), Justin Guth (2218548)
 * @version 1.0
 */

#include "TransitionTable.h"

namespace LibView
{
    namespace model
    {
        namespace data_structure
        {
            class OcvSigmaTransition : public TransitionTable
            {
            public:
                OcvSigmaTransition(ModelFacade* pModel);
                ~OcvSigmaTransition();

                std::string getSigma_type();

                /**
                 * @param value
                 */
                void setSigma_type(std::string value);

            private:
                std::string sigma_type;
            };
        }
    }
}