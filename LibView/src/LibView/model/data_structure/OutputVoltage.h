#pragma once

/**
 * Project LibView
 * @author Leonard Wolf (2207778),  Mirko Sowa (2263258), Daniel Martins Fiebich (2219734), Christoph Böhrer (2070080), Justin Guth (2218548)
 * @version 1.0
 */

#include "LibertyFileGroup.h"
#include "VoltageExpression.h"
#include <LibView/model/data_structure/Library.h>

namespace LibView
{
    namespace model
    {
        namespace data_structure
        {

            class OutputVoltage : public LibertyFileGroup {
            public:

                OutputVoltage(ModelFacade* pModel);
                ~OutputVoltage();

                VoltageExpression* getVol();

                /**
                 * @param value
                 */
                void setVol(std::string value);

                void setParent(LibertyFileGroup* parent);

                VoltageExpression* getVoh();

                /**
                 * @param value
                 */
                void setVoh(std::string value);

                VoltageExpression* getVomin();

                /**
                 * @param value
                 */
                void setVomin(std::string value);

                VoltageExpression* getVomax();

                /**
                 * @param value
                 */
                void setVomax(std::string value);
            private:
                double getScale(std::string);

                bool isSetVol;

                VoltageExpression vol;

                bool isSetVoh;

                VoltageExpression voh;

                bool isSetVomin;

                VoltageExpression vomin;

                bool isSetVomax;

                VoltageExpression vomax;

                Library* library_parent;
            };
        }
    }
}