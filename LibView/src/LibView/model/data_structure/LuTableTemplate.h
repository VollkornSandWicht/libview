#pragma once

/**
 * Project LibView
 * @author Leonard Wolf (2207778),  Mirko Sowa (2263258), Daniel Martins Fiebich (2219734), Christoph Böhrer (2070080), Justin Guth (2218548)
 * @version 1.0
 */

#include "IndexTemplate.h"

namespace LibView
{
    namespace model
    {
        namespace data_structure
        {
            class LuTableTemplate : public IndexTemplate {
            public:
                LuTableTemplate(ModelFacade* pModel);
                ~LuTableTemplate();

                std::string getVariable_1();

                /**
                 * @param value
                 */
                void setVariable_1(std::string value);

                std::string getVariable_2();

                /**
                 * @param value
                 */
                void setVariable_2(std::string value);

                std::string getVariable_3();

                /**
                 * @param value
                 */
                void setVariable_3(std::string value);

            private:

                std::string variable_1;

                std::string variable_2;

                std::string variable_3;
            };
        }
    }
}