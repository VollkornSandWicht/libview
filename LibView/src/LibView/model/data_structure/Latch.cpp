/**
 * Project LibView
 * @author Leonard Wolf (2207778),  Mirko Sowa (2263258), Daniel Martins Fiebich (2219734), Christoph B�hrer (2070080), Justin Guth (2218548)
 * @version 1.0
 */

 /**
  * Latch implementation
  *
  * class for groups that only have one table  with according getters and setters
  */



#include "Latch.h"


namespace LibView
{
    namespace model
    {
        namespace data_structure
        {
            Latch::Latch(ModelFacade* pModel)
                :
                LibertyFileGroup(pModel),
                enable(""),
                data_in("")
            {}

            Latch::~Latch()
            {}

            std::string Latch::getEnable()
            {
                return enable;
            }

            void Latch::setEnable(std::string value)
            {
                orderedAttributes.push_back("enable");
                orderedAttributeValues.push_back(value);
                enable = value;
            }

            std::string Latch::getData_in()
            {
                return data_in;
            }

            void Latch::setData_in(std::string value)
            {
                orderedAttributes.push_back("data_in");
                orderedAttributeValues.push_back(value);
                data_in = value;
            }
        }
    }
}