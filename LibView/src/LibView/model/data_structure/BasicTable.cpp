/**
 * Project LibView
 * @author Leonard Wolf (2207778),  Mirko Sowa (2263258), Daniel Martins Fiebich (2219734), Christoph Böhrer (2070080), Justin Guth (2218548)
 * @version 1.0
 */

 /**
  * BasicTable implementation
  *
  * class for groups that only have one table  with according getters and setters
  */

#include "BasicTable.h"

namespace LibView
{
    namespace model
    {
        namespace data_structure
        {
            /**
            * constructor for BasicTable with ModelFacade* reference
            */
            BasicTable::BasicTable(ModelFacade* pModel)
                : IndexTemplate(pModel),
                values(nullptr)
            {}

            /**
            * BasicTable destructur deleting values 
            */
            BasicTable::~BasicTable()
            {
                delete values;
            }

            /**
             * @return values as a DynamicDimensionArray*
             */
            DynamicDimensionArray* BasicTable::getValues()
            {
                return values;
            }

            /**
             * @param value set values to given param
             */
            void BasicTable::setValues(DynamicDimensionArray* value)
            {
                orderedAttributes.push_back("values");
                orderedAttributeValues.push_back(value->toString());
                values = value;
            }
        }
    }
}