/**
 * Project LibView
 * @author Leonard Wolf (2207778),  Mirko Sowa (2263258), Daniel Martins Fiebich (2219734), Christoph Böhrer (2070080), Justin Guth (2218548)
 * @version 1.0
 */


#include "TransitionTable.h"

 /**
  * TransitionTable implementation
  *
  * class for groups that can have an additional table with according getters and setters
  */

namespace LibView
{
    namespace model
    {
        namespace data_structure
        {
            TransitionTable::TransitionTable(ModelFacade* pModel)
                : BasicTable(pModel),
                intermediate_values(nullptr)
            {}

            TransitionTable::~TransitionTable()
            {
                delete intermediate_values;
            }

            /**
             * @return float*
             */
            DynamicDimensionArray* TransitionTable::getIntermediate_values()
            {
                return intermediate_values;
            }

            /**
             * @param value
             */
            void TransitionTable::setIntermediate_values(DynamicDimensionArray* value)
            {
                orderedAttributes.push_back("intermediate_values");
                orderedAttributeValues.push_back(value->toString());
                intermediate_values = value;
            }
        }
    }
}