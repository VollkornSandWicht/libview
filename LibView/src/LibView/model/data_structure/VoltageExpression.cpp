/**
 * Project LibView
 * @author Leonard Wolf (2207778),  Mirko Sowa (2263258), Daniel Martins Fiebich (2219734), Christoph Böhrer (2070080), Justin Guth (2218548)
 * @version 1.0
 */

 /**
  * VoltageExpression implementation
  *
  * class for attributes that can be a combination of strings and floats
  */

#include "VoltageExpression.h"

namespace LibView
{
    namespace model
    {
        namespace data_structure
        {
            VoltageExpression::VoltageExpression()
                : 
                // Never use variables as initialiser values
                string(""),
                doubleValue(0)
            {}

            VoltageExpression::~VoltageExpression()
            {}

            VoltageExpression::VoltageExpression(std::string value, double scale)
            {
                string = value;
                calculateDoubleValue(scale);
            }

            /**
             * @return std::string
             */
            std::string VoltageExpression::getAsString()
            {
                return string;
            }

            /**
             * @param value
             */
            void VoltageExpression::setVoltageExpression(std::string value, double scale)
            {
                string = value;
                calculateDoubleValue(scale);
            }

            /**
             * @return double
             */
            double VoltageExpression::getAsDouble()
            {
                return doubleValue;
            }

            void VoltageExpression::calculateDoubleValue(double scale)
            {
                double defaultVDDandVCC = 5;
                size_t found = string.find("+");
                if(found != std::string::npos)
                {
                    std::string subStrFront = string.substr(0, found - 1);
                    std::string subStrBack = string.substr(found + 1, string.size());
                    if(subStrFront.find("VDD") != std::string::npos || subStrFront.find("VCC") != std::string::npos)
                        doubleValue = (defaultVDDandVCC / scale) + atof(subStrBack.c_str());
                    else if(subStrBack.find("VDD") != std::string::npos || subStrBack.find("VCC") != std::string::npos)
                        doubleValue = (defaultVDDandVCC / scale) + atof(subStrFront.c_str());
                    else if(subStrFront.find("VSS") != std::string::npos)
                        doubleValue = atof(subStrBack.c_str());
                    else
                        doubleValue = atof(subStrFront.c_str());
                }
                else if(string.find("V") != std::string::npos)
                {
                    if(string.find("VSS") != std::string::npos)
                        doubleValue = 0;
                    else
                        doubleValue = defaultVDDandVCC / scale;
                }
                else
                    doubleValue = atof(string.c_str());
            }
        }
    }
}