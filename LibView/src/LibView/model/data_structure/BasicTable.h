#pragma once

/**
 * Project LibView
 * @author Leonard Wolf (2207778),  Mirko Sowa (2263258), Daniel Martins Fiebich (2219734), Christoph Böhrer (2070080), Justin Guth (2218548)
 * @version 1.0
 */

#include "IndexTemplate.h"
#include "LibView/model/data_structure/DynamicDimensionArray.h"

namespace LibView
{
    namespace model
    {
        namespace data_structure
        {
            class BasicTable : public IndexTemplate {
            public:
                BasicTable(ModelFacade* pModel);
                ~BasicTable();
                DynamicDimensionArray* getValues();

                /**
                 * @param value
                 */
                void setValues(DynamicDimensionArray* value);

            private:
                DynamicDimensionArray* values;
            };
        }
    }
}