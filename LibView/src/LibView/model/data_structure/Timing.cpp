/**
 * Project LibView
 * @author Leonard Wolf (2207778),  Mirko Sowa (2263258), Daniel Martins Fiebich (2219734), Christoph Böhrer (2070080), Justin Guth (2218548)
 * @version 1.0
 */

 /**
  * Timing implementation
  */

#include "Timing.h"

namespace LibView
{
    namespace model
    {
        namespace data_structure
        {
            Timing::Timing(ModelFacade* pModel)
                : LibertyFileGroup(pModel),
                // Never use variables as initialiser values
                related_pin(""),
                timing_sense(""),
                timing_type("")
            {}

            Timing::~Timing()
            {}

            /**
             * @return std::string
             */
            std::string Timing::getTiming_sense()
            {
                return timing_sense;
            }

            /**
             * @param value
             */
            void Timing::setTiming_sense(std::string value)
            {
                orderedAttributes.push_back("timing_sense");
                orderedAttributeValues.push_back(value);
                path.insert(path.find_last_of("(") + 1, "|" + value + "|");
                timing_sense = value;
            }

            /**
             * @return std::string
             */
            std::string Timing::getTiming_type()
            {
                return timing_type;
            }

            /**
             * @param value
             */
            void Timing::setTiming_type(std::string value)
            {
                orderedAttributes.push_back("timing_type");
                orderedAttributeValues.push_back(value);
                path.insert(path.find_last_of("(") + 1, "|" + value + "|");
                timing_type = value;
            }

            /**
             * @return std::string
             */
            std::string Timing::getRelated_pin()
            {
                return related_pin;
            }

            /**
             * @param value
             */
            void Timing::setRelated_pin(std::string value)
            {
                orderedAttributes.push_back("related_pin");
                orderedAttributeValues.push_back(value);
                path.insert(path.find_last_of("(") + 1, "|" + value + "|");
                related_pin = value;
            }
        }
    }
}