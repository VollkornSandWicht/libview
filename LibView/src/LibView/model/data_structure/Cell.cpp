/**
 * Project LibView
 * @author Leonard Wolf (2207778),  Mirko Sowa (2263258), Daniel Martins Fiebich (2219734), Christoph Böhrer (2070080), Justin Guth (2218548)
 * @version 1.0
 */

 /**
  * Cell implementation
  *
  * class that contains attributes and the according getter and setter for the cellgroup
  */

#include "Cell.h"

namespace LibView
{
    namespace model
    {
        namespace data_structure
        {
            Cell::Cell(ModelFacade* pModel)
                : 
                LibertyFileGroup(pModel),
                isSetCell_leakage_power(false),
                cell_leakage_power(0)
            {}

            Cell::~Cell()
            {}

            /**
             * @return double
             */
            double Cell::getCell_leakage_power()
            {
                return cell_leakage_power;
            }

            /**
             * @param value
             */
            void Cell::setCell_leakage_power(double value)
            {
                orderedAttributes.push_back("cell_leakage_power");
                orderedAttributeValues.push_back(std::to_string(value));
                isSetCell_leakage_power = true;
                cell_leakage_power = value;
            }
        }
    }
}