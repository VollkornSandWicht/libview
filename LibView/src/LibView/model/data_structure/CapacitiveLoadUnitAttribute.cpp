/**
 * Project LibView
 * @author Leonard Wolf (2207778),  Mirko Sowa (2263258), Daniel Martins Fiebich (2219734), Christoph Böhrer (2070080), Justin Guth (2218548)
 * @version 1.0
 */

 /**
  * CapacitiveLoadUnitAttribute implementation
  *
  * class for an atrribute that consists of one enum and a double
  */

#include "CapacitiveLoadUnitAttribute.h"

namespace LibView
{
    namespace model
    {
        namespace data_structure
        {
            CapacitiveLoadUnitAttribute::CapacitiveLoadUnitAttribute() 
                : doublePart(0),
                // Never use variables as initialiser values
                stringPart("")
                
            {}

            CapacitiveLoadUnitAttribute::~CapacitiveLoadUnitAttribute()
            {}

            /**
             * @return std::string
             */
            std::string CapacitiveLoadUnitAttribute::getStringPart()
            {
                return stringPart;
            }

            /**
             * @param value
             */
            void CapacitiveLoadUnitAttribute::setStringPart(std::string value)
            {
                stringPart = value;
            }

            /**
             * @return double
             */
            double CapacitiveLoadUnitAttribute::getDoublePart()
            {
                return doublePart;
            }

            /**
             * @param value
             */
            void CapacitiveLoadUnitAttribute::setDoublePart(double value)
            {
                doublePart = value;
            }

            std::string CapacitiveLoadUnitAttribute::toString()
            {
                std::ostringstream strs;
                strs << doublePart;
                std::string result = strs.str();
                result = "capacitive_load_unit(" + result + ", " + stringPart + ");";
                return result;
            }
        }
    }
}