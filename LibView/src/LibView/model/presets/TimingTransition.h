#pragma once

#include "LibView/model/APreset.h"

namespace LibView
{
    namespace model
    {
        class TimingTransition : public APreset
        {
        public:

            inline TimingTransition() :
                APreset("TimingTransition")
            {
                addPath("(.*)(rise_transition)(.*)");
                addPath("(.*)(fall_transition)(.*)");
            }
        };
    }
}