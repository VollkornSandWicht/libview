#pragma once

#include "LibView/model/APreset.h"

namespace LibView
{
    namespace model
    {
        class AllPowerTables : public APreset
        {
        public:

            inline AllPowerTables() :
                APreset("AllPowerTables")
            {
                addPath("(.*)(fall_power)(.*)");
                addPath("(.*)(rise_power)(.*)");
            }
        };
    }
}