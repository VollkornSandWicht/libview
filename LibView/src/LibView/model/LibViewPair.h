#pragma once
#include <string>
#include "LibView/core/LibFileIndex.h"

namespace LibView
{
    namespace model
    {
        /**
         * A simple templated storage class for storing pairs of two different types.
         * 
         * @tparam T_Type1 (typename) : The type of the first pair element.
         * @tparam T_Type2 (typename) : The type of the second pair element.
         */

        template <typename T_Type1, typename T_Type2>
        class LibViewPair
        {
        private:

            /**
             * The first element of the pair.
             */
            T_Type1 m_First;


            /**
             * The second element of the pair.
             */
            T_Type2 m_Second;

        public:


            /**
             * Assigns a given value to the first element of the pair.
             * 
             * @param[in] first (T_Type1) : the value to be assigned to the first element.
             */
            inline void setFirst(T_Type1 first) { m_First = first; }
            
            
            /**
             * Assigns a given value to the second element of the pair.
             *
             * @param[in] second (T_Type2) : the value to be assigned to the second element.
             */
            inline void setSecond(T_Type2 second) { m_Second = second; }


            /**
             * Returns the value of the first element of the pair.
             *
             * @return T_Type1 : the value of the first element.
             */
            inline T_Type1 getFirst() const { return m_First; }
            
            
            /**
             * Returns the value of the second element of the pair.
             *
             * @return T_Type2 : the value of the second element.
             */
            inline T_Type2 getSecond() const { return m_Second; }


            /**
             * Overloaded constructor. Initializes the values of the pair.
             *
             * @param[in] first (T_Type1) : the value to be assigned to the first element.
             * @param[in] second (T_Type2) : the value to be assigned to the second element.
             */
            LibViewPair(T_Type1 first, T_Type2 second) : m_First(first), m_Second(second) {}

            
            LibViewPair(const LibViewPair<T_Type1, T_Type2>& other) : m_First(other.getFirst()), m_Second(other.getSecond()) {}
            
            /**
             * Default constructor. 
             */
            LibViewPair() : m_First(), m_Second() {}

            /**
             * Default destructor.
             */
            ~LibViewPair() {};
            

            /**
             * equivalence operator
             */
            inline bool operator==(const LibViewPair<T_Type1, T_Type2>& other) { return m_First == other.m_First && m_Second == other.m_Second; };
            
            /**
             * ambivalence operator
             */
            inline bool operator!=(const LibViewPair<T_Type1, T_Type2>& other) { return !operator==(other); };



        };

        typedef LibViewPair<std::string, index_t> path_index_pair;
    }
}