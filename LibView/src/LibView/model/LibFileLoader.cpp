/**
 * Project LibView
 * @author Leonard Wolf (2207778),  Mirko Sowa (2263258), Daniel Martins Fiebich (2219734), Christoph Böhrer (2070080), Justin Guth (2218548)
 * @version 1.0
 */


#include "LibFileLoader.h"
#include "LibView/model/FileIO.h"


/**
 * LibFileLoader implementation
 * 
 * Responsible for loading LibFile files with a given path and parses them into a LibFileObject.
 */

namespace LibView
{
    namespace model
    {
        /**
         * @param path
         * @return LibertyFileData
         */
        LibFileLoader::LibFileLoader(ModelFacade* pFacade)
            :
            m_BuildDirector(data_structure::BuildDirector(pFacade))
        {}
        data_structure::LibertyFileData* LibFileLoader::loadFromFile(const std::string& path)
        {
            data_structure::LibertyFileData* fileData =  m_BuildDirector.loadLibertyFile(path);
            fileData->setFileContents(FileIO::readFile(path));

            return fileData;
        }
    }
}