
#include "AttributeHolder.h"
#include "LibViewPair.h"
#include "LibView/exception/model/AttributePathNotFoundException.h"
#include "LibView/core/DebugLog.h"

namespace LibView
{
    namespace model
    {
        void AttributeHolder::removeAttribute(std::string attributeHierarchy)
        {
            int64_t multiplicity = m_AttributeMap[attributeHierarchy].getSecond();
            index_t index = m_AttributeMap[attributeHierarchy].getFirst();
            path_index_pair libViewPair(attributeHierarchy, index);

            if(multiplicity - 1 == 0)
            {
                m_AttributeMap.remove(attributeHierarchy);
                
                addToLastRemoved(libViewPair);
            }
            else
            {
                m_AttributeMap.setMultiplicity(attributeHierarchy, multiplicity - 1);
            }
        }

        void AttributeHolder::addAttribute(const std::string& attributeHierarchy)
        {
            bool existsAlready = false;

            //LIBVIEW_LOG_BEGIN
            //    LIBVIEW_LOG_ELEMENT("Reading attribute : \"")
            //    LIBVIEW_LOG_ELEMENT(attributeHierarchy)
            //    LIBVIEW_LOG_ELEMENT("\"")
            //LIBVIEW_LOG_END

            existsAlready = m_AttributeMap.has(attributeHierarchy);
           

            if(existsAlready)
            {
                //LIBVIEW_LOG("Path already exists...");
                //LIBVIEW_LOG("Incrementing Multiplicity");

                int64_t multiplicity = m_AttributeMap[attributeHierarchy].getSecond();

                m_AttributeMap.setMultiplicity(
                    attributeHierarchy,
                    multiplicity + 1
                );

            }
            else
            {
                //LIBVIEW_LOG("Creating New Attribute...");
                index_t newIndex = index_t::requestUnique();
                m_AttributeMap.add(attributeHierarchy, newIndex , 1);
                path_index_pair pair = LibViewPair(attributeHierarchy, newIndex);
                addToLastAdded(pair);
            }


        }

      


        std::vector<index_t> AttributeHolder::getMatchingAttributeList(std::string regexString)
        {
            std::regex reg1 = std::regex(regexString);
            std::vector<index_t> foundIndicies;
            for(LibViewPair<index_t, int64_t> pair : m_AttributeMap.getPairList())
            {
               
                std::string groupName = m_AttributeMap.getPath(pair.getFirst());
                bool containsRegex = std::regex_search(groupName, reg1);
                if(containsRegex)
                {
                    foundIndicies.push_back(pair.getFirst());
                }
            }
            
            return foundIndicies;
        }

        std::vector<index_t> AttributeHolder::getChildren(const index_t& index)
        {
            const std::string base = m_AttributeMap.getPath(index);
            size_t baseLength = base.length();

            std::vector<index_t> result;

            for(const index_t& ind : m_AttributeMap.getIndexList())
            {
                const std::string checking = m_AttributeMap.getPath(ind);

                if(baseLength > checking.length())
                {
                    continue;
                }

                if(base == checking.substr(0, baseLength))
                {
                    result.push_back(ind);
                }
            }



            return result;
        }


        std::string AttributeHolder::getAttribute(const index_t& index) const
        {
            return m_AttributeMap.getPath(index);
        }

        index_t AttributeHolder::getAttribute(const std::string& path) const
        {
            return m_AttributeMap.getIndex(path);
        }

        

    }
}