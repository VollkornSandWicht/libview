#pragma once


#include "LibViewPair.h"
#include <vector>
#include <set>
#include "LibView/core/LibFileIndex.h"




namespace LibView
{
    namespace model
    {
        class AHolder
        {
        public:


            /**
             * Empties the last added/removed list after the model state is updated.
             */
            void clearLastStoredItems();

            /**
             * Adds a LibViewPair to the last added list
             *
             * @param pair (path_index_pair) : Pair of path and index
             */
            void addToLastAdded(path_index_pair pair);

            /**
             * clear list of last added elements
             */
            void resetLastAdded();
            
            /**
             * clear list of last removed elements
             */
            void resetLastRemoved();

            /**
             * Adds a LibViewPair to the last removed list
             *
             * @param pair (path_index_pair) : Pair of path and index
             */
            void addToLastRemoved(path_index_pair pair);

            /**
             * Returns all last added LibViewPair s
             *
             * @return std::vector<path_index_pair> : List of LibViewPair, pair of path and index
             */
            std::vector<path_index_pair> getLastAdded() const;

            /**
             * Returns all last removed LibViewPair s
             *
             * @return std::vector<LibViewPair<K, J>> : List of LibViewPair, pair of path and index
             */
            std::vector<path_index_pair> getLastRemoved() const;

        protected:

        

        private:
            std::vector<path_index_pair> m_LastRemoved;
            std::vector<path_index_pair> m_LastAdded;
        };
    }

}