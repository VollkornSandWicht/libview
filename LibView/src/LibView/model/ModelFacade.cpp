#include "ModelFacade.h"
#include "LibView/presenter/PresenterFacade.h"

#include "LibView/model/ModelFacade.h"
#include "LibView/presenter/PresenterFacade.h"



namespace LibView
{
    namespace model
    {



        ModelFacade::ModelFacade()
            :
            m_ModelState(new ModelState()),
            m_LibFileHolder(new LibFileHolder(this)),
            //m_AttributeHolder(new AttributeHolder()),
            m_pPresenter(nullptr),
            m_PresetHolder(new PresetHolder())
        {
            //m_ModelState->m_AttributeHolder = m_LibFileHolder->getAttributeHolder();
            m_ModelState->m_LibFileHolder = m_LibFileHolder;

        }

        ModelFacade::~ModelFacade()
        {
            delete m_ModelState;
            delete m_PresetHolder;
            delete m_LibFileHolder;
        }


        void ModelFacade::loadLibertyFiles(const std::vector<std::string>& paths)
        {

            m_LibFileHolder->getAttributeHolder()->resetLastAdded();
            m_LibFileHolder->loadLibertyFiles(paths); //TODO check for exceptions
        }

        void ModelFacade::removeLibertyFile(const index_t& index)
        {
            m_LibFileHolder->removeLibertyFile(index);
        }

        std::vector<index_t> ModelFacade::sortBy(const index_t& groupToSortBy, bool ascending)
        {
            return std::vector<index_t>(); // TODO implement: iterate through LibFile and check for occurences of this attribute
        }

        std::vector<index_t> ModelFacade::getMatchingAttributes(std::string searchTerm)
        {
            return m_LibFileHolder->getAttributeHolder()->getMatchingAttributeList(searchTerm);
        }


        std::vector<index_t> ModelFacade::getChildren(const index_t& index)
        {
            return m_LibFileHolder->getChildren(index);  //TODO check for exceptions
        }

        void ModelFacade::pushModelStateToPresenter()
        {
            m_pPresenter->notifyOfModelStateUpdate(*m_ModelState);
        }

        void ModelFacade::init(presenter::PresenterFacade* presenterFacade)
        {
            // HolderObserver(this, m_ModelState, m_AttributeHolder, m_LibFileHolder);
            m_pPresenter = presenterFacade;
        }

        ModelState& ModelFacade::getModelState() const
        {
            return *m_ModelState;
        }




        double ModelFacade::getDelta(const index_t& libFileIndex, const index_t& attributeIndex)
        {
            return 0.0;
        }

        model::data_structure::LibertyFileGroup ModelFacade::getGroupByIndex(const index_t& groupIndex) const
        {
            return  nullptr;//model::data_structure::LibertyFileGroup(this);
            //TODO FIX
        }

        model::data_structure::LibertyFileData* ModelFacade::getLibFileByIndex(const index_t& libFileIndex) const
        {
            return m_LibFileHolder->getLibFileByIndex(libFileIndex);
        }
        void ModelFacade::addLastAddedLibFiles(const index_t& index)
        {
            m_ModelState->m_LastAddedLibFiles.push_back(index);
        }
        void ModelFacade::setLastRemovedLibbFile(const index_t& removedFile)
        {
            m_ModelState->m_LastRemovedLibFiles.push_back(removedFile);
        }
        void ModelFacade::resetLastAddedLibFiles()
        {
            m_ModelState->m_LastAddedLibFiles.clear();
        }
        void ModelFacade::resetLastRemovedLibFiles()
        {
            m_ModelState->m_LastRemovedLibFiles.clear();
        }
        void ModelFacade::resetLastAddedAttributes()
        {
            m_LibFileHolder->getAttributeHolder()->resetLastAdded();
        }
        void ModelFacade::resetLastRemovedAttributes()
        {
            m_LibFileHolder->getAttributeHolder()->resetLastRemoved();

        }
        void ModelFacade::addAttribute(const std::string& attributeHierarchy)
        {
            m_LibFileHolder->getAttributeHolder()->addAttribute(attributeHierarchy);

        }
        void ModelFacade::removeAttribute(const std::string& attributeHierarchy)
        {
            m_LibFileHolder->getAttributeHolder()->removeAttribute(attributeHierarchy);
        }


        std::string ModelFacade::getAttributeName(const index_t& index)
        {
            return m_LibFileHolder->getAttributeHolder()->getAttribute(index);
        }

        index_t ModelFacade::getAttributeIndex(const std::string& path)
        {
            return m_LibFileHolder->getAttributeHolder()->getAttribute(path);
        }

        bool ModelFacade::isLastRemovedAttribute(const index_t& index)
        {
            for(const path_index_pair& pip : m_LibFileHolder->getAttributeHolder()->getLastRemoved())
            {
                if(pip.getSecond() == index)
                {
                    return true;
                }
            }

            return false;
        }
        const APreset* ModelFacade::getPreset(const index_t& index) const
        {
            for(APreset* preset : m_PresetHolder->getPresets())
            {
                if(preset->index() == index)
                {
                    return preset;
                }
            }

            throw -1; // TODO
        }
        const std::vector<APreset*>& ModelFacade::getPresetList() const
        {
            return m_PresetHolder->getPresets();
        }
    }
}