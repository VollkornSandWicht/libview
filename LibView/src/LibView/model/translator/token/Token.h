#pragma once
/**
 * Project LibView
 * @author Leonard Wolf (2207778),  Mirko Sowa (2263258), Daniel Martins Fiebich (2219734), Christoph Böhrer (2070080), Justin Guth (2218548)
 * @version 1.0
 */




#include "TokenKeys.h"
#include <string>
#include <stdint.h>


namespace LibView
{
	namespace model
	{
		namespace translator
		{
			namespace token
			{
				/**
				* Smallest logical unit of a text.
				*/
				class Token {

				private:
					//member
					std::string m_Value;
					uint64_t m_Column;
					uint64_t m_Row;
					TokenKeys m_Key;

				public:

					/**
					* Constructor
					* @param key [TokenKeys]
					* @param row [uint64_t]
					* @param column [uint64_t]
					* @param word [std::string] : word/ unit in the text
					*/
					Token(TokenKeys key, uint64_t row, uint64_t column, std::string word);
					Token(); //necessary at the moment, because of direct use and not pointer use in other classes. Should be refactored.

					/**
					* Copy constructor
					*/
					Token(const Token& other);

					/**
					* Getter for the
					*
					*/
					TokenKeys getKey() const;

					/**
				   * Getter for the word/ unit in the text.
				   * @return value [std::string]
				   */
					std::string getValue() const;

					/**
				   * Getter for the column number.
				   * @return column [uint64_t]
				   */
					const uint64_t getColumn() const;

					/**
				   * Getter for the row numebr.
				   * @return row [uint64_t]
				   */
					const uint64_t getRow() const;

					/**
					* Formatted string representing the token.
					* @return [std::string]
					*/
					std::string toString() const;

					/**
					* Checks if two tokens are equal in their members.
					* @return [bool]
					*/
					bool isEqual(Token other);

					Token& operator=(const Token& other);

				};

			}
		}
	}
}