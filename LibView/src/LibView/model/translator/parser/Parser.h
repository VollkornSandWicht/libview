#pragma once
/**
 * Project LibView
 * @author Leonard Wolf (2207778),  Mirko Sowa (2263258), Daniel Martins Fiebich (2219734), Christoph Böhrer (2070080), Justin Guth (2218548)
 * @version 1.0
 */



//#include "LibView/model/translator/tokenizer/Tokenizer.h"
#include "LibView/model/translator/ast/Group.h"
#include <string>

namespace LibView
{
	namespace model
	{
		namespace translator
		{
			namespace parser
			{
				/**
				* Abstract parser class showing the interface to calculate an abstract sytax tree out of a liberty file.
				*/
				class Parser {
					/*
				private:
					tokenizer::Tokenizer* m_Tokenizer;
				*/ //design change -> tokenizer in constructor
				
				
				public:
					/**
					* The interface to the parser. This function creates an abstract syntax tree of a liberty file given through the tokenizer.
					* @param path [std::string] : The path to the file in the systems file structure.
					*/
					virtual translator::ast::Group* calculateAST(const std::string& path) = 0;

				
					
					/*
				private:
					inline bool hasNextToken() const { return m_Tokenizer->hasNextToken(); };
					inline token::Token getNextToken() const { return m_Tokenizer->getNextToken(); };
					*/ //design change
				};

			}
		}
	}
}