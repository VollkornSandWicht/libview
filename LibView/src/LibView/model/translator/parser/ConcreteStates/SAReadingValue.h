#pragma once
/**
 * Project LibView
 * @author Leonard Wolf (2207778),  Mirko Sowa (2263258), Daniel Martins Fiebich (2219734), Christoph Böhrer (2070080), Justin Guth (2218548)
 * @version 1.0
 */



#include "LibView/model/translator/parser/StackParserState.h"

namespace LibView
{
	namespace model
	{
		namespace translator
		{
			namespace parser
			{
				namespace concreteState {

					/**
					* Entered first time reading a token for the value of a simpel attribute.
					*/
					class SAReadingValue : public StackParserState {
					public:

						/**
						* Constructor
						* @param parser [StackParser&] : The parser the state is part of.
						*/
						SAReadingValue(StackParser& parser);

					protected:
						//Tokens are collected
						inline void handleFloat() override { addParameter(); };
						inline void handleFloatS() override { addParameter(); };
						inline void handleInteger() override { addParameter(); };
						inline void handleName() override { addParameter(); };
						inline void handleOperator() override { addParameter(); };

						void handleStringEnclosure() override; //Special state for strings entered

					private:
						/**
						* Adds parameter and sets state to read.
						*/
						void addParameter();
					};
				}
			}
		}
	}
}