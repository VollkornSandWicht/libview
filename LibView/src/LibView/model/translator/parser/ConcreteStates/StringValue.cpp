/**
 * Project LibView
 * @author Leonard Wolf (2207778),  Mirko Sowa (2263258), Daniel Martins Fiebich (2219734), Christoph Böhrer (2070080), Justin Guth (2218548)
 * @version 1.0
 */


#include "StringValue.h"
#include "LibView/model/translator/parser/StackParser.h"
namespace LibView
{
    namespace model
    {
        namespace translator
        {
            namespace parser
            {
                namespace concreteState {
                    StringValue::StringValue(StackParser& parser):
                        StackParserState(parser)
                    {
                    }
                    void StringValue::handleStringEnclosure()
                    {
                        m_Parser.addParameter();
                        setState(new concreteState::SARead(m_Parser));
                    }
                    void StringValue::handleRuleEscape()
                    {
                        setState(new concreteState::EscapedExit(m_Parser));
                    }
                    void StringValue::addParameter()
                    {
                        m_Parser.addParameter();
                    }
                   
                }
            }
        }
    }
}