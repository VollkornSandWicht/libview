#pragma once
/**
 * Project LibView
 * @author Leonard Wolf (2207778),  Mirko Sowa (2263258), Daniel Martins Fiebich (2219734), Christoph Böhrer (2070080), Justin Guth (2218548)
 * @version 1.0
 */

#include "LibView/model/translator/parser/StackParserState.h"
namespace LibView
{
	namespace model
	{
		namespace translator
		{
			namespace parser
			{
				namespace concreteState {

					/**
					* Allows the use of a new line inside the value list of a complex attribute, which is otherwise forbidden.
					* (Multilining marked by \ in liberty files)
					*/
					class Multiline : public StackParserState {
					public:

						/**
						* Constructor 
						* @param parser [StackParser&] : The parser the state is part of.
						*/
						Multiline(StackParser& parser);
						
						void handleNewLine() override;
					};

				}
			}
		}
	}
}