/**
 * Project LibView
 * @author Leonard Wolf (2207778),  Mirko Sowa (2263258), Daniel Martins Fiebich (2219734), Christoph Böhrer (2070080), Justin Guth (2218548)
 * @version 1.0
 */


#include "ParameterRead.h"
#include "LibView/model/translator/parser/StackParser.h"
namespace LibView
{
    namespace model
    {
        namespace translator
        {
            namespace parser
            {
                namespace concreteState {
                    /**
                     * ParameterRead implementation
                     */
                    ParameterRead::ParameterRead(StackParser& parser):
                        StackParserState(parser)
                    {
                    }
                    void ParameterRead::handleLSE()
                    {
                        m_Parser.pushGroup();
                        setState(new concreteState::StartTypeReading(m_Parser));
                    }
                    void ParameterRead::handleAttributeCloser()
                    {
                        m_Parser.addComplexAttribute();
                        setState(new concreteState::StartTypeReading(m_Parser));
                    }
                }
            }
        }
    }
}