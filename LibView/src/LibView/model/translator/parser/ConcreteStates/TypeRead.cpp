/**
 * Project LibView
 * @author Leonard Wolf (2207778),  Mirko Sowa (2263258), Daniel Martins Fiebich (2219734), Christoph Böhrer (2070080), Justin Guth (2218548)
 * @version 1.0
 */


#include "TypeRead.h"
#include "LibView/model/translator/parser/StackParser.h"
namespace LibView
{
    namespace model
    {
        namespace translator
        {
            namespace parser
            {
                namespace concreteState {
                    /**
                     * TypeRead implementation
                     */
                    TypeRead::TypeRead(StackParser& parser):
                        StackParserState(parser)
                    {
                    }
                    void TypeRead::handleLPE()
                    {
                        setState(new concreteState::ParameterReading(m_Parser));
                    }
                    void TypeRead::handleSAV()
                    {
                        setState(new concreteState::SAReadingValue(m_Parser));
                    }
                }
            }
        }
    }
}