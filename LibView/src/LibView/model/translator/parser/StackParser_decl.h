#pragma once

namespace LibView
{
	namespace model
	{
		namespace translator
		{
			namespace parser
			{
				/**
				* Cyclic include declaration for the STackParser class.
				*/
				class StackParser;
			}
		}
	}
}