#pragma once

#include "Transitions.h"
#include "States.h"

namespace LibView
{
	namespace model
	{
		namespace translator
		{
			namespace tokenizer
			{
				/**
				* Encapsulation of the tokeniser state mashine transition calculation.
				*/
				class Transition {

				private:

					/**
					* Describes the state mashine by definig the transition table.
					*/
					States c_Table[(int)States::SIZE][Transitions::SIZE];



				private:
					/**
					* Does the mapping of all allowed ascii characters to transitions.
					* @return transition [int]
					*/
					int getTransition(char a);

				public:
					/**
					* Constructor setting up the transition table.
					*/
					Transition();

					/**
					* Looks at the transition table to determine the next state to a given tuple of pair and character.
					* @param state [States] : The current state.
					* @param character [char] : The character that leads to the transition in state.
					*/
					States getNextState(States state, char character);
				};
			}
		}
	}
}