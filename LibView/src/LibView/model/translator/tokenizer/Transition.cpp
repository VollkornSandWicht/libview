#include "Transition.h"

namespace LibView
{
	namespace model
	{
		namespace translator
		{
			namespace tokenizer
			{

				Transition::Transition() {
					//init with every transition goes to error
					for (int i = 0; i < (int)States::SIZE; i++) {
						for (int j = 0; j < Transitions::SIZE; j++) {
							Transition::c_Table[i][j] = States::Error;
						}
					}


					//Allow everything inside of a comment and override escape later
					for (int i = 0; i < Transitions::SIZE; i++) {
						Transition::c_Table[(int)States::CommentMid][i] = States::CommentMid;
					}
					//Get back to comment when ending is interrupted, override true ending later
					for (int i = 0; i < Transitions::SIZE; i++) {
						Transition::c_Table[(int)States::CommentEndStarted][i] = States::CommentMid;
					}


					//defining the transition table

						//START

						//Names
					Transition::c_Table[(int)States::Start][Transitions::Letters] = States::NameId;
					Transition::c_Table[(int)States::Start][Transitions::Letter_e] = States::NameId;

					//Maths
					Transition::c_Table[(int)States::Start][Transitions::Plus] = States::Plus;
					Transition::c_Table[(int)States::Start][Transitions::Dash] = States::Minus;

					Transition::c_Table[(int)States::Start][Transitions::Numbers] = States::Integer;


					//whitespaces
					Transition::c_Table[(int)States::Start][Transitions::Space] = States::Spacing;
					Transition::c_Table[(int)States::Start][Transitions::LineFeed] = States::NewLine;
					Transition::c_Table[(int)States::Start][Transitions::Tab] = States::Tab;
					Transition::c_Table[(int)States::Start][Transitions::CarriageReturn] = States::NewLineCR;

					//structural

					Transition::c_Table[(int)States::Start][Transitions::LeftCurlyBracket] = States::LeftStatementEnclosure;
					Transition::c_Table[(int)States::Start][Transitions::RightCurlyBracket] = States::RightStatementEnclosure;

					Transition::c_Table[(int)States::Start][Transitions::LeftSquareBracket] = States::LeftFieldEnclosure;
					Transition::c_Table[(int)States::Start][Transitions::Colon] = States::SeperatorFieldRange;
					Transition::c_Table[(int)States::Start][Transitions::RightSquareBracket] = States::RightFieldEnclosure;

					Transition::c_Table[(int)States::Start][Transitions::LeftBracket] = States::LeftParameterEnclosure;
					Transition::c_Table[(int)States::Start][Transitions::RightBracket] = States::RightParameterEnclosure;

					Transition::c_Table[(int)States::Start][Transitions::BackwardSlash] = States::RuleEscape;
					Transition::c_Table[(int)States::Start][Transitions::Semicolon] = States::AttributeCloser;

					Transition::c_Table[(int)States::Start][Transitions::Quotes] = States::StringEnclosure;

					Transition::c_Table[(int)States::Start][Transitions::Comma] = States::ParameterSeperator;
					Transition::c_Table[(int)States::Start][Transitions::Asterisk] = States::OperatorAsterisk;

					//STRING ATTRIBUTES
					Transition::c_Table[(int)States::Start][Transitions::SpecialCharacter] = States::SpecialCharacter;

					//COMMENT
					Transition::c_Table[(int)States::Start][Transitions::ForwardSlash] = States::CommentStart;



					//NAMEID
					Transition::c_Table[(int)States::NameId][Transitions::Letters] = States::NameId;
					Transition::c_Table[(int)States::NameId][Transitions::Letter_e] = States::NameId;
					Transition::c_Table[(int)States::NameId][Transitions::Numbers] = States::NameId;
					Transition::c_Table[(int)States::NameId][Transitions::Underscore] = States::FaultyNameId;

					//FAULTYNAMEID
					Transition::c_Table[(int)States::FaultyNameId][Transitions::Letters] = States::NameId;
					Transition::c_Table[(int)States::FaultyNameId][Transitions::Letter_e] = States::NameId;
					Transition::c_Table[(int)States::FaultyNameId][Transitions::Numbers] = States::NameId;

					//PLUS
					Transition::c_Table[(int)States::Plus][Transitions::Numbers] = States::Integer;

					//MINUS
					Transition::c_Table[(int)States::Minus][Transitions::Numbers] = States::Integer;

					//INTEGER
					Transition::c_Table[(int)States::Integer][Transitions::Numbers] = States::Integer;
					Transition::c_Table[(int)States::Integer][Transitions::FullStop] = States::StartedFloat;
					Transition::c_Table[(int)States::Integer][Transitions::Letter_e] = States::StartedFloatScientific;

					//STARTEDFLOAT
					Transition::c_Table[(int)States::StartedFloat][Transitions::Numbers] = States::Float;

					//FLOAT
					Transition::c_Table[(int)States::Float][Transitions::Numbers] = States::Float;
					Transition::c_Table[(int)States::Float][Transitions::Letter_e] = States::StartedFloatScientific;

					//STARTEDFLOATSCIENTIFIC
					Transition::c_Table[(int)States::StartedFloatScientific][Transitions::Dash] = States::StartedFloatScientificSigned;
					Transition::c_Table[(int)States::StartedFloatScientific][Transitions::Numbers] = States::FloatScientific;

					//FLOATSCIENTIFSIGNED
					Transition::c_Table[(int)States::StartedFloatScientificSigned][Transitions::Numbers] = States::FloatScientific;

					//FLOATSCIENTIFIC
					Transition::c_Table[(int)States::FloatScientific][Transitions::Numbers] = States::FloatScientific;

					//COMMENTSTARTED
					Transition::c_Table[(int)States::CommentStart][Transitions::Asterisk] = States::CommentMid;

					//COMMENTMID
					Transition::c_Table[(int)States::CommentMid][Transitions::Asterisk] = States::CommentEndStarted;

					//COMMENTENDSTARTED
					Transition::c_Table[(int)States::CommentEndStarted][Transitions::ForwardSlash] = States::Comment;

					//SPACING
					Transition::c_Table[(int)States::Spacing][Transitions::Space] = States::Spacing; //comment out if you like to have every space seperated
					Transition::c_Table[(int)States::Spacing][Transitions::Colon] = States::StartedAttributeValueSeperator;

					//TAB
					Transition::c_Table[(int)States::Tab][Transitions::Tab] = States::Tab; //comment out if you like to have every tab seperated

					//STARTEDATTRIBUTEVLAUESEPERATOR
					Transition::c_Table[(int)States::StartedAttributeValueSeperator][Transitions::Space] = States::AttributeValueSeperator;


					//NEWLINECR
					Transition::c_Table[(int)States::NewLineCR][Transitions::LineFeed] = States::NewLine;

				}

				int Transition::getTransition(char a)
				{

					// assert(a > 0 & a < 128);

					if (a <= 8)
					{
						return Transitions::NoSupportCC;
					}
					else if (a <= 9)
					{
						return Transitions::Tab;
					}
					else if (a <= 10)
					{
						return Transitions::LineFeed;
					}
					else if (a <= 12)
					{
						return Transitions::NoSupportCC;
					}
					else if (a <= 13)
					{
						return Transitions::CarriageReturn;
					}
					else if (a <= 31)
					{
						return Transitions::NoSupportCC;
					}
					else if (a <= 32)
					{
						return Transitions::Space;
					}
					else if (a <= 33)
					{
						return Transitions::SpecialCharacter;
					}
					else if (a <= 34)
					{
						return Transitions::Quotes;
					}
					else if (a <= 39)
					{
						return Transitions::SpecialCharacter;
					}
					else if (a <= 40)
					{
						return Transitions::LeftBracket;
					}
					else if (a <= 41)
					{
						return Transitions::RightBracket;
					}
					else if (a <= 42)
					{
						return Transitions::Asterisk;
					}
					else if (a <= 43)
					{
						return Transitions::Plus;
					}
					else if (a <= 44)
					{
						return Transitions::Comma;
					}
					else if (a <= 45)
					{
						return Transitions::Dash;
					}
					else if (a <= 46)
					{
						return Transitions::FullStop;
					}
					else if (a <= 47)
					{
						return Transitions::ForwardSlash;
					}
					else if (a <= 57)
					{
						return Transitions::Numbers;
					}
					else if (a <= 58)
					{
						return Transitions::Colon;
					}
					else if (a <= 59)
					{
						return Transitions::Semicolon;
					}
					else if (a <= 64)
					{
						return Transitions::SpecialCharacter;
					}
					else if (a <= 90)
					{
						return Transitions::Letters;
					}
					else if (a <= 91)
					{
						return Transitions::LeftSquareBracket;
					}
					else if (a <= 92)
					{
						return Transitions::BackwardSlash;
					}
					else if (a <= 93)
					{
						return Transitions::RightSquareBracket;
					}
					else if (a <= 94)
					{
						return Transitions::SpecialCharacter;
					}
					else if (a <= 95)
					{
						return Transitions::Underscore;
					}
					else if (a <= 96)
					{
						return Transitions::SpecialCharacter;
					}
					else if (a <= 100)
					{
						return Transitions::Letters;
					}
					else if (a <= 101)
					{
						return Transitions::Letter_e;
					}
					else if (a <= 122)
					{
						return Transitions::Letters;
					}
					else if (a <= 123)
					{
						return Transitions::LeftCurlyBracket;
					}
					else if (a <= 124)
					{
						return Transitions::SpecialCharacter;
					}
					else if (a <= 125)
					{
						return Transitions::RightCurlyBracket;
					}
					else if (a <= 126)
					{
						return Transitions::SpecialCharacter;
					}
					else if (a <= 127)
					{
						return Transitions::NoSupportCC;
					}
					//not needed
					return Transitions::NoSupportCC;
				}

				States Transition::getNextState(States state, char character)
				{
					//as many characters behave the same as others, we group them in possible transitions.
					return c_Table[(int)state][getTransition(character)];
				}
			}
		}
	}
}
