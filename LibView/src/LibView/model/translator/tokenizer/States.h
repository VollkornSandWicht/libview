#pragma once

namespace LibView
{
	namespace model
	{
		namespace translator
		{
			namespace tokenizer
			{
				/**
				* Encapsulation of the on demand tokenizer state mashine states.
				* In this case states are a set of fixed named integers realised as a enumeration.
				*/
				enum class States {
					//state mashine specific
					Error = 0,
					Start = 1,

					//structural

					LeftStatementEnclosure = 2,
					RightStatementEnclosure = 3,

					LeftParameterEnclosure = 4,
					RightParameterEnclosure = 5,

					LeftFieldEnclosure = 6,
					RightFieldEnclosure = 7,
					SeperatorFieldRange = 8,


					AttributeCloser = 9,
					StartedAttributeValueSeperator = 10,
					AttributeValueSeperator = 11,

					StringEnclosure = 12,

					RuleEscape = 13,

					ParameterSeperator = 14,

					//whitespace
					Spacing = 15,
					Tab = 16,
					NewLine = 17,
					NewLineCR = 18,

					//data

					NameId = 19,
					FaultyNameId = 20,

					Plus = 21,
					Minus = 22,

					StartedFloat = 23,
					Float = 24,
					FaultyFloat = 25,

					FloatScientific = 26,
					StartedFloatScientific = 27,
					StartedFloatScientificSigned = 28,

					Integer = 29,

					CommentStart = 30,
					CommentMid = 31,
					CommentEndStarted = 32,
					Comment = 33,



					SpecialCharacter = 34,

					OperatorAsterisk = 35,

					SIZE = 36  //Not part of the enumeration on a theoretical level, just helper to avoid magic number
				};


			}
		}
	}
}