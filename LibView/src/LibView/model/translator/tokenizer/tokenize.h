#pragma once
/**
 * Project LibView
 * @author Leonard Wolf (2207778),  Mirko Sowa (2263258), Daniel Martins Fiebich (2219734), Christoph Böhrer (2070080), Justin Guth (2218548)
 * @version 1.0
 */


#include "LibView/model/translator/token/Token.h"
namespace LibView
{
    namespace model
    {
        namespace translator
        {
            namespace tokenizer
            {
                /**
                * Interface for a token iterator. 
                */
                class tokenize {
                public:

                    /**
                     * Returns next token. Throws an exception if  a read character/ word is not part of the language.
                     */
                    virtual token::Token getNextToken() = 0;

                    /*
                    * @return bool : True if a next Token can be received, false else.
                    */
                    virtual bool hasNextToken() = 0;
                };

            }
        }
    }
}