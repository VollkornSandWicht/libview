/**
 * Project LibView
 * @author Leonard Wolf (2207778),  Mirko Sowa (2263258), Daniel Martins Fiebich (2219734), Christoph Böhrer (2070080), Justin Guth (2218548)
 * @version 1.0
 */


#include "OnDemandTokenizer.h"

 /**
  * OnDemandTokenizer implementation
  */

namespace LibView
{
	namespace model
	{
		namespace translator
		{
			namespace tokenizer
			{

				//OnDemandTokenizer::dema


				/**
				 * Partitions the states in sattes where the read word matches a valid token, and those who dont.
				 * @param state
				 * @return bool
				 */
				bool OnDemandTokenizer::isTokenState(States state)
				{
					return m_KeyMap.find(state) != m_KeyMap.end();
				}

				/**
				 * @param token
				 */
				bool OnDemandTokenizer::isNewLine(const token::Token& token) const
				{
					return token.getKey() == token::TokenKeys::NewLine;
				}

				/**
				 * @param state
				 * @return bool
				 */
				bool OnDemandTokenizer::isErrorState(States state)
				{
					return state == States::Error;
				}

				/**
				 * @param state
				 * @return TokenKeys
				 */
				token::TokenKeys OnDemandTokenizer::getKey(States state)
				{
					return m_KeyMap.at(state); //todo check if correct
				}
				/** -use this later to encapsulate m_Current++ etc-
				void OnDemandTokenizer::incrementRow()
				{
					m_CurrentRow++;
				}

				void OnDemandTokenizer::incrementColumn()
				{
					m_CurrentColumn++;
				}
				**/


				void OnDemandTokenizer::setUpKeyMap()
				{
					m_KeyMap.insert({ States::NameId, token::TokenKeys::Name });
					m_KeyMap.insert({ States::Plus, token::TokenKeys::Operator });
					m_KeyMap.insert({ States::Minus , token::TokenKeys::Operator });
					m_KeyMap.insert({ States::Integer, token::TokenKeys::Integer });
					m_KeyMap.insert({ States::Float, token::TokenKeys::Float });
					m_KeyMap.insert({ States::FloatScientific, token::TokenKeys::FloatS });
					m_KeyMap.insert({ States::Comment, token::TokenKeys::Comment });
					m_KeyMap.insert({ States::Spacing, token::TokenKeys::Spacing });
					m_KeyMap.insert({ States::AttributeValueSeperator, token::TokenKeys::SeperatorAttributeValue });
					m_KeyMap.insert({ States::NewLine, token::TokenKeys::NewLine });
					m_KeyMap.insert({ States::LeftFieldEnclosure, token::TokenKeys::LeftFieldEnclosure });
					m_KeyMap.insert({ States::RightFieldEnclosure, token::TokenKeys::RightFieldEnlosure });
					m_KeyMap.insert({ States::LeftParameterEnclosure, token::TokenKeys::LeftParameterEnclosure });
					m_KeyMap.insert({ States::RightParameterEnclosure, token::TokenKeys::RightParameterEnclosure });
					m_KeyMap.insert({ States::LeftStatementEnclosure, token::TokenKeys::LeftStatementEnclosure });
					m_KeyMap.insert({ States::RightStatementEnclosure, token::TokenKeys::RightStatementEnclosure });
					m_KeyMap.insert({ States::AttributeCloser, token::TokenKeys::AttributeCloser });
					m_KeyMap.insert({ States::RuleEscape, token::TokenKeys::RuleEscape });
					m_KeyMap.insert({ States::SeperatorFieldRange, token::TokenKeys::SeperatorFieldRange });
					m_KeyMap.insert({ States::StringEnclosure, token::TokenKeys::StringEnclosure });
					m_KeyMap.insert({ States::ParameterSeperator, token::TokenKeys::ParameterSeperator });
					m_KeyMap.insert({ States::Tab, token::TokenKeys::Tab });
					m_KeyMap.insert({ States::OperatorAsterisk, token::TokenKeys::Operator });
					m_KeyMap.insert({ States::CommentStart, token::TokenKeys::Operator });
					m_KeyMap.insert({ States::SpecialCharacter, token::TokenKeys::SpecialCharacter });
				}

				char OnDemandTokenizer::getNextCharacter()
				{
					m_CurrentColumn++;
					return m_Reader.getNextCharacter();
				}

				void OnDemandTokenizer::setPositionNewLine()
				{
					m_CurrentColumn = 1;
					m_CurrentRow++;
				}

				void OnDemandTokenizer::setCharacterHolder(char toHold)
				{
					m_CharacterHolder = toHold;
					m_CharacterHolderEmpty = false;
				}

				char OnDemandTokenizer::getHoldCharacter()
				{
					if (characterHolderIsEmpty()) {
						throw exception::TriedRetrievingEmptyVariableException(LIBVIEW_LOCATION);
					}
					return m_CharacterHolder;
				}


				OnDemandTokenizer::OnDemandTokenizer(std::string path)
					: m_KeyMap(std::map<States, token::TokenKeys>()),
					m_Transtion(Transition()),
					m_CharacterHolderEmpty(true),
					m_CurrentRow(1),
					m_CurrentColumn(0),
					m_Reader(reader::PreCalculatingCharacterReader(path))
				{

					setUpKeyMap();
				}

				OnDemandTokenizer::OnDemandTokenizer(std::string* text)
					: m_KeyMap(std::map<States, token::TokenKeys>()),
					m_Transtion(Transition()),
					m_CharacterHolderEmpty(true),
					m_CurrentRow(1),
					m_CurrentColumn(0),
					m_Reader(reader::PreCalculatingCharacterReader(text))
				{

					setUpKeyMap();

				}

				token::Token OnDemandTokenizer::getNextToken()
				{


					if ((!hasNextCharacter()) && characterHolderIsEmpty()) {
						throw exception::NoNextTokenAvailableException(LIBVIEW_LOCATION);
					}

					//at start of reading setup the char holder
					if (hasNextCharacter() && characterHolderIsEmpty()) {
						setCharacterHolder(getNextCharacter());
					}

					States currentState = States::Start;


					std::string word = "";

					//safe position of the start of the next token
					uint64_t positionRow = getCurrentRow();
					uint64_t positionColumn = getCurrentColumn();


					//start taking transitions and building the word
					word += getHoldCharacter();


					currentState = getNextState(currentState, getHoldCharacter()); //calculate transition

					//check if it was valid
					if (isErrorState(currentState)) {
						throw exception::WordNotAllowedException(LIBVIEW_LOCATION); //positional, word information could be used
					}

					//now see if the next characters belong to the current zoken, add them until a character cant be part of the word
					char nextChar;
					States nextState;

					do {


						//----------------- END OF TEXT HANDLING --------------------//
						if (!hasNextCharacter()) {
							//end of text reached

							if (!isTokenState(currentState)) {
								//read word is not a token
								throw exception::WordNotAllowedException(LIBVIEW_LOCATION); //positional, word information could be used
							}
							else {
								clearCharacterHolder();
								return token::Token(getKey(currentState), positionRow, positionColumn, word);
							}
						}
						//----------------------------------------------------------//



						nextChar = getNextCharacter();

						//look ahead state
						nextState = getNextState(currentState, nextChar); //calculate transition

						//is this charcter part of the current word?
						if (isErrorState(nextState)) {
							//next char is definetly not part of word

							setCharacterHolder(nextChar); //safe char for next token

							//was the calculated word a valid token?
							if (isTokenState(currentState)) {

								token::Token calculatedToken = token::Token(getKey(currentState), positionRow, positionColumn, word);
								if (isNewLine(calculatedToken)) {
									setPositionNewLine();
								}
								return calculatedToken;
							}
							else {
								//ran into faulty word
								throw exception::WordNotAllowedException(LIBVIEW_LOCATION); //positional, word information could be used
							}

						}

						word += nextChar;
						currentState = nextState;


					} while (true);
				}


			}
		}
	}
}