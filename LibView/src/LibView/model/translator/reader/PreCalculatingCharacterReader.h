#pragma once
/**
 * Project LibView
 * @author Leonard Wolf (2207778),  Mirko Sowa (2263258), Daniel Martins Fiebich (2219734), Christoph Böhrer (2070080), Justin Guth (2218548)
 * @version 1.0
 */



#include "readFile.h"
#include <string>
#include "LibView/model/FileIO.h"

#include "LibView/core/LibViewTesting.h"


namespace LibView
{
	namespace model
	{
		namespace translator
		{
			namespace reader
			{
				/**
				* Reader fully storing the text and passign it character by character.
				*/
				class PreCalculatingCharacterReader : public readFile {

				private:

					std::string m_Text;
					std::string::iterator m_Current;
					//std::string::iterator m_LastCharacter;
					std::string::iterator m_End;

				public:
					/*
					* @param text (std::string) : A string to be iterated.
					*/
					void readString(std::string text);

					/*
					* Constructor
					* @param textOrPath (std::string) : A path as a string to a file to be iterated.
					*/
					PreCalculatingCharacterReader(std::string path);


					/*
					* Constructor
					* @param textOrPath (std::string) : A path as a string to a file to be iterated.
					*/
					PreCalculatingCharacterReader(std::string* text);


					/*
					* Returns the next character of the stored text.
					* @return next character [char]
					*/
					char getNextCharacter();

					/*
					* Sees if there is a next character of the stored text to be returned.
					* @return [bool] : Is there next character available?
					*/
					inline bool hasNextCharacter() { return m_Current != m_End; };

				private:

					inline bool  isASCII(char c) { return c >= 0; };
					void setIterators();
				};
			}
		}
	}
}
#include "LibView/core/LibViewEndTesting.h"