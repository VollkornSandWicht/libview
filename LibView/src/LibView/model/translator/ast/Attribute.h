/**
 * Project LibView
 * @author Leonard Wolf (2207778),  Mirko Sowa (2263258), Daniel Martins Fiebich (2219734), Christoph Böhrer (2070080), Justin Guth (2218548)
 * @version 1.0
 */


#include "LibView/model/translator/token/Token.h"
#include "../token/Token.h"
#include <vector>

namespace LibView
{
	namespace model
	{
		namespace translator
		{
			namespace ast
			{
				/**
				* A representation of a liberty file attribute, simple and complex not distinguished.
				*/
				class Attribute {

				private:

					//member 

					token::Token m_Name;
					std::vector<token::Token> m_Value;

				public:

					/**
					* Constructor taking in the name and value.
					* @param name [token::Token]
					* @param value [std::vector<token::Token>] : The Tokens making up the value(s).
					*/
					Attribute(token::Token name, std::vector<token::Token> value);

					/**
					* Getter for the name.
					* @return name [token::Token]
					*/
					token::Token getM_Name();

					/**
					* Getter for the name.
					* @return name [token::Token]
					*/
					std::vector<token::Token> getM_Value();
				};

			}
		}
	}
}