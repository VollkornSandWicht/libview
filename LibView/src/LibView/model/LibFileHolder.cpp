
#include "LibFileHolder.h"


#include "LibView/exception/model/LibFileIndexNotFoundException.h"
#include "LibView/core/Macros.h"
#include "ModelFacade.h"

namespace LibView
{
    namespace model
    {
        LibFileHolder::~LibFileHolder()
        {
            for(data_structure::LibertyFileData* file : m_LibFiles)
            {
                delete file;
            }

            delete m_AttributeHolder;
        }



        std::vector<data_structure::LibertyFileData*> LibFileHolder::getLibFileList()
        {
            return m_LibFiles;
        }            

        data_structure::LibertyFileData* LibFileHolder::getLibFileByIndex(const index_t& index) const
        {
            for(data_structure::LibertyFileData* dataPointer : m_LibFiles)
            {
                if(dataPointer->getLibFileIndex() == index)
                {
                    return dataPointer;
                }
            };

            throw exception::LibFileIndexNotFoundException(LIBVIEW_LOCATION);
        }

        std::vector<index_t> LibFileHolder::getChildren(const index_t& index)
        {
            return m_AttributeHolder->getChildren(index);
        }

        void LibFileHolder::calculateAllDelta(data_structure::LibertyFileData* libFile)
        {
            /*TODO check parameter, i think we get a index and not the pointer
            (gain access to data behind index)
            for each entry in given liberty file iterate through every other file
            and calculate absolute and percentile difference with (Data of given libFile - iterated libFile)
            if iterated file = given file dont calculate
            store result of each calculation in the designated spot in the data structure
            */
        }


        double LibFileHolder::getDelta(const index_t& libFileIndex, const index_t& attributeIndex)
        {
            //TODO change to pair because we have 2 delat values.
            //resolve index to actual pointer. search in object for the index of the
            //given attribute and return the delta values.
            return 0.0;
        }

        void LibFileHolder::loadLibertyFiles(const std::vector<std::string>& filePathList)
        {
            m_ModelFacade->resetLastRemovedLibFiles();
            m_ModelFacade->resetLastAddedLibFiles();


            for(size_t i = 0; i < filePathList.size(); i++)
            {

                // TODO check if path is directory

                loadLibertyFile(filePathList[i]);

            }
        }

        void LibFileHolder::removeLibertyFile(const index_t& index)
        {
            std::vector<data_structure::LibertyFileData*>::iterator removePosition;

            for(std::vector<data_structure::LibertyFileData*>::iterator it = m_LibFiles.begin(); it != m_LibFiles.end(); ++it) 
            {
                if((*it)->getLibFileIndex() == index)
                {
                    removePosition = it;
                    break;
                }
            }

            m_ModelFacade->resetLastRemovedLibFiles();
            m_ModelFacade->resetLastAddedLibFiles();
            m_ModelFacade->resetLastRemovedAttributes();
            m_ModelFacade->resetLastAddedAttributes();

            delete (*removePosition);
            m_LibFiles.erase(removePosition);


            m_ModelFacade->setLastRemovedLibbFile(index);
        }

        void LibFileHolder::regenerateAttributes()
        {

        }

        void LibFileHolder::loadLibertyFile(const std::string& path)
        {
            
            data_structure::LibertyFileData* newFile = m_LibFileLoader.loadFromFile(path);
            m_LibFiles.push_back(newFile);

            
            

            m_ModelFacade->addLastAddedLibFiles(newFile->getLibFileIndex());

        }

        
        LibFileHolder::LibFileHolder(ModelFacade* facade)
            :
            m_ModelFacade(facade),
            m_AttributeHolder(new AttributeHolder()),
            m_LibFileLoader(model::LibFileLoader(facade)),
            m_LibFiles(std::vector<data_structure::LibertyFileData*>())
        {
        }

    }
}