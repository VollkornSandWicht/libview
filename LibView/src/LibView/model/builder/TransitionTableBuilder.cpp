/**
 * Project LibView
 * @author Leonard Wolf (2207778),  Mirko Sowa (2263258), Daniel Martins Fiebich (2219734), Christoph Böhrer (2070080), Justin Guth (2218548)
 * @version 1.0
 */

 /**
  * TransitionTableBuilder implementation
  *
  * Builder class for the TransitionTable class in the data structure.
  */

#include "TransitionTableBuilder.h"
#include "LibView/model/ModelFacade.h"

namespace LibView
{
    namespace model
    {
        namespace data_structure
        {
             

            TransitionTableBuilder::TransitionTableBuilder(ModelFacade* pFacade) 
                : 

                GroupBuilder(pFacade),
                transitionTable(new TransitionTable(pFacade))
            {
            }

            void TransitionTableBuilder::setSimpleAttribute(std::string name, std::vector<translator::token::Token> value)
            {
                transitionTable->setUndifinedAttribute(name, undifinedSimpleAttributeValueString(value));
            }

            void TransitionTableBuilder::setComplexAttribute(std::string name, std::vector<translator::token::Token> value)
            {
                if(name == "index_1")
                {
                    transitionTable->setIndex_1(to1DArray(value));
                    return;
                }
                else if(name == "index_2")
                {
                    transitionTable->setIndex_2(to1DArray(value));
                    return;
                }
                //else if(name == "index_3")
                //{
                //    transitionTable->setIndex_3(to1DArray(value));
                //    return;
                //}
                
                else if(name == "values")
                {
                    IndexTemplate* indexTemplate = getTemplate(transitionTable);
                    //if(indexTemplate->getIndex_3() != nullptr)
                    //{
                    //    size_t columns = indexTemplate->getDimensionOfIndex_1();
                    //    size_t rows = indexTemplate->getDimensionOfIndex_2();
                    //    size_t  height = indexTemplate->getDimensionOfIndex_3();
                    //    transitionTable->setValues(to3DArray(columns, rows, height, value));
                    //}
                    if(indexTemplate->getIndex_2() != nullptr)
                    {
                        size_t columns = indexTemplate->getDimensionOfIndex_1();
                        size_t rows = indexTemplate->getDimensionOfIndex_2();
                        transitionTable->setValues(to2DArray(columns, rows, value));
                    }
                    else if(indexTemplate->getIndex_1() != nullptr)
                    {
                        transitionTable->setValues(to1DArray(value));
                    }
                    return;
                }
                else if(name == "intermediate_values")
                {
                    IndexTemplate* indexTemplate = getTemplate(transitionTable);
                    //if(indexTemplate->getIndex_3() != nullptr)
                    //{
                    //    size_t columns = indexTemplate->getDimensionOfIndex_1();
                    //    size_t rows = indexTemplate->getDimensionOfIndex_2();
                    //    size_t  height = indexTemplate->getDimensionOfIndex_3();
                    //    transitionTable->setIntermediate_values(to3DArray(columns, rows, height, value));
                    //}
                    if(indexTemplate->getIndex_2() != nullptr)
                    {
                        size_t columns = indexTemplate->getDimensionOfIndex_1();
                        size_t rows = indexTemplate->getDimensionOfIndex_2();
                        transitionTable->setIntermediate_values(to2DArray(columns, rows, value));
                    }
                    else if(indexTemplate->getIndex_1() != nullptr)
                    {
                        transitionTable->setIntermediate_values(to1DArray(value));
                    }
                    return;
                }
                else if(name == "valuesscalar")
                {
                    transitionTable->setValues(to1DArray(value));
                    return;
                }
                else if(name == "intermediate_valuesscalar")
                {
                    transitionTable->setIntermediate_values(to1DArray(value));
                    return;
                }
                transitionTable->setUndifinedAttribute(name, undifinedComplexAttributeValueString(value));
            }

            /**
             * @param group
             * @param parent
             * @return LibertyFileGroup*
             */
            LibertyFileGroup* TransitionTableBuilder::buildGroup(translator::ast::Group* group, LibertyFileGroup* parent)
            {
                transitionTable->setParent(parent);

                std::string path = constructGroupPath(group, parent);
                transitionTable->setPath(path);

                std::vector<std::string> addedAttributes = iterateOverAttributes(group);

                path = transitionTable->getPath();

                getModel()->addAttribute(path);

                // Temporary patch ( insert children into parent group)

                if(parent != nullptr)
                    parent->setChild(transitionTable);


                return transitionTable;
            }
        }
    }
}