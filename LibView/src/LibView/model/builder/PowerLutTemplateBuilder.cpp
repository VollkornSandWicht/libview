/**
 * Project LibView
 * @author Leonard Wolf (2207778),  Mirko Sowa (2263258), Daniel Martins Fiebich (2219734), Christoph Böhrer (2070080), Justin Guth (2218548)
 * @version 1.0
 */

 /**
  * PowerLutTemplateBuilder implementation
  *
  * Builder class for the PowerLutTemplate class in the data structure.
  */

#include "PowerLutTemplateBuilder.h"
#include "LibView/model/ModelFacade.h"

namespace LibView
{
    namespace model
    {
        namespace data_structure
        {
             

            PowerLutTemplateBuilder::PowerLutTemplateBuilder(ModelFacade* pFacade) 
                : 

                GroupBuilder(pFacade),
                powerLutTemplate(new PowerLutTemplate(pFacade))
            {
            }

            void PowerLutTemplateBuilder::setSimpleAttribute(std::string name, std::vector<translator::token::Token> value)
            {
                if(name == "variable_1")
                {                
                    powerLutTemplate->setVariable_1(stringOfFirstToken(value));
                    return;
                }
                else if(name == "variable_2")
                {
                    powerLutTemplate->setVariable_2(stringOfFirstToken(value));
                    return;
                }
                else if(name == "variable_3")
                {
                    powerLutTemplate->setVariable_3(stringOfFirstToken(value));
                    return;
                }

                powerLutTemplate->setUndifinedAttribute(name, undifinedSimpleAttributeValueString(value));
            }

            void PowerLutTemplateBuilder::setComplexAttribute(std::string name, std::vector<translator::token::Token> value)
            {
                if(name == "index_1")
                {
                    powerLutTemplate->setIndex_1(to1DArray(value));
                    return;
                }
                else if(name == "index_2")
                {
                    powerLutTemplate->setIndex_2(to1DArray(value));
                    return;
                }
                else if(name == "index_3")
                {
                    powerLutTemplate->setIndex_3(to1DArray(value));
                    return;
                }

                powerLutTemplate->setUndifinedAttribute(name, undifinedComplexAttributeValueString(value));
            }

            /**
             * @param group
             * @param parent
             * @return LibertyFileGroup*
             */
            LibertyFileGroup* PowerLutTemplateBuilder::buildGroup(translator::ast::Group* group, LibertyFileGroup* parent)
            {
                powerLutTemplate->setParent(parent);

                std::string path = constructGroupPath(group, parent);
                powerLutTemplate->setPath(path);

                std::vector<std::string> addedAttributes = iterateOverAttributes(group);

                path = powerLutTemplate->getPath();

                //updateAttributeHolder(path, addedAttributes);
                getModel()->addAttribute(path);
                // Temporary patch ( insert children into parent group)

                if(parent != nullptr)
                    parent->setChild(powerLutTemplate);


                return powerLutTemplate;
            }
        }
    }
}