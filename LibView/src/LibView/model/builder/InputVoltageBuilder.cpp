/**
 * Project LibView
 * @author Leonard Wolf (2207778),  Mirko Sowa (2263258), Daniel Martins Fiebich (2219734), Christoph Böhrer (2070080), Justin Guth (2218548)
 * @version 1.0
 */

 /**
  * InputVoltageBuilder implementation
  *
  * Builder class for the InputVoltage class in the data structure.
  */

#include "InputVoltageBuilder.h"
#include "LibView/model/ModelFacade.h"


namespace LibView
{
    namespace model
    {
        namespace data_structure
        {
             

            InputVoltageBuilder::InputVoltageBuilder(ModelFacade* pFacade) : 
                GroupBuilder(pFacade), 
                inputVoltage(new InputVoltage(pFacade))
            {
            }

            void InputVoltageBuilder::setSimpleAttribute(std::string name, std::vector<translator::token::Token> value)
            {
                if(name == "vil")
                {
                    inputVoltage->setVil(expressionToString(value));
                    return;
                }
                else if(name == "vih")
                {
                    inputVoltage->setVih(expressionToString(value));
                    return;
                }
                else if(name == "vimin")
                {
                    inputVoltage->setVimin(expressionToString(value));
                    return;
                }
                else if(name == "vimax")
                {
                    inputVoltage->setVimax(expressionToString(value));
                    return;
                }

                inputVoltage->setUndifinedAttribute(name, undifinedSimpleAttributeValueString(value));
            }

            void InputVoltageBuilder::setComplexAttribute(std::string name, std::vector<translator::token::Token> value)
            {

                inputVoltage->setUndifinedAttribute(name, undifinedComplexAttributeValueString(value));
            }

            /**
             * @param group
             * @param parent
             * @return LibertyFileGroup*
             */
            LibertyFileGroup* InputVoltageBuilder::buildGroup(translator::ast::Group* group, LibertyFileGroup* parent)
            {
                inputVoltage->setParent(parent);
                std::string path = constructGroupPath(group, parent);
                inputVoltage->setPath(path);

                std::vector<std::string> addedAttributes = iterateOverAttributes(group);

                path = inputVoltage->getPath();

                //updateAttributeHolder(path, addedAttributes);
                getModel()->addAttribute(path);
                // Temporary patch ( insert children into parent group)

                if(parent != nullptr)
                    parent->setChild(inputVoltage);


                return inputVoltage;
            }
        }
    }
}