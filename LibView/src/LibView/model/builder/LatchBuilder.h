#pragma once

/**
 * Project LibView
 * @author Leonard Wolf (2207778),  Mirko Sowa (2263258), Daniel Martins Fiebich (2219734), Christoph B�hrer (2070080), Justin Guth (2218548)
 * @version 1.0
 */

#include "GroupBuilder.h"
#include "LibView/model/data_structure/Latch.h"

namespace LibView
{
    namespace model
    {
        namespace data_structure
        {
            class LatchBuilder : public GroupBuilder
            {
            public:
                LatchBuilder(ModelFacade* pFacade);

                LibertyFileGroup* buildGroup(translator::ast::Group* group, LibertyFileGroup* parent) override;

            private:

                Latch* latch;

                void setSimpleAttribute(std::string, std::vector<translator::token::Token> value) override;

                void setComplexAttribute(std::string, std::vector<translator::token::Token>) override;
            };
        }
    }
}