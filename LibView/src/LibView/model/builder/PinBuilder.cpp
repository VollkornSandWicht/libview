/**
 * Project LibView
 * @author Leonard Wolf (2207778),  Mirko Sowa (2263258), Daniel Martins Fiebich (2219734), Christoph Böhrer (2070080), Justin Guth (2218548)
 * @version 1.0
 */

 /**
  * PinBuilder implementation
  *
  * Builder class for the Pin class in the data structure.
  */

#include "PinBuilder.h"
#include "LibView/model/ModelFacade.h"

namespace LibView
{
    namespace model
    {
        namespace data_structure
        {
             

            PinBuilder::PinBuilder(ModelFacade* pFacade) :
                GroupBuilder(pFacade),
                pin(new Pin(pFacade))
            {
            }

            void PinBuilder::setSimpleAttribute(std::string name, std::vector<translator::token::Token> value)
            {
                if(name == "direction")
                {
                    pin->setDirection(stringOfFirstToken(value));
                    return;
                }
                else if(name == "capacitance")
                {
                    pin->setCapacitance(toDouble(value.front()));
                    return;
                }
                else if(name == "driver_waveform_rise")
                {
                    pin->setDriver_waveform_rise(stringOfFirstToken(value));
                    return;
                }
                else if(name == "driver_waveform_fall")
                {
                    pin->setDriver_waveform_fall(stringOfFirstToken(value));
                    return;
                }
                else if(name == "input_voltage")
                {
                    pin->setInput_voltage(stringOfFirstToken(value));
                    return;
                }
                else if(name == "function")
                {
                    pin->setFunction(stringOfFirstToken(value));
                    return;
                }
                else if(name == "output_voltage")
                {
                    pin->setOutput_voltage(stringOfFirstToken(value));
                    return;
                }
                else if(name == "min_capacitance")
                {
                    pin->setMin_capacitance(toDouble(value.front()));
                    return;
                }
                else if(name == "max_capacitance")
                {
                    pin->setMax_capacitance(toDouble(value.front()));
                    return;
                }
                else if(name == "clock")
                {
                    pin->setClock(stringOfFirstToken(value));
                    return;
                }
                else if(name == "min_pulse_width_low")
                {
                    pin->setMin_pulse_width_low(toDouble(value.front()));
                    return;
                }
                else if(name == "min_pulse_width_high")
                {
                    pin->setMin_pulse_width_high(toDouble(value.front()));
                    return;
                }
                else if(name == "nextstate_type")
                {
                    pin->setNextstate_type(stringOfFirstToken(value));
                    return;
                }
                else if(name == "three_state")
                {
                    pin->setThree_state(stringOfFirstToken(value));
                    return;
                }

                pin->setUndifinedAttribute(name, undifinedSimpleAttributeValueString(value));
            }

            void PinBuilder::setComplexAttribute(std::string name, std::vector<translator::token::Token> value)
            {
                pin->setUndifinedAttribute(name, undifinedComplexAttributeValueString(value));
            }

            /**
             * @param group
             * @param parent
             * @return LibertyFileGroup*
             */
            LibertyFileGroup* PinBuilder::buildGroup(translator::ast::Group* group, LibertyFileGroup* parent)
            {
                pin->setParent(parent);

                std::string path = constructGroupPath(group, parent);
                pin->setPath(path);

                std::vector<std::string> addedAttributes = iterateOverAttributes(group);

                path = pin->getPath();

                //updateAttributeHolder(path, addedAttributes);
                getModel()->addAttribute(path);
                // Temporary patch ( insert children into parent group)

                if(parent != nullptr)
                    parent->setChild(pin);


                return pin;
            }
        }
    }
}