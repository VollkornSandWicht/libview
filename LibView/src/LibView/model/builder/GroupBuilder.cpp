/**
 * Project LibView
 * @author Leonard Wolf (2207778),  Mirko Sowa (2263258), Daniel Martins Fiebich (2219734), Christoph Böhrer (2070080), Justin Guth (2218548)
 * @version 1.0
 */

 /**
  * GroupBuilder implementation
  */

#include "GroupBuilder.h"

#include "LibView/model/ModelFacade.h"

namespace LibView
{
    namespace model
    {
        namespace data_structure
        {
            //GroupBuilder::GroupBuilder() : builtGroup(nullptr), attrHolder(nullptr)
            //{}
            /**
            * constructor for GroupBuilder
            * @param ModelFacade* reference to ModelFacade
            */
            GroupBuilder::GroupBuilder(ModelFacade* pFacade)
                :
                builtGroup(nullptr),
                ModelFacadeKnower(pFacade)
            {}

            /**
             * The build operation for creating a group in the data structur after parsing a file.
             * @param translator::ast::Group* with no specified builder
             * @param LibertyFileGroup* the parent group
             * @return LibertyFileGroup* the built group
             */
            LibertyFileGroup* GroupBuilder::buildGroup(translator::ast::Group* group, LibertyFileGroup* parent)
            {
                std::string name;
                std::vector<translator::token::Token> tokens = group->getName();
                std::vector<translator::token::Token>::iterator iterator = tokens.begin();

                for(; iterator != tokens.end(); ++iterator)
                {
                    name.append(iterator->getValue());
                }
                builtGroup = new LibertyFileGroup(getModel());
                builtGroup->setParent(parent);
                std::string path = constructGroupPath(group, parent);
                builtGroup->setPath(path);

                std::vector<std::string> addedAttributes = iterateOverAttributes(group);

                getModel()->addAttribute(path);

                // Temporary patch ( insert children into parent group)

                if(parent != nullptr)
                {
                    parent->setChild(builtGroup);
                }

                return builtGroup;
            }


            /**
            * translating a simple attribute into the data structure
            * @param std::string name of the attribute
            * @param std::vector<translator::token::Token> value vector that is translated into a string
            */
            void GroupBuilder::setSimpleAttribute(std::string name, std::vector<translator::token::Token> value)
            {
                builtGroup->setUndifinedAttribute(name, undifinedSimpleAttributeValueString(value));
            }


            /**
            * translating a complex attribute into the data structure
            * @param std::string name of the attribute
            * @param std::vector<translator::token::Token> value vector that is translated into it's corresponding attribute
            */
            void GroupBuilder::setComplexAttribute(std::string name, std::vector<translator::token::Token> value)
            {
                builtGroup->setUndifinedAttribute(name, undifinedComplexAttributeValueString(value));
            }



            /**
            * translating a undifined simple attribute into the data structure
            * @param std::vector<translator::token::Token> value vector that is translated into a string
            * @return std::string the value vector is translated into
            */
            std::string GroupBuilder::undifinedSimpleAttributeValueString(std::vector<translator::token::Token> value)
            {
                //std::string result = " : ";
                std::string result = "";

                std::vector<translator::token::Token>::iterator iterator = value.begin();

                for(; iterator != value.end(); ++iterator)
                {
                    result.append(iterator->getValue());
                }
                //result.append(" ;"); //closing simple attribute

                return result;
            }


            /**
            * translating a undifined complex attribute into the data structure
            * @param std::vector<translator::token::Token> value vector that is translated
            * @return std::string the value vector is translated into
            */
            std::string GroupBuilder::undifinedComplexAttributeValueString(std::vector<translator::token::Token> value)
            {
                std::string result = ""; //opening complex attribute

                std::vector<translator::token::Token>::iterator iterator = value.begin();

                for(; iterator != value.end(); ++iterator)
                {
                    if(iterator->getKey() == translator::token::TokenKeys::ParameterSeperator)
                        result.append(", "); //VECTOR NOT SAFE
                    else
                        result.append(iterator->getValue());
                }
                //result.append(") ;"); //close complex attribute
                return result;
            }


            /**
            * translating a value vector into a 1d array containing double values
            * @param std::vector<translator::token::Token> value vector that is translated into 1d array
            * @return DynamicDimensionArray* the created 3d array
            */
            DynamicDimensionArray* GroupBuilder::to1DArray(std::vector<translator::token::Token> value)
            {
                DynamicDimensionArray* result = new DynamicDimensionArray(1);

                std::vector<translator::token::Token>::iterator iterator = value.begin();

                for(; iterator != value.end(); ++iterator)
                {
                    if(iterator->getKey() == translator::token::TokenKeys::Float || iterator->getKey() == translator::token::TokenKeys::FloatS || iterator->getKey() == translator::token::TokenKeys::Integer)
                        *result << atof(iterator->getValue().c_str());
                }
                return result;
            }



            /**
            * translating a value vector into a 2d array containing double values
            * @param std::vector<translator::token::Token> value vector that is translated into 2d array
            * @return DynamicDimensionArray* the created 3d array
            */
            DynamicDimensionArray* GroupBuilder::to2DArray(size_t columns, size_t rows, std::vector<translator::token::Token> value)
            {
                size_t count = 0;
                DynamicDimensionArray* result = new DynamicDimensionArray(2);
                result->reserve(columns);

                std::vector<translator::token::Token>::iterator iterator = value.begin();

                for(; iterator != value.end(); ++iterator)
                {
                    if(iterator->getKey() == translator::token::TokenKeys::Float || iterator->getKey() == translator::token::TokenKeys::FloatS || iterator->getKey() == translator::token::TokenKeys::Integer)
                    {
                        std::string temp = iterator->getValue();
                        double doubleValue = atof(temp.c_str());
                        size_t position = floor(count / rows);
                        (*result)[position] << doubleValue;
                        count++;
                    }
                }

                return result;
            }



            /**
            * translating a value vector into a 3d array containing double values
            * @param std::vector<translator::token::Token> value vector that is translated into 3d array
            * @return DynamicDimensionArray* the created 3d array
            */
            //DynamicDimensionArray* GroupBuilder::to3DArray(size_t columns, size_t rows, size_t height, //std::vector<translator::token::Token> value) //TODO FIX
            //{
            //    size_t count = 1;
            //    DynamicDimensionArray result = DynamicDimensionArray(3);
            //    result.reserve(columns);
            //    for(size_t i = 0; i < columns; i++)
            //        result[i].reserve(rows);
            //
            //    std::vector<translator::token::Token>::iterator iterator = value.begin();
            //
            //    size_t column;
            //    size_t row;
            //
            //    for(; iterator != value.end(); ++iterator)
            //    {
            //        if(iterator->getKey() == translator::token::TokenKeys::Float || iterator->getKey() == //translator::token::TokenKeys::FloatS || iterator->getKey() == translator::token::TokenKeys::Integer)
            //        {
            //            size_t column = floor(count / rows);
            //            size_t row = count % height;
            //            result[column][row] << atof(iterator->getValue().c_str());
            //            count++;
            //        }
            //    }
            //    return new DynamicDimensionArray(result);
            //
            //}



            /**
            * translating a value vector that contains a expression into a std::string
            * @param std::vector<translator::token::Token> value vector that needs translation
            * @return std::string the expression as a std::string
            */
            std::string GroupBuilder::expressionToString(std::vector<translator::token::Token> value)
            {
                std::string result = "";

                std::vector<translator::token::Token>::iterator iterator = value.begin();

                for(; iterator != value.end(); ++iterator)
                {
                    result.append(iterator->getValue());
                }
                return result;
            }


            /**
            * translating a Token containing a double value as std::string into double
            * @param translator::token::Token value token that is translated
            * @return double std::string value in the Token is parsed to
            */
            double GroupBuilder::toDouble(translator::token::Token value)
            {
                return atof(value.getValue().c_str());
            }



            /**
            * translating a value vector into a vector of std::strings
            * @param std::vector<translator::token::Token> value vector that is translated into a vector
            */
            std::vector<std::string> GroupBuilder::toVectorOfString(std::vector<translator::token::Token> value)
            {
                std::vector<std::string> vectorOfString;

                std::vector<translator::token::Token>::iterator iterator = value.begin();

                for(; iterator != value.end(); ++iterator)
                {
                    if(iterator->getKey() != translator::token::TokenKeys::ParameterSeperator)
                        vectorOfString.push_back(iterator->getValue());
                }
                return vectorOfString;
            }


            /**
            *
            * @param
            * @return
            */
            std::string GroupBuilder::stringOfFirstToken(std::vector<translator::token::Token> value) //TODO remove same as expression to string MERGE THEM
            {
                std::string result = "";
                std::vector<translator::token::Token>::iterator iterator = value.begin();

                for(; iterator != value.end(); ++iterator)
                {
                    result.append(iterator->getValue());
                }
                return result;
            }

            /**
            * methode to create the hierarchical path of a group
            * @param translator::ast::Group* group for which the path will be created
            * @param LibertyFileGroup* the parent of the group
            * @return std::string of the created path
            */
            std::string GroupBuilder::constructGroupPath(translator::ast::Group* group, LibertyFileGroup* parent)
            {
                std::string name;
                std::string path;



                std::vector<translator::token::Token> tokens = group->getName();
                std::vector<translator::token::Token>::iterator iterator = tokens.begin();


                //for(std::vector<translator::token::Token>::iterator iterator = group->getName().begin(); iterator != group->getName().end(); ++iterator)
                //                                                                      ---------                             ---------
                // Returnen neue kopien; iteratoren können also nie gleich sein

                for(; iterator != tokens.end(); ++iterator)
                {
                    name.append(iterator->getValue());
                }


                if(parent)
                    path = parent->getPath() + "/" + group->getType().getValue() + "(" + name + ")";
                else
                    path = group->getType().getValue() + "(" + name + ")";

                return path;
            }



            /**
            * iterrate over all attributes that need to be set for a new group
            * @param translator::ast::Group* group for which attributes need to be set
            * @return std::vector<std::string> of set attributes
            */
            std::vector<std::string> GroupBuilder::iterateOverAttributes(translator::ast::Group* group)
            {
                std::string groupType = group->getType().getValue();
                std::vector<std::string> addedAttributes;
                addedAttributes.clear();
                std::vector<translator::ast::Attribute*> simpleAttributes = group->getSimpleAttributes();
                bool isScalar = false;

                for(translator::ast::Attribute* attribute : simpleAttributes)
                {
                    translator::token::Token nameToken = attribute->getM_Name();
                    std::string name = nameToken.getValue();
                    setSimpleAttribute(name, attribute->getM_Value());
                    addedAttributes.push_back(name);
                }
                std::vector<translator::ast::Attribute*> complexAttributes = group->getComplexAttributes();

                std::string groupName = stringOfFirstToken(group->getName());
                if(groupName == "scalar")
                    isScalar = true;

                for(translator::ast::Attribute* attribute : complexAttributes)
                {

                    translator::token::Token nameToken = attribute->getM_Name();
                    std::string name = nameToken.getValue();
                    if((name == "values" || name == "intermediate_values") && isScalar)
                        name += "scalar";
                    setComplexAttribute(name, attribute->getM_Value());
                    addedAttributes.push_back(name);
                }

                return addedAttributes;
            }




            /**
            * notify the attribute holder about added attributes
            * @param std::string path of the group
            * @param std::vector<std::string> addedAttributes
            */
            //void GroupBuilder::updateAttributeHolder(std::string path, std::vector<std::string> addedAttributes)
            //{
            //    getModel()->addAttribute(path); //ping AttributeHolder with group path
            //
            //    for(std::string attributes : addedAttributes)
            //    {
            //        getModel()->addAttribute(path + "/" + attributes); //ping AttributeHolder with attribute paths
            //    }
            //}



            /**
            * get pointer to Library file
            * @param LibertyFileGroup* to look if it is the library group
            */
            LibertyFileGroup* GroupBuilder::getLibrary(LibertyFileGroup* child)
            {
                if(child->getParent() != nullptr)
                    return getLibrary(child->getParent());
                else
                    return child;
            }



            /**
            * get the table template class for the attribute
            * @param LibertyFileGroup* group the defining template is needed for
            * @return IndexTemplate* the needed table template
            */
            IndexTemplate* GroupBuilder::getTemplate(LibertyFileGroup* child)
            {
                std::string name = child->getName();
                LibertyFileGroup* library = getLibrary(child);
                IndexTemplate* indexTemplate = (IndexTemplate*) library->getChild(name);
                if(indexTemplate != nullptr)
                    return indexTemplate;
                else
                    throw - 1; //TODO replace                
            }
        }
    }
}