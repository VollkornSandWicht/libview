/**
 * Project LibView
 * @author Leonard Wolf (2207778),  Mirko Sowa (2263258), Daniel Martins Fiebich (2219734), Christoph Böhrer (2070080), Justin Guth (2218548)
 * @version 1.0
 */

 /**
  * FlipFlopBuilder implementation
  *
  * Builder class for the FlipFlop class in the data structure.
  */

#include "FlipFlopBuilder.h"
#include "LibView/model/ModelFacade.h"


namespace LibView
{
    namespace model
    {
        namespace data_structure
        {
             

            FlipFlopBuilder::FlipFlopBuilder(ModelFacade* pFacade) : 
                GroupBuilder(pFacade), 
                flipFlop(new Ff(pFacade))
            {
            }

            void FlipFlopBuilder::setSimpleAttribute(std::string name, std::vector<translator::token::Token> value)
            {
                if(name == "clocked_on")
                {
                    flipFlop->setClocked_on(expressionToString(value));
                    return;
                }
                else if(name == "next_state")
                {
                    flipFlop->setNext_state(expressionToString(value));
                    return;
                }
                else if(name == "clear")
                {
                    flipFlop->setClear(expressionToString(value));
                    return;
                }
                else if(name == "preset")
                {
                    flipFlop->setPreset(expressionToString(value));
                    return;
                }
                flipFlop->setUndifinedAttribute(name, undifinedSimpleAttributeValueString(value));
            }

            void FlipFlopBuilder::setComplexAttribute(std::string name, std::vector<translator::token::Token> value)
            {
                flipFlop->setUndifinedAttribute(name, undifinedComplexAttributeValueString(value));
            }

            /**
             * @param group
             * @param parent
             * @return LibertyFileGroup*
             */
            LibertyFileGroup* FlipFlopBuilder::buildGroup(translator::ast::Group* group, LibertyFileGroup* parent)
            {
                flipFlop->setParent(parent);
                std::string path = constructGroupPath(group, parent);
                flipFlop->setPath(path);

                std::vector<std::string> addedAttributes = iterateOverAttributes(group);

                path = flipFlop->getPath();

                //updateAttributeHolder(path, addedAttributes);
                getModel()->addAttribute(path);
                // Temporary patch ( insert children into parent group)

                if(parent != nullptr)
                    parent->setChild(flipFlop);


                return flipFlop;
            }
        }
    }
}