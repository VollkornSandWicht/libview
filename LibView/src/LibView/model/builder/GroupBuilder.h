#pragma once

/**
 * Project LibView
 * @author Leonard Wolf (2207778),  Mirko Sowa (2263258), Daniel Martins Fiebich (2219734), Christoph Böhrer (2070080), Justin Guth (2218548)
 * @version 1.0
 */

#include "LibView/model/data_structure/LibertyFileGroup.h"
#include "LibView/model/translator/ast/Group.h"
#include "LibView/model/translator/token/Token.h"
#include "LibView/model/AttributeHolder.h"
#include "LibView/model/data_structure/IndexTemplate.h"
#include "LibView/model/data_structure/DynamicDimensionArray.h"

#include "LibView/model/ModelFacadeKnower.h"

#include <array>
#include <math.h>

namespace LibView
{
    namespace model
    {
        namespace data_structure
        {
            class GroupBuilder : public ModelFacadeKnower
            {
            public:
                //GroupBuilder();

                GroupBuilder(ModelFacade* pFacade);

                /**
                 * The build operation for creating a group in the data structur after parsing a file.
                 * @param group
                 * @param parent
                 */
                virtual LibertyFileGroup* buildGroup(translator::ast::Group* group, LibertyFileGroup* parent);
            protected:
                

                virtual void setSimpleAttribute(std::string, std::vector<translator::token::Token> value);

                virtual void setComplexAttribute(std::string, std::vector<translator::token::Token>);

                std::string undifinedComplexAttributeValueString(std::vector<translator::token::Token> value);

                std::string undifinedSimpleAttributeValueString(std::vector<translator::token::Token> value);

                DynamicDimensionArray* to1DArray(std::vector<translator::token::Token> value);

                std::string expressionToString(std::vector<translator::token::Token> value);

                double toDouble(translator::token::Token value);

                std::vector<std::string> toVectorOfString(std::vector<translator::token::Token> value);

                std::string stringOfFirstToken(std::vector<translator::token::Token> value);

                std::string constructGroupPath(translator::ast::Group* group, LibertyFileGroup* parent);

                std::vector<std::string> iterateOverAttributes(translator::ast::Group* group);

                //AttributeHolder* attrHolder;

                //void updateAttributeHolder(std::string path, std::vector<std::string> addedAttributes);

                LibertyFileGroup* getLibrary(LibertyFileGroup*);

                DynamicDimensionArray* to2DArray(size_t columns, size_t rows, std::vector<translator::token::Token> value);

                //DynamicDimensionArray* to3DArray(size_t columns, size_t rows, size_t height, std::vector<translator::token::Token> value);

                IndexTemplate* getTemplate(LibertyFileGroup*);

            private: 
                LibertyFileGroup* builtGroup;
            };
        }
    }
}