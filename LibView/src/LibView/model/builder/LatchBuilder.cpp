#include "LatchBuilder.h"
#include "LibView/model/ModelFacade.h"


namespace LibView
{
    namespace model
    {
        namespace data_structure
        {
            LatchBuilder::LatchBuilder(ModelFacade* pFacade)
                :
                GroupBuilder(pFacade),
                latch(new Latch(pFacade))
            {}

            LibertyFileGroup* LatchBuilder::buildGroup(translator::ast::Group* group, LibertyFileGroup* parent)
            {
                latch->setParent(parent);
                std::string path = constructGroupPath(group, parent);
                latch->setPath(path);

                std::vector<std::string> addedAttributes = iterateOverAttributes(group);

                path = latch->getPath();

                //updateAttributeHolder(path, addedAttributes);
                getModel()->addAttribute(path);
                // Temporary patch ( insert children into parent group)

                if(parent != nullptr)
                    parent->setChild(latch);


                return latch;
            }

            void LatchBuilder::setSimpleAttribute(std::string name, std::vector<translator::token::Token> value)
            {
                if(name == "enable")
                {
                    latch->setEnable(stringOfFirstToken(value));
                    return;
                }
                else if(name == "data_in")
                {
                    latch->setData_in(stringOfFirstToken(value));
                    return;
                }
                latch->setUndifinedAttribute(name, undifinedSimpleAttributeValueString(value));
            }

            void LatchBuilder::setComplexAttribute(std::string name, std::vector<translator::token::Token> value)
            {
                latch->setUndifinedAttribute(name, undifinedComplexAttributeValueString(value));
            }
        }
    }
}
