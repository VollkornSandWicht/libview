/**
 * Project LibView
 * @author Leonard Wolf (2207778),  Mirko Sowa (2263258), Daniel Martins Fiebich (2219734), Christoph Böhrer (2070080), Justin Guth (2218548)
 * @version 1.0
 */

 /**
  * TimingBuilder implementation
  *
  * Builder class for the Timing class in the data structure.
  */

#include "TimingBuilder.h"
#include "LibView/model/ModelFacade.h"

namespace LibView
{
    namespace model
    {
        namespace data_structure
        {
             

            TimingBuilder::TimingBuilder(ModelFacade* pFacade) 
                : 

                GroupBuilder(pFacade),
                timing(new Timing(pFacade))
            {
            }

            void TimingBuilder::setSimpleAttribute(std::string name, std::vector<translator::token::Token> value)
            {
                if(name == "related_pin")
                {
                    timing->setRelated_pin(expressionToString(value));
                    return;
                }
                else if(name == "timing_sense")
                {
                    timing->setTiming_sense(expressionToString(value));
                    return;
                }
                else if(name == "timing_type")
                {
                    timing->setTiming_type(expressionToString(value));
                    return;
                }

                timing->setUndifinedAttribute(name, undifinedSimpleAttributeValueString(value));
            }

            void TimingBuilder::setComplexAttribute(std::string name, std::vector<translator::token::Token> value)
            {
                timing->setUndifinedAttribute(name, undifinedComplexAttributeValueString(value));
            }

            /**
             * @param group
             * @param parent
             * @return LibertyFileGroup*
             */
            LibertyFileGroup* TimingBuilder::buildGroup(translator::ast::Group* group, LibertyFileGroup* parent)
            {
                timing->setParent(parent);

                std::string path = constructGroupPath(group, parent);
                timing->setPath(path);

                std::vector<std::string> addedAttributes = iterateOverAttributes(group);

                path = timing->getPath();

                //updateAttributeHolder(path, addedAttributes);
                getModel()->addAttribute(path);
                // Temporary patch ( insert children into parent group)

                if(parent != nullptr)
                    parent->setChild(timing);


                return timing;
            }
        }
    }
}