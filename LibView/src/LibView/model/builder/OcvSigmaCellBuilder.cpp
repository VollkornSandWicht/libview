/**
 * Project LibView
 * @author Leonard Wolf (2207778),  Mirko Sowa (2263258), Daniel Martins Fiebich (2219734), Christoph Böhrer (2070080), Justin Guth (2218548)
 * @version 1.0
 */

 /**
  * OcvSigmaCellBuilder implementation
  *
  * Builder class for the OcvSigmaCell class in the data structure.
  */

#include "OcvSigmaCellBuilder.h"
#include "LibView/model/ModelFacade.h"

namespace LibView
{
    namespace model
    {
        namespace data_structure
        {
             

            OcvSigmaCellBuilder::OcvSigmaCellBuilder(ModelFacade* pFacade) :
                GroupBuilder(pFacade),
                ocvSigmaCell(new OcvSigmaCell(pFacade))
            {
            }

            void OcvSigmaCellBuilder::setSimpleAttribute(std::string name, std::vector<translator::token::Token> value)
            {
                if(name == "sigma_type")
                {
                    ocvSigmaCell->setSigma_type(stringOfFirstToken(value));
                    return;
                }

                ocvSigmaCell->setUndifinedAttribute(name, undifinedSimpleAttributeValueString(value));
            }

            void OcvSigmaCellBuilder::setComplexAttribute(std::string name, std::vector<translator::token::Token> value)
            {
                if(name == "index_1")
                {
                    ocvSigmaCell->setIndex_1(to1DArray(value));
                    return;
                }
                else if(name == "index_2")
                {
                    ocvSigmaCell->setIndex_2(to1DArray(value));
                    return;
                }
                //else if(name == "index_3")
                //{
                //    ocvSigmaCell->setIndex_3(to1DArray(value));
                //    return;
                //}
                
                else if(name == "values")
                {
                    IndexTemplate* indexTemplate = getTemplate(ocvSigmaCell);
                    //if(indexTemplate->getIndex_3() != nullptr)
                    //{
                    //    size_t columns = indexTemplate->getDimensionOfIndex_1();
                    //    size_t rows = indexTemplate->getDimensionOfIndex_2();
                    //    size_t  height = indexTemplate->getDimensionOfIndex_3();
                    //    ocvSigmaCell->setValues(to3DArray(columns, rows, height, value));
                    //}
                    if(indexTemplate->getIndex_2() != nullptr)
                    {
                        size_t columns = indexTemplate->getDimensionOfIndex_1();
                        size_t rows = indexTemplate->getDimensionOfIndex_2();
                        ocvSigmaCell->setValues(to2DArray(columns, rows, value));
                    }
                    else if(indexTemplate->getIndex_1() != nullptr)
                    {
                        ocvSigmaCell->setValues(to1DArray(value));
                    }
                    return;
                }
                else if(name == "valuesscalar")
                {
                    ocvSigmaCell->setValues(to1DArray(value));
                    return;
                }
                ocvSigmaCell->setUndifinedAttribute(name, undifinedComplexAttributeValueString(value));
            }

            /**
             * @param group
             * @param parent
             * @return LibertyFileGroup*
             */
            LibertyFileGroup* OcvSigmaCellBuilder::buildGroup(translator::ast::Group* group, LibertyFileGroup* parent)
            {
                ocvSigmaCell->setParent(parent);

                std::string path = constructGroupPath(group, parent);
                ocvSigmaCell->setPath(path);

                std::vector<std::string> addedAttributes = iterateOverAttributes(group);

                path = ocvSigmaCell->getPath();

                //updateAttributeHolder(path, addedAttributes);
                getModel()->addAttribute(path);
                // Temporary patch ( insert children into parent group)

                if(parent != nullptr)
                parent->setChild(ocvSigmaCell);


                return ocvSigmaCell;
            }
        }
    }
}