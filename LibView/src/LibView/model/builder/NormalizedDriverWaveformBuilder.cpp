/**
 * Project LibView
 * @author Leonard Wolf (2207778),  Mirko Sowa (2263258), Daniel Martins Fiebich (2219734), Christoph Böhrer (2070080), Justin Guth (2218548)
 * @version 1.0
 */

 /**
  * NormalizedDriverWaveformBuilder implementation
  *
  * Builder class for the NormalizedDriverWaveform class in the data structure.
  */

#include "NormalizedDriverWaveformBuilder.h"
#include "LibView/model/ModelFacade.h"

namespace LibView
{
    namespace model
    {
        namespace data_structure
        {
             

            NormalizedDriverWaveformBuilder::NormalizedDriverWaveformBuilder(ModelFacade* pFacade) 
                : 

                GroupBuilder(pFacade),
                ndw(new NormalizedDriverWaveform(pFacade))
            {
            }

            void NormalizedDriverWaveformBuilder::setSimpleAttribute(std::string name, std::vector<translator::token::Token> value)
            {
                if(name == "driver_waveform_name")
                {
                    ndw->setDriver_waveform_name(stringOfFirstToken(value));
                    return;
                }

                ndw->setUndifinedAttribute(name, undifinedSimpleAttributeValueString(value));
            }

            void NormalizedDriverWaveformBuilder::setComplexAttribute(std::string name, std::vector<translator::token::Token> value)
            {
                if(name == "index_1")
                {
                    ndw->setIndex_1(to1DArray(value));
                    return;
                }
                else if(name == "index_2")
                {
                    ndw->setIndex_2(to1DArray(value));
                    return;
                }
                else if(name == "values")
                {
                    if(ndw->getIndex_2() != nullptr)
                    {
                        size_t columns = ndw->getIndex_1()->getSize();
                        size_t rows = ndw->getIndex_2()->getSize();
                        ndw->setValues(to2DArray(columns, rows, value));
                    }
                    else if(ndw->getIndex_1() != nullptr)
                    {
                        ndw->setValues(to1DArray(value));
                    }
                    return;
                }


                ndw->setUndifinedAttribute(name, undifinedComplexAttributeValueString(value));
            }

            /**
             * @param group
             * @param parent
             * @return LibertyFileGroup*
             */
            LibertyFileGroup* NormalizedDriverWaveformBuilder::buildGroup(translator::ast::Group* group, LibertyFileGroup* parent)
            {
                ndw->setParent(parent);

                std::string path = constructGroupPath(group, parent);
                ndw->setPath(path);

                std::vector<std::string> addedAttributes = iterateOverAttributes(group);

                path = ndw->getPath();

                //updateAttributeHolder(path, addedAttributes);
                getModel()->addAttribute(path);
                // Temporary patch ( insert children into parent group)

                if(parent != nullptr)
                    parent->setChild(ndw);


                return ndw;
            }
        }
    }
}