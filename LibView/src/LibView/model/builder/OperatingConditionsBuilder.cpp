/**
 * Project LibView
 * @author Leonard Wolf (2207778),  Mirko Sowa (2263258), Daniel Martins Fiebich (2219734), Christoph Böhrer (2070080), Justin Guth (2218548)
 * @version 1.0
 */

 /**
  * OperatingConditionsBuilder implementation
  *
  * Builder class for the OperatingConditions class in the data structure.
  */

#include "OperatingConditionsBuilder.h"
#include "LibView/model/ModelFacade.h"

namespace LibView
{
    namespace model
    {
        namespace data_structure
        {
             

            OperatingConditionsBuilder::OperatingConditionsBuilder(ModelFacade* pFacade) :
                GroupBuilder(pFacade),
                operatingConditions(new OperatingConditions(pFacade))
            {
            }

            void OperatingConditionsBuilder::setSimpleAttribute(std::string name, std::vector<translator::token::Token> value)
            {
                if(name == "process")
                {
                    operatingConditions->setProcess(toDouble(value.front()));
                    return;
                }
                else if(name == "temperature")
                {
                    operatingConditions->setTemperature(toDouble(value.front()));
                    return;
                }
                else if(name == "voltage")
                {
                    operatingConditions->setVoltage(toDouble(value.front()));
                    return;
                }

                operatingConditions->setUndifinedAttribute(name, undifinedSimpleAttributeValueString(value));
            }

            void OperatingConditionsBuilder::setComplexAttribute(std::string name, std::vector<translator::token::Token> value)
            {
                operatingConditions->setUndifinedAttribute(name, undifinedComplexAttributeValueString(value));
            }

            /**
             * @param group
             * @param parent
             * @return LibertyFileGroup*
             */
            LibertyFileGroup* OperatingConditionsBuilder::buildGroup(translator::ast::Group* group, LibertyFileGroup* parent)
            {
                operatingConditions->setParent(parent);

                std::string path = constructGroupPath(group, parent);
                operatingConditions->setPath(path);

                std::vector<std::string> addedAttributes = iterateOverAttributes(group);

                path = operatingConditions->getPath();

                //updateAttributeHolder(path, addedAttributes);
                getModel()->addAttribute(path);
                // Temporary patch ( insert children into parent group)

                if(parent != nullptr)
                    parent->setChild(operatingConditions);


                return operatingConditions;
            }
        }
    }
}