/**
 * Project LibView
 * @author Leonard Wolf (2207778),  Mirko Sowa (2263258), Daniel Martins Fiebich (2219734), Christoph Böhrer (2070080), Justin Guth (2218548)
 * @version 1.0
 */

 /**
  * InternalPowerBuilder implementation
  *
  * Builder class for the InternalPower class in the data structure.
  */

#include "InternalPowerBuilder.h"
#include "LibView/model/ModelFacade.h"


namespace LibView
{
    namespace model
    {
        namespace data_structure
        {
             

            InternalPowerBuilder::InternalPowerBuilder(ModelFacade* pFacade) :
                GroupBuilder(pFacade),
                internalPower(new InternalPower(pFacade))
            {
            }

            void InternalPowerBuilder::setSimpleAttribute(std::string name, std::vector<translator::token::Token> value)
            {
                if(name == "related_pin")
                {
                    internalPower->setRelated_pin(expressionToString(value));
                    return;
                }

                internalPower->setUndifinedAttribute(name, undifinedSimpleAttributeValueString(value));
            }

            void InternalPowerBuilder::setComplexAttribute(std::string name, std::vector<translator::token::Token> value)
            {
                internalPower->setUndifinedAttribute(name, undifinedComplexAttributeValueString(value));
            }

            /**
             * @param group
             * @param parent
             * @return LibertyFileGroup*
             */
            LibertyFileGroup* InternalPowerBuilder::buildGroup(translator::ast::Group* group, LibertyFileGroup* parent)
            {
                internalPower->setParent(parent);

                std::string path = constructGroupPath(group, parent);
                internalPower->setPath(path);

                std::vector<std::string> addedAttributes = iterateOverAttributes(group);

                path = internalPower->getPath();

                //updateAttributeHolder(path, addedAttributes);
                getModel()->addAttribute(path);
                // Temporary patch ( insert children into parent group)

                if(parent != nullptr)
                    parent->setChild(internalPower);


                return internalPower;
            }


        }
    }
}