
#include "AHolder.h"

namespace LibView
{
    namespace model
    {


       

        void AHolder::clearLastStoredItems()
        {
            m_LastAdded.clear();
            m_LastRemoved.clear();
        }

        void AHolder::addToLastAdded(path_index_pair pair)
        {
            m_LastAdded.push_back(pair);
        }

        void AHolder::resetLastAdded()
        {
            m_LastAdded.clear();
        }

        void AHolder::resetLastRemoved()
        {
            m_LastRemoved.clear();
        }

        void AHolder::addToLastRemoved(path_index_pair pair)
        {
            m_LastRemoved.push_back(pair);

        }

        std::vector<path_index_pair>  AHolder::getLastAdded() const
        {
            return m_LastAdded;
        }

        std::vector<path_index_pair>  AHolder::getLastRemoved() const
        {
            return m_LastRemoved;
        }


    }
}