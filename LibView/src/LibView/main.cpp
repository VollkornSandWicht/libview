#ifndef LIBVIEW_TEST


#include <QApplication>
#include "LibView/view/QtView.h"
#include "LibView/presenter/PresenterFacade.h"
#include "LibView/model/ModelFacade.h"

int main(int argc, char** argv)
{
    QApplication app(argc, argv);

    

    LibView::model::ModelFacade* modelFacade = new LibView::model::ModelFacade();
    LibView::presenter::PresenterFacade* presenter = new LibView::presenter::PresenterFacade(modelFacade);
    LibView::view::QtView* view = new LibView::view::QtView(presenter);



    int status = app.exec();

    delete view; // TODO Make view delete all pointers

    return status;
}

#endif