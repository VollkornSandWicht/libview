#pragma once
#include "JSON_decl.h"
#include <vector>
#include <string>

namespace JG
{
    namespace json
    {
        class JSONArray
        {
        private:

            std::vector<JSON*> m_Vector;
        
        public:


            JSONArray();
            JSONArray(const std::vector<JSON*>& array);
            JSONArray(const JSONArray& array);
            JSONArray(const JSONArray* array);
            ~JSONArray();

            JSON& operator[] (const const size_t& index);

            
            void operator<< (JSON* object);


            std::string toString();


        private:

            void clearData();
        };

    }
}