#pragma once

namespace JG
{
    namespace json
    {
        enum class JSONDataType
        {
            UNDEFINED,
            BOOL,
            ARRAY,
            OBJECT,
            INTEGER,
            FLOAT,
            STRING,
            UNSIGNED,
            BINARY,
            DISCARDED

        };
    }
}