#pragma once

/**

The JSON class is made available to the LibView project only for internal use.

For any other use please contact @VollkornSandWicht on GitLab

*/















#include "JSONDataType.h"
#include <vector>
#include <string>

#include "JSONArray.h"
#include "JSONObject.h"


namespace JG
{
    namespace json
    {

        class JSON
        {

            friend JSONObject;
            friend JSONArray;

        private:

            JSONDataType m_DataType;

            union
            {
                void* m_u_Undefined = nullptr;
                std::string* m_u_String;
                int64_t* m_u_Int;
                uint64_t* m_u_UnsignedInt;
                double* m_u_Double;
                bool* m_u_Bool;
                JSONArray* m_u_Array;
                JSONObject* m_u_Object;
                //std::vector<char>* m_u_Binary;
            };

        public:

            JSON();
            ~JSON();

            JSON (const std::string& text);
            JSON (const char* text);
            JSON (const int64_t& integer);
            JSON (const uint64_t& unsignedInteger);
            JSON (const double& floatType);
            JSON (const bool& boolType);
            JSON (const std::vector<JSON*>& array);
            JSON (const JSONArray& array);
            JSON (const std::map<std::string, JSON*>& object);
            JSON (const JSONObject& object);
            JSON (const JSON& object);
            JSON (const JSON* object);


            // TODO Make Value Preferred using template <> or smth similar


            JSON& operator= (const std::string& text);
            JSON& operator= (const char* text);
            JSON& operator= (const int64_t& integer);
            JSON& operator= (const uint64_t& unsignedInteger);
            JSON& operator= (const double& floatType);
            JSON& operator= (const bool& boolType);
            JSON& operator= (const std::vector<JSON*>& array);
            JSON& operator= (const JSONArray& array);
            JSON& operator= (const JSONObject& object);
            JSON& operator= (const std::map<std::string, JSON*>& object);
            JSON& operator= (const JSON& object);
            JSON& operator= (const JSON* object);
            //JSON& operator= (const std::vector<char>& binary);

            JSON& operator[] (const std::string& index);
            JSON& operator[] (const char* index);

            JSON& operator[] (const size_t& index);

            JSON& operator<< (const std::string& text);
            JSON& operator<< (const char* text);
            JSON& operator<< (const int64_t& integer);
            JSON& operator<< (const uint64_t& unsignedInteger);
            JSON& operator<< (const double& floatType);
            JSON& operator<< (const bool& boolType);
            JSON& operator<< (const std::vector<JSON*>& array);
            JSON& operator<< (const JSON& object);

            JSON& assignSelf(const JSON& object);

            std::string toString();


            template <typename T_Type>
            T_Type get();

            std::string str();
            double flt();
            uint64_t uint();
            int64_t sint();

            JSONDataType getDataType();


        private:

            void clearData();
            void checkInitArray();
        };

        template<typename T_Type>
        inline T_Type JSON::get()
        {
            // TODO Static assert types

            // TODO add implementation


            return *((T_Type*) m_u_Undefined);
        }

    }
}