# Summary

Date : 2020-08-22 19:40:10

Directory f:\dev\libview-gl\LibView\src

Total : 496 files,  18447 codes, 4780 comments, 5855 blanks, all 29082 lines

[details](details.md)

## Languages
| language | files | code | comment | blank | total |
| :--- | ---: | ---: | ---: | ---: | ---: |
| C++ | 494 | 17,884 | 4,780 | 5,842 | 28,506 |
| Markdown | 2 | 563 | 0 | 13 | 576 |

## Directories
| path | files | code | comment | blank | total |
| :--- | ---: | ---: | ---: | ---: | ---: |
| . | 496 | 18,447 | 4,780 | 5,855 | 29,082 |
| .VSCodeCounter | 2 | 563 | 0 | 13 | 576 |
| LibView | 494 | 17,884 | 4,780 | 5,842 | 28,506 |
| LibView\core | 6 | 170 | 88 | 76 | 334 |
| LibView\exception | 41 | 517 | 146 | 120 | 783 |
| LibView\exception\model | 28 | 303 | 51 | 44 | 398 |
| LibView\exception\model\parser | 8 | 72 | 0 | 4 | 76 |
| LibView\exception\model\reader | 4 | 36 | 0 | 3 | 39 |
| LibView\exception\model\tokenizer | 6 | 54 | 0 | 5 | 59 |
| LibView\exception\presenter | 2 | 18 | 0 | 8 | 26 |
| LibView\moc | 35 | 2,744 | 290 | 465 | 3,499 |
| LibView\moc\qt | 30 | 2,250 | 240 | 390 | 2,880 |
| LibView\moc\view | 5 | 494 | 50 | 75 | 619 |
| LibView\model | 177 | 8,150 | 3,174 | 2,635 | 13,959 |
| LibView\model\builder | 40 | 2,114 | 547 | 555 | 3,216 |
| LibView\model\data_structure | 52 | 2,744 | 1,157 | 879 | 4,780 |
| LibView\model\translator | 54 | 2,157 | 720 | 658 | 3,535 |
| LibView\model\translator\ast | 4 | 171 | 159 | 62 | 392 |
| LibView\model\translator\parser | 32 | 1,063 | 259 | 260 | 1,582 |
| LibView\model\translator\parser\ConcreteStates | 24 | 664 | 150 | 109 | 923 |
| LibView\model\translator\reader | 3 | 94 | 56 | 38 | 188 |
| LibView\model\translator\token | 3 | 160 | 26 | 39 | 225 |
| LibView\model\translator\tokenizer | 10 | 638 | 203 | 248 | 1,089 |
| LibView\presenter | 41 | 953 | 638 | 469 | 2,060 |
| LibView\presenter\presets | 22 | 340 | 248 | 174 | 762 |
| LibView\qt | 91 | 1,717 | 217 | 717 | 2,651 |
| LibView\qt\uic | 31 | 1,177 | 217 | 477 | 1,871 |
| LibView\test | 31 | 1,374 | 102 | 484 | 1,960 |
| LibView\test\core | 1 | 43 | 0 | 13 | 56 |
| LibView\test\model | 27 | 1,248 | 102 | 446 | 1,796 |
| LibView\test\model\AttributeMap | 2 | 113 | 0 | 37 | 150 |
| LibView\test\model\LibViewPair | 1 | 38 | 0 | 13 | 51 |
| LibView\test\model\data_structure | 12 | 671 | 78 | 201 | 950 |
| LibView\test\model\data_structure\BasicTable | 1 | 28 | 0 | 10 | 38 |
| LibView\test\model\data_structure\CapacitiveLoadUnitAttribute | 2 | 46 | 0 | 17 | 63 |
| LibView\test\model\data_structure\Cell | 1 | 20 | 0 | 9 | 29 |
| LibView\test\model\data_structure\Ff | 1 | 36 | 0 | 15 | 51 |
| LibView\test\model\data_structure\InputVoltage | 1 | 62 | 0 | 31 | 93 |
| LibView\test\model\data_structure\InternalPower | 1 | 21 | 0 | 10 | 31 |
| LibView\test\model\data_structure\LeakagePower | 1 | 29 | 0 | 10 | 39 |
| LibView\test\model\data_structure\LibertyFileData | 1 | 27 | 10 | 9 | 46 |
| LibView\test\model\data_structure\LibertyFileGroup | 1 | 40 | 68 | 14 | 122 |
| LibView\test\model\data_structure\Library | 2 | 362 | 0 | 76 | 438 |
| LibView\test\model\translator | 12 | 426 | 24 | 195 | 645 |
| LibView\test\model\translator\ast | 2 | 87 | 8 | 40 | 135 |
| LibView\test\model\translator\parser | 2 | 19 | 2 | 16 | 37 |
| LibView\test\model\translator\parser\ConcreteStates | 1 | 19 | 2 | 15 | 36 |
| LibView\test\model\translator\reader | 4 | 142 | 8 | 73 | 223 |
| LibView\test\model\translator\token | 1 | 25 | 2 | 10 | 37 |
| LibView\test\model\translator\tokenizer | 3 | 153 | 4 | 56 | 213 |
| LibView\test\model\translator\tokenizer\Tokennizing | 1 | 110 | 2 | 39 | 151 |
| LibView\test\presenter | 1 | 30 | 0 | 15 | 45 |
| LibView\test\test_one | 1 | 49 | 0 | 10 | 59 |
| LibView\view | 70 | 2,243 | 125 | 863 | 3,231 |

[details](details.md)