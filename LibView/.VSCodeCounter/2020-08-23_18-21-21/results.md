# Summary

Date : 2020-08-23 18:21:21

Directory f:\dev\libview-gl\LibView\src\LibView

Total : 524 files,  18963 codes, 4829 comments, 6279 blanks, all 30071 lines

[details](details.md)

## Languages
| language | files | code | comment | blank | total |
| :--- | ---: | ---: | ---: | ---: | ---: |
| C++ | 524 | 18,963 | 4,829 | 6,279 | 30,071 |

## Directories
| path | files | code | comment | blank | total |
| :--- | ---: | ---: | ---: | ---: | ---: |
| . | 524 | 18,963 | 4,829 | 6,279 | 30,071 |
| core | 6 | 170 | 88 | 76 | 334 |
| exception | 41 | 517 | 146 | 120 | 783 |
| exception\model | 28 | 303 | 51 | 44 | 398 |
| exception\model\parser | 8 | 72 | 0 | 4 | 76 |
| exception\model\reader | 4 | 36 | 0 | 3 | 39 |
| exception\model\tokenizer | 6 | 54 | 0 | 5 | 59 |
| exception\presenter | 2 | 18 | 0 | 8 | 26 |
| moc | 37 | 2,940 | 310 | 495 | 3,745 |
| moc\qt | 30 | 2,250 | 240 | 390 | 2,880 |
| moc\view | 7 | 690 | 70 | 105 | 865 |
| model | 185 | 8,368 | 3,178 | 2,689 | 14,235 |
| model\builder | 40 | 2,111 | 550 | 555 | 3,216 |
| model\data_structure | 52 | 2,760 | 1,157 | 879 | 4,796 |
| model\presets | 5 | 88 | 0 | 15 | 103 |
| model\translator | 54 | 2,159 | 720 | 658 | 3,537 |
| model\translator\ast | 4 | 171 | 159 | 62 | 392 |
| model\translator\parser | 32 | 1,063 | 259 | 260 | 1,582 |
| model\translator\parser\ConcreteStates | 24 | 664 | 150 | 109 | 923 |
| model\translator\reader | 3 | 94 | 56 | 38 | 188 |
| model\translator\token | 3 | 160 | 26 | 39 | 225 |
| model\translator\tokenizer | 10 | 640 | 203 | 248 | 1,091 |
| presenter | 41 | 1,005 | 651 | 487 | 2,143 |
| presenter\presets | 22 | 340 | 248 | 174 | 762 |
| qt | 91 | 1,698 | 217 | 713 | 2,628 |
| qt\uic | 31 | 1,158 | 217 | 473 | 1,848 |
| test | 49 | 1,922 | 103 | 780 | 2,805 |
| test\core | 1 | 43 | 0 | 13 | 56 |
| test\model | 45 | 1,796 | 103 | 742 | 2,641 |
| test\model\AttributeMap | 2 | 113 | 0 | 37 | 150 |
| test\model\LibViewPair | 1 | 38 | 0 | 13 | 51 |
| test\model\data_structure | 12 | 719 | 79 | 221 | 1,019 |
| test\model\data_structure\BasicTable | 1 | 28 | 0 | 10 | 38 |
| test\model\data_structure\CapacitiveLoadUnitAttribute | 2 | 46 | 0 | 17 | 63 |
| test\model\data_structure\Cell | 1 | 27 | 1 | 18 | 46 |
| test\model\data_structure\Ff | 1 | 42 | 0 | 20 | 62 |
| test\model\data_structure\InputVoltage | 1 | 69 | 0 | 32 | 101 |
| test\model\data_structure\InternalPower | 1 | 27 | 0 | 11 | 38 |
| test\model\data_structure\LeakagePower | 1 | 36 | 0 | 11 | 47 |
| test\model\data_structure\LibertyFileData | 1 | 32 | 10 | 10 | 52 |
| test\model\data_structure\LibertyFileGroup | 1 | 42 | 68 | 14 | 124 |
| test\model\data_structure\Library | 2 | 370 | 0 | 78 | 448 |
| test\model\translator | 30 | 926 | 24 | 471 | 1,421 |
| test\model\translator\ast | 2 | 87 | 8 | 40 | 135 |
| test\model\translator\parser | 2 | 19 | 2 | 16 | 37 |
| test\model\translator\parser\ConcreteStates | 1 | 19 | 2 | 15 | 36 |
| test\model\translator\reader | 4 | 142 | 8 | 73 | 223 |
| test\model\translator\token | 1 | 25 | 2 | 10 | 37 |
| test\model\translator\tokenizer | 21 | 653 | 4 | 332 | 989 |
| test\model\translator\tokenizer\Tokennizing | 19 | 610 | 2 | 315 | 927 |
| test\presenter | 1 | 30 | 0 | 15 | 45 |
| test\test_one | 1 | 49 | 0 | 10 | 59 |
| view | 72 | 2,327 | 136 | 906 | 3,369 |

[details](details.md)